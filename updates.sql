CREATE TABLE points_redemption (
                                   id int(11) NOT NULL,
                                   profile_id int(11) NOT NULL,
                                   account int(11) NOT NULL,
                                   transaction_type int(11) NOT NULL,
                                   amount int(11) NOT NULL,
                                   created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                   updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE points_redemption
    ADD PRIMARY KEY (id),
    ADD KEY profile_id (profile_id),
    ADD KEY account (account),
    ADD KEY transaction_type (transaction_type),
    ADD KEY amount (amount),
    ADD KEY created (created),
    ADD KEY updated (updated);


ALTER TABLE points_redemption
    MODIFY id int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE admin_transaction (
                                   id int(11) NOT NULL,
                                   profile_id int(11) NOT NULL,
                                   account bigint(50) NOT NULL,
                                   transaction_type int(11) NOT NULL,
                                   amount int(11) NOT NULL,
                                   created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                   updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE admin_transaction
    ADD PRIMARY KEY (id),
    ADD KEY profile_id (profile_id),
    ADD KEY account (account),
    ADD KEY transaction_type (transaction_type),
    ADD KEY amount (amount),
    ADD KEY created (created),
    ADD KEY updated (updated);


ALTER TABLE admin_transaction
    MODIFY id int(11) NOT NULL AUTO_INCREMENT;





ALTER TABLE `utility` ADD `purchase_type` INT NOT NULL DEFAULT '1' AFTER `payment_id`, ADD INDEX (`purchase_type`);


ALTER TABLE `utility` ADD `initiator` bigint(50) NOT NULL DEFAULT '0' AFTER `payment_id`, ADD INDEX (`initiator`);