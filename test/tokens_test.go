package test

import (
	"fmt"
	"log"
	"strings"
	"testing"
)

func TokenFormatter(tokens string) string {


	parts := strings.Split(tokens,"")
	counts := 4

	var groups []string
	group := ""

	for z,y := range parts {

		x := z +1

		group = strings.TrimSpace(fmt.Sprintf("%s%s",group,y))

		if x > 0 && x % counts == 0 {

			groups = append(groups,group)
			group = ""
		}

	}


	if len(group) > 0 {

		groups = append(groups,group)
		group = ""
	}

	return strings.Join(groups,"-")

}

func TestTokenFormatter(t *testing.T)  {

	tokens := "44936810930842465459"
	formatted := TokenFormatter(tokens)

	log.Printf("Received %s formatted %s ",tokens,formatted)

}

func TestHmac(t *testing.T)  {

	tokens := "44936810930842465459"
	formatted := TokenFormatter(tokens)

	log.Printf("Received %s formatted %s ",tokens,formatted)

}