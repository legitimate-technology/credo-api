package main

import (
	_ "credo/app/queue"
	subroute "credo/app/router"
	_ "credo/local_time"
	_ "credo/app/cron"
)


func main() {

	router := &subroute.App{}
	router.Initialize()
	router.Run()

	//select {}
	//forever := make(chan bool)
	//<-forever
}
