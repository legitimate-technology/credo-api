package worker

import (
	"credo/app/library"
	"credo/app/logger"
	"database/sql"
	"fmt"
	"github.com/streadway/amqp"
	"gopkg.in/ini.v1"
	"strconv"
)

func ProcessAirtimeTransaction(db *sql.DB,cfg *ini.File,deliveries <-chan amqp.Delivery, done chan error)  {

	for d := range deliveries {

		t0 := library.GetTime()

		tx, err := db.Begin()
		if err != nil {

			logger.Error("got error starting db transaction %s ",err.Error())
			logger.PanicLog.Printf("got error starting database transaction")
			d.Ack(false)
			continue
		}

		transactionSTMT, err := tx.Prepare("UPDATE  utility SET status = ?,description = ?,last_retry = now() WHERE id = ?  ")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ",err.Error())
			logger.PanicLog.Printf("got error starting database transaction")
			d.Ack(false)
			continue
		}


		transactionID,err := strconv.ParseInt(string(d.Body),10,64)
		if err != nil {
			transactionSTMT.Close()
			tx.Rollback()
			logger.Error(" got error decoding queue response to struct %s",err.Error())
			d.Ack(false)
			continue
		}

		logger.Error("GOT FROM AIRTIME QUEUE %s ",string(d.Body))


		// load utility

		var profile_id, transaction_type, amount sql.NullInt64
		var reference sql.NullString

		err = db.QueryRow("SELECT profile_id, transaction_type, reference, amount FROM utility WHERE  id = ? AND status IN (1,2,3) AND length(reference) > 0 AND reference IS NOT NULL ",transactionID).Scan(&profile_id, &transaction_type, &reference, &amount)

		if err != nil && err != sql.ErrNoRows {

			transactionSTMT.Close()
			tx.Rollback()
			logger.Error(" could not retrieve transaction ")
			d.Ack(false)
			continue

		} else if err == sql.ErrNoRows {

			transactionSTMT.Close()
			tx.Rollback()
			d.Ack(false)
			continue
		}

		// update transaction
		_, err = transactionSTMT.Exec(2,"Transaction Queued for sending",transactionID)
		if err != nil {

			transactionSTMT.Close()
			tx.Rollback()
			logger.Error("got error executing transaction %s ",err.Error())
			d.Ack(false)
			continue
		}

		// call AirTime API
		msisdn, _ := strconv.ParseInt(reference.String,10,64)


		// get initiator
		var initiator sql.NullInt64
		err = db.QueryRow("SELECT initiator FROM utility WHERE id = ? ",transactionID).Scan(&initiator)
		if err != nil {

			logger.Error("got error retrieving initiator")
			initiator.Int64 = msisdn

		}

		if initiator.Int64 == 0 {

			initiator.Int64 = msisdn
		}

		err = library.SendAirtime(db,cfg,transactionID,amount.Int64,msisdn,initiator.Int64)

		if err == nil {

			_, err = transactionSTMT.Exec(4,"Transaction successful",transactionID)
			if err != nil {

				logger.Error("got error executing transaction %s ",err.Error())
				d.Ack(false)
				transactionSTMT.Close()
				tx.Commit()
				continue
			}

			// update user points
			points := int64(int64(amount.Int64) / 10 )
			updateStmt, err := db.Prepare("UPDATE profile_balance SET points = points + ? WHERE profile_id = ? LIMIT 1")
			if err != nil {

				logger.Error("got error executing transaction %s ",err.Error())
				transactionSTMT.Close()
				d.Ack(false)
				continue
			}

			_, err = updateStmt.Exec(points,profile_id.Int64)
			if err != nil {

				logger.Error("got error executing transaction %s ",err.Error())
				transactionSTMT.Close()
				updateStmt.Close()
				d.Ack(false)
				updateStmt.Close()
				continue
			}

			updateStmt.Close()

		} else {

			_, err = transactionSTMT.Exec(3,fmt.Sprintf("Failed %s",err.Error()),transactionID)
			if err != nil {

				logger.Error("got error executing transaction %s ",err.Error())
				d.Ack(false)
				transactionSTMT.Close()
				tx.Commit()
				continue
			}
		}

		transactionSTMT.Close()
		tx.Commit()

		d.Ack(false)

		t1 := library.GetTime()
		tt := t1 - t0
		logger.Log(" time taken to process airtime for %s amount %d is %dms ",reference.String,amount.Int64,tt)
	}

	//logger.Info("handle 2")

	logger.Info("handle: deliveries channel closed")
	done <- nil
}

func ProcessKPLCTokenTransaction(db *sql.DB,cfg *ini.File,deliveries <-chan amqp.Delivery, done chan error)  {

	for d := range deliveries {

		t0 := library.GetTime()

		tx, err := db.Begin()
		if err != nil {

			logger.Error("got error starting db transaction %s ",err.Error())
			continue
		}

		transactionSTMT, err := tx.Prepare("UPDATE  utility SET status = ?,description = ?,last_retry = now() WHERE id = ?  ")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ",err.Error())
			continue
		}

		transactionID,err := strconv.ParseInt(string(d.Body),10,64)
		if err != nil {

			transactionSTMT.Close()
			tx.Rollback()
			logger.Error(" got error decoding queue response to struct %s",err.Error())
			d.Nack(false,true)
			continue
		}

		// load utility

		var msisdn, transaction_type, amount,profile_id sql.NullInt64
		var reference sql.NullString

		err = db.QueryRow("SELECT p.msisdn, t.transaction_type, t.reference, t.amount,t.profile_id FROM utility t INNER JOIN profile p ON t.profile_id = p.id WHERE  t.id = ? AND t.status IN (1,2,3) AND length(reference) > 0 AND reference IS NOT NULL ",transactionID).Scan(&msisdn, &transaction_type, &reference, &amount,&profile_id)

		if err != nil && err != sql.ErrNoRows {

			transactionSTMT.Close()
			tx.Rollback()
			logger.Error(" could not retrieve transaction ")
			d.Nack(false,true)
			continue

		} else if err == sql.ErrNoRows {

			transactionSTMT.Close()
			tx.Rollback()
			d.Ack(false)
			continue
		}

		// update transaction
		_, err = transactionSTMT.Exec(2,"Transaction Queued for sending",transactionID)
		if err != nil {

			transactionSTMT.Close()
			tx.Rollback()
			logger.Error("got error executing transaction %s ",err.Error())
			d.Nack(false,true)
			continue
		}

		// call AirTime API

		err = library.PurchaseKPLCToken(db,cfg,transactionID,amount.Int64,reference.String,msisdn.Int64)

		if err == nil {

			_, err = transactionSTMT.Exec(4,"TransactionSuccessful",transactionID)
			if err != nil {

				logger.Error("got error executing transaction %s ",err.Error())
				transactionSTMT.Close()
				tx.Commit()
				d.Ack(false)
				continue
			}

			// update user points
			points := int64(int64(amount.Int64) / 10 )
			updateStmt, err := db.Prepare("UPDATE profile_balance SET points = points + ? WHERE profile_id = ? LIMIT 1")
			if err != nil {

				logger.Error("got error executing transaction %s ",err.Error())
				transactionSTMT.Close()
				d.Ack(false)
				continue
			}

			_, err = updateStmt.Exec(points,profile_id.Int64)
			if err != nil {

				logger.Error("got error executing transaction %s ",err.Error())
				transactionSTMT.Close()
				updateStmt.Close()
				d.Ack(false)
				updateStmt.Close()
				continue
			}

			updateStmt.Close()

		} else {

			_, err = transactionSTMT.Exec(3,fmt.Sprintf("Failed %s ",err.Error()),transactionID)
			if err != nil {

				logger.Error("got error executing transaction %s ",err.Error())
				tx.Commit()
				d.Ack(false)
				continue
			}

		}

		transactionSTMT.Close()
		tx.Commit()

		d.Ack(false)

		t1 := library.GetTime()
		tt := t1 - t0
		logger.Log(" time taken to process airtime for %s amount %d is %dms ",reference.String,amount.Int64,tt)
	}

	//logger.Info("handle 2")

	logger.Info("handle: deliveries channel closed")
	done <- nil
}
