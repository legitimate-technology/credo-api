package queue

import (
	"credo/app/logger"
	"credo/app/queue/worker"
	"credo/config"
	"database/sql"
	"fmt"
	"github.com/logrusorgru/aurora"
	"github.com/streadway/amqp"
	"gopkg.in/ini.v1"
	"redis"
	"strconv"
)

type Consumer struct {
	conn    *amqp.Connection
	channel *amqp.Channel
	tag     string
	done    chan error
}

func NewConsumer(conn *amqp.Connection, db *sql.DB,cfg *ini.File,redisConn *redis.Client,exchange, exchangeType, queue, key, ctag string, prefetch int) (*Consumer, error) {

	var err error

	c := &Consumer{
		conn:    conn,
		channel: nil,
		tag:     ctag,
		done:    make(chan error),
	}

	// initialise channel
	c.channel, err = c.conn.Channel()

	if err != nil {

		return nil, fmt.Errorf("channel: %s", err)

	}

	// set channet properties
	err = c.channel.Qos(
		prefetch, // prefetch count
		0,       // prefetch size
		true,    // global
	)

	if err != nil {

		return nil, fmt.Errorf("channel: %s", err)
	}

	// declare exchange
	if err = c.channel.ExchangeDeclare(
		exchange,     // name of the exchange
		exchangeType, // type
		true,         // durable
		false,        // delete when complete
		false,        // internal
		false,        // noWait
		nil,          // arguments
	);

		err != nil {
		return nil, fmt.Errorf("exchange Declare: %s", err)
	}

	// desclare queue
	_, err = c.channel.QueueDeclare(
		queue, // name of the queue
		true,  // durable
		false, // delete when usused
		false, // exclusive
		false, // noWait
		nil,   // arguments
	)

	if err != nil {

		return nil, fmt.Errorf("queue Declare: %s", err)
	}

	// bind to queue
	if err = c.channel.QueueBind(
		queue,    // name of the queue
		key,      // bindingKey
		exchange, // sourceExchange
		false,    // noWait
		nil,      // arguments
	); err != nil {

		return nil, fmt.Errorf("queue Bind: %s", err)

	}

	// start consuming from queue
	deliveries, err := c.channel.Consume(
		queue, // name
		c.tag, // consumerTag,
		false, // noAck
		false, // exclusive
		false, // noLocal
		false, // noWait
		nil,   // arguments
	)

	if err != nil {

		return nil, fmt.Errorf("queue consume: %s", err)
	}

	//logger.Log("DONE setting up %s ",c.tag)

	forever := make(chan bool)

	switch c.tag {

		case "AIRTIME":
			worker.ProcessAirtimeTransaction(db, cfg,deliveries, c.done)
			break

		case "KPLC_TOKEN":
			worker.ProcessKPLCTokenTransaction(db, cfg,deliveries, c.done)
			break
	}

	<-forever

	return c, nil
}

func (c *Consumer) Shutdown() error {
	// will close() the deliveries channel
	if err := c.channel.Cancel(c.tag, true); err != nil {
		return fmt.Errorf("consumer cancel failed: %s", err)
	}

	if err := c.conn.Close(); err != nil {
		return fmt.Errorf("AMQP connection close error: %s", err)
	}

	defer logger.Log("AMQP shutdown OK")

	// wait for handle() to exit
	return <-c.done
}

func SetupQueue(conn *amqp.Connection, db *sql.DB,cfg *ini.File,redisConn *redis.Client, QueueName string) {

	queue, err := config.GetKey(cfg,QueueName, "queue")
	if err != nil {
		logger.Error(err.Error())
	}

	exchange, err := config.GetKey(cfg,QueueName, "exchange")
	if err != nil {
		logger.Error(err.Error())
	}

	bindingKey, err := config.GetKey(cfg,QueueName, "routing_key")
	if err != nil {
		logger.Error(err.Error())
	}

	exchangeType, err := config.GetKey(cfg,QueueName, "exchange_type")
	if err != nil {
		logger.Error(err.Error())
	}

	workers, err := config.GetKey(cfg,QueueName, "workers")
	if err != nil {

		logger.Error(" cant retrive %s workers got error %s ", QueueName, aurora.Red(err.Error()))
		workers = "1"
	}

	prefetch, err := config.GetKey(cfg,QueueName, "prefetch")
	if err != nil {

		prefetch = workers
	}

	numberOfWorkers, _ := strconv.Atoi(prefetch)

	consumerTag := QueueName

	_, err = NewConsumer(conn, db,cfg, redisConn,exchange, exchangeType, queue, bindingKey, consumerTag, numberOfWorkers)

	if err != nil {

		logger.PanicLog.Fatalf("%s", err.Error())
	}

	logger.Log("running forever")

	select {}
}
