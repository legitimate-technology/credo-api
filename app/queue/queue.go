package queue

import (
	"fmt"
	"github.com/logrusorgru/aurora"
	"github.com/streadway/amqp"
	"gopkg.in/ini.v1"
	"log"
	"credo/app/database"
	"credo/app/logger"
	"credo/config"
	"strconv"
	"strings"
)

type App struct {
	AMQPURI string
}

/**
initialize rabbitMQ connections and start all the consumers
*/

var defaultConfigPath = "/credo/config/config.ini"

func init() {

	var err error


	cfg, err := ini.Load(defaultConfigPath)
	if err != nil {

		log.Panic(err.Error())
	}

	// get configs
	host, err := config.GetKey(cfg,"rabbit", "host")

	if err != nil {

		logger.Error(err.Error())
	}

	user, err := config.GetKey(cfg,"rabbit", "user")

	if err != nil {

		logger.Error(err.Error())
	}

	pass, err := config.GetKey(cfg,"rabbit", "pass")

	if err != nil {
		logger.Error(err.Error())
	}

	port, err := config.GetKey(cfg,"rabbit", "port")
	if err != nil {
		logger.Error(err.Error())
	}

	vhost, err := config.GetKey(cfg,"rabbit", "vhost")
	if err != nil {
		logger.Error(err.Error())
	}

	// create connection string
	amqpURI := fmt.Sprintf("amqp://%s:%s@%s:%s/%s", user, pass, host, port, vhost)

	// open rabbitMQ connection
	conn, err := amqp.Dial(amqpURI)

	if err != nil {

		logger.Error("got error %s establishing queue connection ", err.Error())
		logger.PanicLog.Panic("got error %s establishing queue connection ", err.Error())
	}

	// get database instance
	db := database.DbInstance(cfg)

	// get websocket instance
	queues, err := config.GetKey(cfg,"rabbit", "queues")
	if err != nil {

		logger.Error(" cant retrive rabbit queues got error %s ", aurora.Red(err.Error()))
		panic(err)
	}

	redisConn := database.RedisClient(cfg)

	// get all the queues to consume from
	parts := strings.Split(queues, ",")

	for _, que := range parts {

		workers, err := config.GetKey(cfg,que, "workers")
		if err != nil {

			logger.Error(" cant retrieve %s workers got error %s ", que, aurora.Red(err.Error()))
			panic(err)
		}

		numberOfWorkers, _ := strconv.Atoi(workers)

		x := 0

		for x < numberOfWorkers {

			go SetupQueue(conn, db,cfg,redisConn, que)
			x++
		}
	}

}
