package controllers

import (
	"credo/app/library"
	"credo/app/logger"
	"credo/app/models"
	"credo/config"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/logrusorgru/aurora"
	"github.com/streadway/amqp"
	"gopkg.in/ini.v1"
	"io"
	"io/ioutil"
	"net/http"
	"redis"
	"strconv"
	"strings"
)

type MultiPurchase struct {
	Src      string      `json:"src"`
	Msisdn   interface{} `json:"msisdn"`
	Token string `json:"token"`
	Purchase []struct {
		Account interface{} `json:"account"`
		Amount  interface{} `json:"amount"`
	} `json:"purchase"`
}

type Purchase struct {
	Account int64 `json:"account"`
	Amount  int64 `json:"amount"`
}

func ToHex(int642 int64) string {

	int642 = int642 + 21433740 // make 5 digits starting from FA
	hex :=  strings.ToUpper(strconv.FormatInt(int642,36))
	return strings.ToUpper(hex)
}

/**
@params
{
	"src":"web",
	"msisdn":"0726120256",
	"msisdn_to_topup":"0726120256",
	"amount":100
}
*/
func AirtimePurchase(db *sql.DB, cfg *ini.File, redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, m.Error)
		return
	}

	src, _ := library.GetString(payload, "src", "web")

	// params
	mobile, _ := library.GetString(payload, "msisdn", "")
	mobile = library.FormatMSISDN(mobile)
	if len(mobile) == 0 {

		logger.Log(" could not retrieve mobile number to use to pay ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}
	msisdn, _ := strconv.ParseInt(mobile, 10, 64)

	token, _ := library.GetString(payload, "token", "")
	if len(token) == 0 {

		logger.Log(" could not retrieve token ")
		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	tokenKey := fmt.Sprintf("token:msisdn:%d",msisdn)
	getToken, err := library.GetRedisKey(redisConn,tokenKey)
	if err != nil {

		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if token != getToken {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}


	mobile_to_top, _ := library.GetString(payload, "msisdn_to_topup", "")
	mobile_to_top = library.FormatMSISDN(mobile_to_top)
	if len(mobile_to_top) == 0 {

		logger.Log(" could not retrieve mobile number to topup ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number to topup")
		return
	}
	msisdn_to_top, _ := strconv.ParseInt(mobile_to_top, 10, 64)

	amounts, _ := library.GetFloat(payload, "amount", 0)
	if amounts == 0 {

		logger.Log(" could not retrieve amount ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Invalid amount supplied")
		return
	}

	amount := int64(amounts)

	// create profile if it does not exist
	profile, err := library.GetProfileFromMSISDN(db, redisConn, cfg, msisdn, "", "", src, 0, true)

	if err != nil {

		logger.Log(" could not retrieve profile %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// check balance

	if profile.Balance >= amount {

		// deduct profile

	} else {

		// create transaction
		stmt, err := tx.Prepare("INSERT INTO utility (profile_id, transaction_type, reference, amount, status, created) VALUE (?,1,?,?,0,now()) ")
		if err != nil {

			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		res, err := stmt.Exec(profile.ID, msisdn_to_top, amount)
		if err != nil {

			logger.Error("got error executing transaction %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		lastInsertID, err := res.LastInsertId()
		if err != nil {

			logger.Error("got error retrieving last insert ID %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		tx.Commit()

		// initiate STK Push

		callbackURL, err := config.GetKey(cfg, "payment", "callbackURL")
		if err != nil {

			logger.Error("got error retrieving payment->callbackURL from configs %s ", err.Error())
			callbackURL = "https://sms-savvy.co.ke/payment/callback/{transactionID}"
		}

		callbackURL = strings.Replace(callbackURL, "{transactionID}", fmt.Sprintf("%d", lastInsertID), -1)

		paybill, err := config.GetKey(cfg, "payment", "paybill")
		if err != nil {

			logger.Error("got error retrieving payment->paybill from configs %s ", err.Error())
			paybill = "491929"
		}

		party_b, err := config.GetKey(cfg, "payment", "party_b")
		if err != nil {

			party_b = ""
		}

		paymentURL, err := config.GetKey(cfg, "payment", "paymentURL")
		if err != nil {

			logger.Error("got error retrieving payment->paymentURL from configs %s ", err.Error())
			callbackURL = "https://vas.sms-savvy.co.ke/api/payment/mpesa/request"
		}

		token, err := config.GetKey(cfg, "payment", "token")
		if err != nil {

			logger.Error("got error retrieving payment->token from configs %s ", err.Error())
			callbackURL = ""
		}

		var requestPayload map[string]interface{}

		if len(party_b) == 0 {

			requestPayload = map[string]interface{}{
				"callback_url":   callbackURL,
				"msisdn":         msisdn,
				"amount":         amount,
				"transaction_id": msisdn_to_top,
				"short_code":     paybill,
			}

		} else {

			requestPayload = map[string]interface{}{
				"callback_url":   callbackURL,
				"msisdn":         msisdn,
				"amount":         amount,
				"transaction_id": msisdn_to_top,
				"short_code":     paybill,
				"party_b":        party_b,
			}
		}

		headers := map[string]string{

			"Authorization": token,
			"Content-Type":  "application/json",
			"Accept":        "application/json",
		}

		status, msg := library.HTTPPostWithHeaders(paymentURL, requestPayload, headers)

		if status != 200 {

			logger.Error("got error initiating STK Push %s ", msg)
			RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		RespondJSON(db, started, w, r, http.StatusOK, msg.Message)
		return

	}

	RespondJSON(db, started, w, r, http.StatusOK, "Your request is being processed")
	return
}

/**
@params
{
	"src":"web",
	"msisdn":"0726120256",
	"metre_number":"0726120256",
	"amount":100
}
*/
func TokenPurchase(db *sql.DB, cfg *ini.File, redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, m.Error)
		return
	}

	src, _ := library.GetString(payload, "src", "web")

	// params
	mobile, _ := library.GetString(payload, "msisdn", "")
	mobile = library.FormatMSISDN(mobile)
	if len(mobile) == 0 {

		logger.Log(" could not retrieve mobile number to use to pay ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}
	msisdn, _ := strconv.ParseInt(mobile, 10, 64)


	token, _ := library.GetString(payload, "token", "")
	if len(token) == 0 {

		logger.Log(" could not retrieve token ")
		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	tokenKey := fmt.Sprintf("token:msisdn:%d",msisdn)
	getToken, err := library.GetRedisKey(redisConn,tokenKey)
	if err != nil {

		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if token != getToken {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	metre_number, _ := library.GetInt(payload, "metre_number", 0)
	if metre_number < 100000000 {

		logger.Log(" could not retrieve metre_number ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Invalid metre_number supplied")
		return
	}

	amount, _ := library.GetInt(payload, "amount", 0)
	if amount < 0 {

		logger.Log(" could not retrieve amount ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Invalid amount supplied. Enter amount greater than 200")
		return
	}
	// create profile if it does not exist
	profile, err := library.GetProfileFromMSISDN(db, redisConn, cfg, msisdn, "", "", src, 0, true)

	if err != nil {

		logger.Log(" could not retrieve profile %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// check balance

	if profile.Balance >= amount {

		// deduct profile

	} else {

		// create transaction
		stmt, err := tx.Prepare("INSERT INTO utility (profile_id, transaction_type, reference, amount, status, created) VALUE (?,2,?,?,0,now()) ")
		if err != nil {

			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		res, err := stmt.Exec(profile.ID, metre_number, amount)
		if err != nil {

			logger.Error("got error executing transaction %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		lastInsertID, err := res.LastInsertId()
		if err != nil {

			logger.Error("got error retrieving last insert ID %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		tx.Commit()
		// initiate STK Push

		callbackURL, err := config.GetKey(cfg, "payment", "callbackURL")
		if err != nil {

			logger.Error("got error retrieving payment->callbackURL from configs %s ", err.Error())
			callbackURL = "https://airtime.wasiliana.co.ke/payment/callback/{transactionID}"
		}

		callbackURL = strings.Replace(callbackURL, "{transactionID}", fmt.Sprintf("%d", lastInsertID), -1)

		paybill, err := config.GetKey(cfg, "payment", "paybill")
		if err != nil {

			logger.Error("got error retrieving payment->paybill from configs %s ", err.Error())
			paybill = "491929"
		}

		party_b, err := config.GetKey(cfg, "payment", "party_b")
		if err != nil {

			party_b = ""
		}

		paymentURL, err := config.GetKey(cfg, "payment", "paymentURL")
		if err != nil {

			logger.Error("got error retrieving payment->paymentURL from configs %s ", err.Error())
			callbackURL = "https://vas.wasiliana.co.ke/api/payment/mpesa/request"
		}

		token, err := config.GetKey(cfg, "payment", "token")
		if err != nil {

			logger.Error("got error retrieving payment->token from configs %s ", err.Error())
			callbackURL = ""
		}

		var requestPayload map[string]interface{}

		if len(party_b) == 0 {

			requestPayload = map[string]interface{}{
				"callback_url":   callbackURL,
				"msisdn":         msisdn,
				"amount":         amount,
				"transaction_id": metre_number,
				"paybill":        paybill,
			}

		} else {

			requestPayload = map[string]interface{}{
				"callback_url":   callbackURL,
				"msisdn":         msisdn,
				"amount":         amount,
				"transaction_id": metre_number,
				"short_code":     paybill,
				"party_b":        party_b,
			}
		}

		headers := map[string]string{

			"Authorization": token,
			"Content-Type":  "application/json",
			"Accept":        "application/json",
		}

		status, msg := library.HTTPPostWithHeaders(paymentURL, requestPayload, headers)

		if status != 200 {

			logger.Error("got error initiating STK Push %s ", msg)
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		RespondJSON(db, started, w, r, http.StatusOK, msg.Message)
		return
	}

	RespondJSON(db, started, w, r, http.StatusOK, "Your request is being processed")
	return
}

/**
@params
{
	"src":"web",
	"msisdn_to_topup":"0726120256",
	"purchase":[
		{
			"msisdn": "0726120256",
			"amount": 100
		},
		{
			"msisdn": "0726120256",
			"amount": 100
		},
	]
}
*/
func TokenMultiPurchase(db *sql.DB, cfg *ini.File, redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload MultiPurchase

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, m.Error)
		return
	}

	if len(payload.Purchase) == 0 {

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, "Missing account numbers to topup")
		return
	}

	src := payload.Src

	// params
	mobile, _ := library.GetStringValue(payload.Msisdn, "")
	mobile = library.FormatMSISDN(mobile)
	if len(mobile) == 0 {

		logger.Log(" could not retrieve mobile number to use to pay ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}
	msisdn, _ := strconv.ParseInt(mobile, 10, 64)

	token:= payload.Token
	if len(token) == 0 {

		logger.Log(" could not retrieve token ")
		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	tokenKey := fmt.Sprintf("token:msisdn:%d",msisdn)
	getToken, err := library.GetRedisKey(redisConn,tokenKey)
	if err != nil {

		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if token != getToken {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	var purchase []Purchase
	totalAmount := int64(0)

	for _, a := range payload.Purchase {

		metre_number, _ := library.GetInt64Value(a.Account, 0)
		if metre_number < 100000000 {

			logger.Log(" could not retrieve metre_number ")
			RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Invalid metre_number supplied")
			return
		}

		amount, _ := library.GetInt64Value(a.Amount, 0)
		if amount < 0 {

			logger.Log(" could not retrieve amount ")
			RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Invalid amount supplied. Enter amount greater than 200")
			return
		}

		purchase = append(purchase, Purchase{
			Account: metre_number,
			Amount:  amount,
		})

		totalAmount = totalAmount + amount
	}

	// create profile if it does not exist
	profile, err := library.GetProfileFromMSISDN(db, redisConn, cfg, msisdn, "", "", src, 0, true)

	if err != nil {

		logger.Log(" could not retrieve profile %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// check balance

	insertStmt, err := tx.Prepare("INSERT INTO multi_purchase (profile_id, total_amount, purchase_type, purchase_count) VALUE (?, ?, ?, ?)")
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}
	defer insertStmt.Close()

	res, err := insertStmt.Exec(profile.ID, totalAmount, "Tokens", len(purchase))
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	purchaseID, err := res.LastInsertId()
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	for _, x := range purchase {

		// create transaction
		stmt, err := tx.Prepare("INSERT INTO utility (profile_id, transaction_type, reference, amount, status,multi_purchase_id, created) VALUE (?,2,?,?,0,?,now()) ")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		defer stmt.Close()

		_, err = stmt.Exec(profile.ID, x.Account, x.Amount, purchaseID)
		if err != nil {

			tx.Rollback()
			logger.Error("got error executing transaction %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

	}

	tx.Commit()
	// initiate STK Push

	callbackURL, err := config.GetKey(cfg, "payment", "callbackURL")
	if err != nil {

		logger.Error("got error retrieving payment->callbackURL from configs %s ", err.Error())
		callbackURL = "https://sms-savvy.co.ke/payment/callback/{transactionID}"
	}

	callbackURL = strings.Replace(callbackURL, "{transactionID}", fmt.Sprintf("MP%d", purchaseID), -1)

	paybill, err := config.GetKey(cfg, "payment", "paybill")
	if err != nil {

		logger.Error("got error retrieving payment->paybill from configs %s ", err.Error())
		paybill = "491929"
	}

	party_b, err := config.GetKey(cfg, "payment", "party_b")
	if err != nil {

		party_b = ""
	}

	paymentURL, err := config.GetKey(cfg, "payment", "paymentURL")
	if err != nil {

		logger.Error("got error retrieving payment->paymentURL from configs %s ", err.Error())
		callbackURL = "https://vas.wasiliana.co.ke/api/payment/mpesa/request"
	}

	ptoken, err := config.GetKey(cfg, "payment", "token")
	if err != nil {

		logger.Error("got error retrieving payment->token from configs %s ", err.Error())
		callbackURL = ""
	}

	var requestPayload map[string]interface{}

	if len(party_b) == 0 {

		requestPayload = map[string]interface{}{
			"callback_url":   callbackURL,
			"msisdn":         msisdn,
			"amount":         totalAmount,
			"transaction_id": fmt.Sprintf("MP%d", purchaseID),
			"paybill":        paybill,
		}

	} else {

		requestPayload = map[string]interface{}{
			"callback_url":   callbackURL,
			"msisdn":         msisdn,
			"amount":         totalAmount,
			"transaction_id": fmt.Sprintf("MP%d", purchaseID),
			"short_code":     paybill,
			"party_b":        party_b,
		}
	}

	headers := map[string]string{

		"Authorization": ptoken,
		"Content-Type":  "application/json",
		"Accept":        "application/json",
	}

	status, msg := library.HTTPPostWithHeaders(paymentURL, requestPayload, headers)

	if status != 200 {

		logger.Error("got error initiating STK Push %s ", msg)
		RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	RespondJSON(db, started, w, r, http.StatusOK, msg.Message)
	return

}

/**
@params
{
	"src":"web",
	"msisdn_to_topup":"0726120256",
	"purchase":[
		{
			"msisdn": "0726120256",
			"amount": 100
		},
		{
			"msisdn": "0726120256",
			"amount": 100
		},
	]
}
*/
func AirtimeMultiPurchase(db *sql.DB, cfg *ini.File, redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload MultiPurchase

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		logger.Error("got error here %s ",err.Error())
		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, m.Error)
		return
	}

	if len(payload.Purchase) == 0 {

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, "Missing account numbers to topup")
		return
	}

	src := payload.Src

	// params
	mobile, _ := library.GetStringValue(payload.Msisdn, "")
	mobile = library.FormatMSISDN(mobile)
	if len(mobile) == 0 {

		logger.Log(" could not retrieve mobile number to use to pay ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}
	msisdn, _ := strconv.ParseInt(mobile, 10, 64)

	token := payload.Token
	if len(token) == 0 {

		logger.Log(" could not retrieve token ")
		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	tokenKey := fmt.Sprintf("token:msisdn:%d",msisdn)
	getToken, err := library.GetRedisKey(redisConn,tokenKey)
	if err != nil {

		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if token != getToken {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	var purchase []Purchase
	totalAmount := int64(0)

	for _, a := range payload.Purchase {

		phone_number, _ := library.GetStringValue(a.Account, "0")
		mobile = library.FormatMSISDN(phone_number)
		if len(mobile) == 0 {

			logger.Log(" could not retrieve mobile number to use to pay ")
			RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number")
			return
		}
		msisdn, _ := strconv.ParseInt(mobile, 10, 64)

		amount, _ := library.GetInt64Value(a.Amount, 0)
		if amount < 0 {

			logger.Log(" could not retrieve amount ")
			RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Invalid amount supplied. Enter amount greater than 200")
			return
		}

		purchase = append(purchase, Purchase{
			Account: msisdn,
			Amount:  amount,
		})

		totalAmount = totalAmount + amount
	}

	// create profile if it does not exist
	profile, err := library.GetProfileFromMSISDN(db, redisConn, cfg, msisdn, "", "", src, 0, true)

	if err != nil {

		logger.Log(" could not retrieve profile %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// check balance

	insertStmt, err := tx.Prepare("INSERT INTO multi_purchase (profile_id, total_amount, purchase_type, purchase_count) VALUE (?, ?, ?, ?)")
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}
	defer insertStmt.Close()

	res, err := insertStmt.Exec(profile.ID, totalAmount, "Airtime", len(purchase))
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	purchaseID, err := res.LastInsertId()
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	for _, x := range purchase {

		// create transaction
		stmt, err := tx.Prepare("INSERT INTO utility (profile_id, transaction_type, reference, amount, status,multi_purchase_id, created) VALUE (?,1,?,?,0,?,now()) ")

		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		defer stmt.Close()

		_, err = stmt.Exec(profile.ID, x.Account, x.Amount, purchaseID)
		if err != nil {

			tx.Rollback()
			logger.Error("got error executing transaction %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

	}

	tx.Commit()
	// initiate STK Push

	callbackURL, err := config.GetKey(cfg, "payment", "callbackURL")
	if err != nil {

		logger.Error("got error retrieving payment->callbackURL from configs %s ", err.Error())
		callbackURL = "https://sms-savvy.co.ke/payment/callback/{transactionID}"
	}

	callbackURL = strings.Replace(callbackURL, "{transactionID}", fmt.Sprintf("MP%d", purchaseID), -1)

	paybill, err := config.GetKey(cfg, "payment", "paybill")
	if err != nil {

		logger.Error("got error retrieving payment->paybill from configs %s ", err.Error())
		paybill = "491929"
	}

	party_b, err := config.GetKey(cfg, "payment", "party_b")
	if err != nil {

		party_b = ""
	}

	paymentURL, err := config.GetKey(cfg, "payment", "paymentURL")
	if err != nil {

		logger.Error("got error retrieving payment->paymentURL from configs %s ", err.Error())
		callbackURL = "https://vas.sms-savvy.co.ke/api/payment/mpesa/request"
	}

	ptoken, err := config.GetKey(cfg, "payment", "token")
	if err != nil {

		logger.Error("got error retrieving payment->token from configs %s ", err.Error())
		callbackURL = ""
	}

	var requestPayload map[string]interface{}

	if len(party_b) == 0 {

		requestPayload = map[string]interface{}{
			"callback_url":   callbackURL,
			"msisdn":         msisdn,
			"amount":         totalAmount,
			"transaction_id": fmt.Sprintf("MP%d", purchaseID),
			"paybill":        paybill,
		}

	} else {

		requestPayload = map[string]interface{}{
			"callback_url":   callbackURL,
			"msisdn":         msisdn,
			"amount":         totalAmount,
			"transaction_id": fmt.Sprintf("MP%d", purchaseID),
			"short_code":     paybill,
			"party_b":        party_b,
		}
	}

	headers := map[string]string{
		"Authorization": ptoken,
		"Content-Type":  "application/json",
		"Accept":        "application/json",
	}

	status, msg := library.HTTPPostWithHeaders(paymentURL, requestPayload, headers)

	if status != 200 {

		logger.Error("got error initiating STK Push %s ", msg)
		RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	RespondJSON(db, started, w, r, http.StatusOK, msg.Message)
	return

}

func RedeemPoints(db *sql.DB,conn *amqp.Connection, cfg *ini.File, redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, m.Error)
		return
	}

	profile_id, _ := library.GetInt(payload, "profile_id", 0)
	if profile_id == 0 {

		logger.Log(" could not retrieve mobile number to use to pay ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}

	token, _ := library.GetString(payload, "token", "")
	if len(token) == 0 {

		logger.Log(" could not retrieve token ")
		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	tokenKey := fmt.Sprintf("token:id:%d",profile_id)
	getToken, err := library.GetRedisKey(redisConn,tokenKey)
	if err != nil {

		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if token != getToken {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	transaction_type, _ := library.GetInt(payload, "transaction_type", 1)

	account, _ := library.GetString(payload, "account", "")
	accountNumber, err := strconv.ParseInt(account, 10, 64)
	if err != nil {
		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "You have enter invalid account number")
		return
	}

	if transaction_type == 1 {

		mobile_to_top := library.FormatMSISDN(account)
		if len(mobile_to_top) == 0 {

			logger.Log(" could not retrieve mobile number to topup ")
			RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number to topup")
			return
		}
		accountNumber, _ = strconv.ParseInt(mobile_to_top, 10, 64)
	}

	// create profile if it does not exist
	profile, err := library.GetProfile(db, redisConn, profile_id)
	if err != nil {

		logger.Log(" could not retrieve profile %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if profile.Points < 50 {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "You have insufficient points")
		return
	}

	amount := profile.Points / 10
	points := amount * 10

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// check balance

	if profile.Points >= points {

		// deduct profile
		updateTx, err := db.Prepare("UPDATE profile_balance SET points = points - ? WHERE id = ? ")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		defer updateTx.Close()

		_, err = updateTx.Exec(points,profile.ID)
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		// create transaction

		createTx, err := db.Prepare("INSERT INTO points_redemption (profile_id, account, transaction_type, amount) VALUE (?, ?, ?, ? )")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}
		defer createTx.Close()

		resp, err := createTx.Exec(profile.ID,accountNumber,transaction_type,amount)
		if err != nil {

			tx.Rollback()
			logger.Error("got error executing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		redeemptionID, err := resp.LastInsertId()
		if err != nil {

			tx.Rollback()
			logger.Error("got error getting last insert ID query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}


		reference := ToHex(redeemptionID)

		network := "KPLC"

		if transaction_type == 1 {
			// get network
			network = library.GetKENetwork(cfg, fmt.Sprintf("%d",accountNumber))

			network = strings.ToUpper(network)

			if network == "TELCOM" {

				network = "TELKOM"

			}

			if network == "ORANGE" {

				network = "network"

			}
		}

		// create transaction
		stmt, err := tx.Prepare("INSERT INTO utility (profile_id, transaction_type, reference, amount,payment_id,network,purchase_type, status, created) VALUE (?,?,?,?,?,?,2,1,now()) ")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		res, err := stmt.Exec(profile.ID,transaction_type, accountNumber, amount,redeemptionID,network)
		if err != nil {

			logger.Error("got error executing transaction %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		transactionID, err := res.LastInsertId()
		if err != nil {

			logger.Error("got error retrieving last insert ID %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		_, err = library.CreateTransaction(tx,profile.ID,library.TRANSACTION_TYPE_DEBIT,transaction_type,transactionID,amount,0,fmt.Sprintf("Utility Purchase#%d",transactionID))
		if err != nil {

			tx.Rollback()
			logger.Error("got error executing transaction %s ",err.Error())
			RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
			return
		}


		tx.Commit()

		transactionType := "Airtime"

		if transaction_type == 2 {

			transactionType = "KPLC Token"
		}

		if transaction_type == 1 {

			err = library.Publish(conn, cfg, "AIRTIME", transactionID, 0)

		} else {

			err = library.Publish(conn, cfg, "KPLC_TOKEN", transactionID, 0)

		}

		if err != nil {

			logger.Error(" got error publishing to queue %s", aurora.Red(err.Error()))
			RespondJSON(db,started, w, r, http.StatusInternalServerError, "Internal server error")
			return
		}

		// send SMS
		// send SMS
		sms := library.GetSMSTemplate(db,redisConn,"REDEEM_POINTS")

		if len(sms) > 0 {

			replacements := map[string] string{
				"reference" : reference,
				"amount" : fmt.Sprintf("%d",amount),
				"points" : fmt.Sprintf("%d",points),
				"balance" : fmt.Sprintf("%d",profile.Points - points),
				"type" : transactionType,
			}

			sms = library.BuildSMS(sms,replacements)

			library.SendSMS(db,cfg,"REDEEM_POINTS",profile.Msisdn,sms)
		}

		RespondJSON(db,started, w, r, http.StatusOK, "transaction queued")
		return

	} else {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "You have insufficient points")
		return

	}

}

func AdminRedeemPoints(db *sql.DB,conn *amqp.Connection, cfg *ini.File, redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, m.Error)
		return
	}

	token, _ := library.GetString(payload, "token", "")
	if len(token) == 0 {

		logger.Log(" could not retrieve token ")
		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	profile_id, _ := library.GetInt(payload, "profile_id", 0)
	if profile_id == 0 {

		logger.Log(" could not retrieve mobile number to use to pay ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}

	getToken, err := config.GetKey(cfg,"admin","token")
	if err != nil {

		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if token != getToken {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	transaction_type, _ := library.GetInt(payload, "transaction_type", 1)
	account, _ := library.GetString(payload, "account", "")
	accountNumber, err := strconv.ParseInt(account, 10, 64)
	if err != nil {
		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "You have enter invalid account number")
		return
	}

	if transaction_type == 1 {

		mobile_to_top := library.FormatMSISDN(account)
		if len(mobile_to_top) == 0 {

			logger.Log(" could not retrieve mobile number to topup ")
			RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number to topup")
			return
		}
		accountNumber, _ = strconv.ParseInt(mobile_to_top, 10, 64)
	}

	// create profile if it does not exist
	profile, err := library.GetProfile(db, redisConn, profile_id)
	if err != nil {

		logger.Log(" could not retrieve profile %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if profile.Points < 50 {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "You have insufficient points")
		return
	}

	amount := profile.Points / 10

	points := amount * 10

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// check balance

	if profile.Points >= points {

		// deduct profile
		updateTx, err := db.Prepare("UPDATE profile_balance SET points = points - ? WHERE profile_id = ? ")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		defer updateTx.Close()

		_, err = updateTx.Exec(points,profile.ID)
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		// create transaction

		createTx, err := db.Prepare("INSERT INTO points_redemption (profile_id, account, transaction_type, amount) VALUE (?, ?, ?, ? )")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}
		defer createTx.Close()

		resp, err := createTx.Exec(profile.ID,accountNumber,transaction_type,amount)
		if err != nil {

			tx.Rollback()
			logger.Error("got error executing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		redeemptionID, err := resp.LastInsertId()
		if err != nil {

			tx.Rollback()
			logger.Error("got error getting last insert ID query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		reference := ToHex(redeemptionID)

		network := "KPLC"

		if transaction_type == 1 {
			// get network
			network = library.GetKENetwork(cfg, fmt.Sprintf("%d",accountNumber))

			network = strings.ToUpper(network)

			if network == "TELCOM" {

				network = "TELKOM"

			}

			if network == "ORANGE" {

				network = "network"

			}
		}

		// create transaction
		stmt, err := tx.Prepare("INSERT INTO utility (profile_id, transaction_type, reference, amount,payment_id,network,purchase_type, status, created) VALUE (?,?,?,?,?,?,2,1,now()) ")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		res, err := stmt.Exec(profile.ID,transaction_type, accountNumber, amount,redeemptionID,network)
		if err != nil {

			logger.Error("got error executing transaction %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		transactionID, err := res.LastInsertId()
		if err != nil {

			logger.Error("got error retrieving last insert ID %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		_, err = library.CreateTransaction(tx,profile.ID,library.TRANSACTION_TYPE_DEBIT,transaction_type,transactionID,amount,0,fmt.Sprintf("Utility Purchase#%d",transactionID))
		if err != nil {

			tx.Rollback()
			logger.Error("got error executing transaction %s ",err.Error())
			RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		tx.Commit()

		transactionType := "Airtime"

		if transaction_type == 2 {

			transactionType = "KPLC Token"
		}

		if transaction_type == 1 {

			err = library.Publish(conn, cfg, "AIRTIME", transactionID, 0)

		} else {

			err = library.Publish(conn, cfg, "KPLC_TOKEN", transactionID, 0)

		}

		if err != nil {

			logger.Error(" got error publishing to queue %s", aurora.Red(err.Error()))
			RespondJSON(db,started, w, r, http.StatusInternalServerError, "Internal server error")
			return
		}

		// send SMS
		// send SMS
		sms := library.GetSMSTemplate(db,redisConn,"REDEEM_POINTS")

		if len(sms) > 0 {

			replacements := map[string] string{
				"reference" : reference,
				"amount" : fmt.Sprintf("%d",amount),
				"points" : fmt.Sprintf("%d",points),
				"balance" : fmt.Sprintf("%d",profile.Points - points),
				"type" : transactionType,
			}

			sms = library.BuildSMS(sms,replacements)

			library.SendSMS(db,cfg,"REDEEM_POINTS",profile.Msisdn,sms)
		}

		RespondJSON(db,started, w, r, http.StatusOK, "transaction queued")
		return

	} else {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "You have insufficient points")
		return

	}

}

func AdminPurchaseAirtime(db *sql.DB,conn *amqp.Connection, cfg *ini.File, redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, m.Error)
		return
	}

	token, _ := library.GetString(payload, "token", "")
	if len(token) == 0 {

		logger.Log(" could not retrieve token ")
		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	mobile, _ := library.GetString(payload, "msisdn", "")
	mobile = library.FormatMSISDN(mobile)
	if len(mobile) == 0 {

		logger.Log(" could not retrieve mobile number to use to pay ")
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}
	msisdn, _ := strconv.ParseInt(mobile, 10, 64)

	getToken, err := config.GetKey(cfg,"admin","token")
	if err != nil {

		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if token != getToken {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	src, _ := library.GetString(payload, "src", "admin")
	transaction_type, _ := library.GetInt(payload, "transaction_type", 1)
	amount, err := library.GetInt(payload, "amount", 0)
	if err != nil  {

		logger.Log(" could not retrieve tamountoken %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing amount")
		return
	}

	if amount ==0  {

		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing amount")
		return
	}

	account, _ := library.GetString(payload, "account", "")
	accountNumber, err := strconv.ParseInt(account, 10, 64)
	if err != nil {
		
		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "You have enter invalid account number")
		return
	}

	if transaction_type == 1 {

		mobile_to_top := library.FormatMSISDN(account)
		if len(mobile_to_top) == 0 {

			logger.Log(" could not retrieve mobile number to topup ")
			RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing mobile number to topup")
			return
		}
		accountNumber, _ = strconv.ParseInt(mobile_to_top, 10, 64)
	}

	// create profile if it does not exist
	profile, err := library.GetProfileFromMSISDN(db, redisConn, cfg, msisdn, "", "", src, 0, true)
	if err != nil {

		logger.Log(" could not retrieve profile %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// check balance

	if amount > 0 {

		// create transaction

		createTx, err := db.Prepare("INSERT INTO admin_transaction (profile_id, account, transaction_type, amount) VALUE (?, ?, ?, ? )")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}
		defer createTx.Close()

		resp, err := createTx.Exec(profile.ID,accountNumber,transaction_type,amount)
		if err != nil {

			tx.Rollback()
			logger.Error("got error executing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		redeemptionID, err := resp.LastInsertId()
		if err != nil {

			tx.Rollback()
			logger.Error("got error getting last insert ID query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		reference := ToHex(redeemptionID)

		network := "KPLC"

		if transaction_type == 1 {
			// get network
			network = library.GetKENetwork(cfg, fmt.Sprintf("%d",accountNumber))

			network = strings.ToUpper(network)

			if network == "TELCOM" {

				network = "TELKOM"

			}

			if network == "ORANGE" {

				network = "network"

			}
		}

		// create transaction
		stmt, err := tx.Prepare("INSERT INTO utility (profile_id, transaction_type, reference, amount,payment_id,network,initiator,purchase_type, status, created) VALUE (?,?,?,?,?,?,?,3,1,now()) ")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		res, err := stmt.Exec(profile.ID,transaction_type, accountNumber, amount,redeemptionID,network,msisdn)
		if err != nil {

			logger.Error("got error executing transaction %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		transactionID, err := res.LastInsertId()
		if err != nil {

			logger.Error("got error retrieving last insert ID %s ", err.Error())
			RespondJSON(db, started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
			return
		}

		_, err = library.CreateTransaction(tx,profile.ID,library.TRANSACTION_TYPE_DEBIT,transaction_type,transactionID,amount,0,fmt.Sprintf("Utility Purchase#%d",transactionID))
		if err != nil {

			tx.Rollback()
			logger.Error("got error executing transaction %s ",err.Error())
			RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
			return
		}


		tx.Commit()

		transactionType := "Airtime"

		if transaction_type == 2 {

			transactionType = "KPLC Token"
		}

		if transaction_type == 1 {

			err = library.Publish(conn, cfg, "AIRTIME", transactionID, 0)

		} else {

			err = library.Publish(conn, cfg, "KPLC_TOKEN", transactionID, 0)

		}

		if err != nil {

			logger.Error(" got error publishing to queue %s", aurora.Red(err.Error()))
			RespondJSON(db,started, w, r, http.StatusInternalServerError, "Internal server error")
			return
		}

		// send SMS
		// send SMS
		sms := library.GetSMSTemplate(db,redisConn,"ADMIN_PURCHASE")

		if len(sms) > 0 {

			replacements := map[string] string{
				"reference" : reference,
				"amount" : fmt.Sprintf("%d",amount),
				"points" : fmt.Sprintf("%d",0),
				"balance" : fmt.Sprintf("%d",profile.Points),
				"type" : transactionType,
			}

			sms = library.BuildSMS(sms,replacements)

			library.SendSMS(db,cfg,"REDEEM_POINTS",profile.Msisdn,sms)
		}

		RespondJSON(db,started, w, r, http.StatusOK, "transaction queued")
		return

	} else {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "You have insufficient points")
		return

	}

}

func AdminRequeueTransaction(db *sql.DB,conn *amqp.Connection, cfg *ini.File, redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, m.Error)
		return
	}

	token, _ := library.GetString(payload, "token", "")
	if len(token) == 0 {

		logger.Log(" could not retrieve token ")
		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	utility_id, _ := library.GetInt(payload, "utility_id", 0)
	if utility_id == 0 {

		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing transaction to process")
		return
	}

	transaction_type, _ := library.GetInt(payload, "transaction_type", 0)
	if transaction_type == 0 {

		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Missing transaction type to process")
		return
	}

	getToken, err := config.GetKey(cfg,"admin","token")
	if err != nil {

		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if token != getToken {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	//reset

	stmt, err := db.Prepare("UPDATE utility SET status = 1 WHERE id = ? ")
	if err != nil {

		logger.Log(" could not update utility %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusInternalServerError, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}
	defer stmt.Close()

	_,err = stmt.Exec(utility_id)
	if err != nil {

		logger.Log(" could not update utility %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusInternalServerError, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if transaction_type == 1 {

		err = library.Publish(conn, cfg, "AIRTIME", utility_id, 0)

	} else {

		err = library.Publish(conn, cfg, "KPLC_TOKEN", utility_id, 0)

	}

	RespondJSON(db, started, w, r, http.StatusOK, "Transaction queued for processing")
	return

}
