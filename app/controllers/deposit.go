package controllers

import (
	"credo/app/library"
	"credo/app/logger"
	"credo/app/models"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/logrusorgru/aurora"
	"github.com/streadway/amqp"
	"gopkg.in/ini.v1"
	"io"
	"io/ioutil"
	"net/http"
	"redis"
	"strconv"
	"strings"
)

type Utility struct {
	ProfileID int64
	TransactionID int64
	TransactionType int64
	Reference string
	Amount int64
}

func STKPaymentCallback(db *sql.DB,cfg *ini.File,conn *amqp.Connection,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()

	vars := mux.Vars(r)
	transID := vars["transactionID"]

	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	c2bCallback := models.C2BPaymentCallback{}

	err = json.Unmarshal(body, &c2bCallback)
	if err != nil {

		logger.Error(" got error decoding payload %s ",err.Error())
		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	};

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};

	// params

	if c2bCallback.StatusCode != 3112 {

		logger.Error("Failed transaction ")
		RespondJSON(db,started,  w, r, http.StatusOK, "Failed transaction")
		return
	}

	transactionType := 1
	transactionID := int64(0)

	if strings.HasPrefix(c2bCallback.Account,"MP") {

		co := strings.Replace(c2bCallback.Account,"MP","",-1)
		_c, err := strconv.ParseInt(co,10,64)
		if err != nil {

			logger.Error("Got error converting %s to int %s",co,err.Error())
			_c = 0
		}
		transactionType = 2
		transactionID = _c

	} else {

		transactionID, _ = strconv.ParseInt(transID,10,64)

	}

	logger.Error("Got transactionID here as %d type %d",transactionID,transactionType)

	// get load transaction

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// create profile if it does not exist
	profile, err := library.GetProfileFromMSISDN(db,redisConn,cfg,c2bCallback.Msisdn,c2bCallback.FirstName,fmt.Sprintf("%s %s",c2bCallback.MiddleName,c2bCallback.LastName),c2bCallback.Method,0,true)
	if err != nil {

		tx.Rollback()
		logger.Error(" could not retrieve profile %s ",err.Error())
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	// create payment
	paymentSTMT, err := tx.Prepare("INSERT INTO payment (transaction_id,profile_id,first_name,middle_name,last_name,short_code, amount, reference, method, created) VALUE (?,?,?,?,?,?, ?, ?, ?,now()) ")
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	defer paymentSTMT.Close()

	res, err := paymentSTMT.Exec(transactionID,profile.ID,c2bCallback.FirstName,c2bCallback.MiddleName,c2bCallback.LastName,c2bCallback.ShortCode, c2bCallback.Amount, c2bCallback.Reference, c2bCallback.Method)
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	paymentID, err := res.LastInsertId()
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}
	
	var rows *sql.Rows
	var utilities []Utility

	if transactionType == 1 {

		rows,err = db.Query("SELECT id,profile_id, transaction_type, reference, amount FROM utility WHERE  id = ? AND status = 0 ", transactionID)

	} else {

		rows,err = db.Query("SELECT id,profile_id, transaction_type, reference, amount FROM utility WHERE  multi_purchase_id = ? AND status = 0 ", transactionID)

	}

	if err != nil  {

		 if err == sql.ErrNoRows {

			transaction_type := library.GetTransactionType(c2bCallback.TransactionID)
			// create transaction
			stmt, err := tx.Prepare("INSERT INTO utility (profile_id, transaction_type, reference, amount, status, created) VALUE (?,?,?,?,0,now()) ")
			if err != nil {

				tx.Rollback()
				logger.Error("got error preparing query %s ", err.Error())
				RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
				return
			}
			defer stmt.Close()

			res, err := stmt.Exec(profile.ID, transaction_type, c2bCallback.TransactionID, c2bCallback.Amount)
			if err != nil {

				tx.Rollback()
				logger.Error("got error executing transaction %s ", err.Error())
				RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
				return
			}

			transactionID, err = res.LastInsertId()
			if err != nil {

				tx.Rollback()
				logger.Error("got error retrieving last insert ID %s ", err.Error())
				RespondJSON(db, started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
				return
			}

			//reference.String = c2bCallback.Account
			utilities = append(utilities,Utility{
				ProfileID:       profile.ID,
				TransactionType: transaction_type,
				Reference:       c2bCallback.Account,
				Amount:          c2bCallback.Amount,
				TransactionID: transactionID,
			})

		} else {

			 tx.Rollback()
			 logger.Error(" could not retrieve transaction ")
			 RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "status_code field not found")
			 return
		 }

	} else {

		defer rows.Close()

		for rows.Next() {

			var id,profile_id, transaction_type, amount sql.NullInt64
			var reference sql.NullString
			err = rows.Scan(&id,&profile_id, &transaction_type, &reference, &amount)
			if err != nil {

				logger.Error("Got error retrieving transaction %s",err.Error())
				continue
			}

			utilities = append(utilities,Utility{
				ProfileID:       profile_id.Int64,
				TransactionType: transaction_type.Int64,
				Reference:       reference.String,
				Amount:          amount.Int64,
				TransactionID: id.Int64,
			})
		}

	}

	_transactionType := ""

	tx.Commit()

	// create deposit transaction
	_, err = library.CreateTransaction1(db, profile.ID, library.TRANSACTION_TYPE_CREDIT, library.REFERENCE_ID_DEPOSIT, paymentID,c2bCallback.Amount, 0, fmt.Sprintf("Deposit#%s",c2bCallback.Reference))
	if err != nil {

		logger.Error("got error executing transaction %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "status_code field not found")
		return
	}

	for _, i := range utilities {

		network := "KPLC"

		if i.TransactionType == 1 {
			// get network
			network = library.GetKENetwork(cfg, i.Reference)

			network = strings.ToUpper(network)

			if network == "TELCOM" {

				network = "TELKOM"
			}

			if network == "ORANGE" {

				network = "network"

			}
		}

		// update transaction
		transactionSTMT, err := db.Prepare("UPDATE  utility SET status = 1, payment_id = ?,network = ? WHERE id = ?  ")
		if err != nil {

			logger.Error("got error preparing query %s ", err.Error())
			continue
		}

		_, err = transactionSTMT.Exec(paymentID, network, i.TransactionID)
		if err != nil {

			transactionSTMT.Close()
			logger.Error("got error executing transaction %s ", err.Error())
			continue
		}

		transactionSTMT.Close()
		_transactionType = "Airtime"

		if i.TransactionType == 2 {

			_transactionType = "KPLC Token"
		}

		// create utility transaction
		_, err = library.CreateTransaction1(db, i.ProfileID, library.TRANSACTION_TYPE_DEBIT, i.TransactionType, i.TransactionID, i.Amount, 0, fmt.Sprintf("%s Purchase Ksh.%d",_transactionType, i.Amount))
		if err != nil {

			logger.Error("got error executing transaction %s ", err.Error())
			continue
		}

		if i.TransactionType == 1 {

			err = library.Publish(conn, cfg, "AIRTIME", i.TransactionID, 0)

		} else {

			err = library.Publish(conn, cfg, "KPLC_TOKEN", i.TransactionID, 0)

		}

		if err != nil {

			logger.Error(" got error publishing to queue %s", aurora.Red(err.Error()))
			m := models.ErrorMessage{}
			m.Error = "Could not publish to queue"
			//RespondJSON(db, started, w, r, http.StatusInternalServerError, m)
			//return
		}

	}

	// send SMS
	sms := library.GetSMSTemplate(db, redisConn, "PAYMENT_ACKNOWLEDGEMENT")

	if len(sms) > 0 {

		points := c2bCallback.Amount / 10

		replacements := map[string]string{
			"reference": c2bCallback.Reference,
			"amount":    fmt.Sprintf("%d", c2bCallback.Amount),
			"points":    fmt.Sprintf("%d", points),
			"balance":   fmt.Sprintf("%d", profile.Points+points),
			"type":      _transactionType,
		}

		sms = library.BuildSMS(sms, replacements)

		library.SendSMS(db, cfg, "PAYMENT_ACKNOWLEDGEMENT", c2bCallback.Msisdn, sms)
	}

	RespondJSON(db,started, w, r, http.StatusOK, "transaction queued")
	return
}


func TokensCallback(db *sql.DB,cfg *ini.File,conn *amqp.Connection,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()

	var payload models.KyandaCallback

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))
	if err != nil {

		logger.Error("error reading body %s ",err.Error())
		RespondRaw(db,started,w, r,http.StatusOK,map[string]string{
			"status": "success",
		})
		return
	}

	if err := r.Body.Close(); err != nil {

		logger.Error("error reading body %s ",err.Error())
		RespondRaw(db,started,w, r,http.StatusOK,map[string]string{
			"status": "success",
		})
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		logger.Error("error converting %s to required payload %s ",string(body),err.Error())
		RespondRaw(db,started,w, r,http.StatusOK,map[string]string{
			"status": "success",
		})
		return
	};

	jsn, _ := json.Marshal(payload)

	logger.Error("Got payload %s ",string(jsn))

	TransactionRef := payload.TransactionRef

	var id,amount,initiator,transaction_type sql.NullInt64
	var reference sql.NullString

	err = db.QueryRow("SELECT u.id,u.amount,u.initiator,u.reference,u.transaction_type FROM utility u INNER JOIN api_request a ON u.id = a.transaction_id WHERE a.external_reference = ? ",TransactionRef).Scan(&id,&amount,&initiator,&reference,&transaction_type)
	if err != nil {

		logger.Error("error retrieving customer details %s ",err.Error())
		RespondRaw(db,started,w, r,http.StatusOK,map[string]string{
			"status": "success",
		})
		return
	}

	tokens := strings.ReplaceAll(strings.TrimSpace(payload.Details.Tokens)," ","-")
	units := payload.Details.Units

	updateStmt, err := db.Prepare("UPDATE utility SET description = ? WHERE id = ? ")
	if err != nil {

		logger.Error("error retrieving customer details %s ",err.Error())
		RespondRaw(db,started,w, r,http.StatusInternalServerError, "Failed")
		return
	}
	defer updateStmt.Close()

	_, err = updateStmt.Exec(fmt.Sprintf("%s - %s",payload.TransactionRef,payload.Message),id.Int64)
	if err != nil {

		logger.Error("error retrieving customer details %s ",err.Error())
	}

	if transaction_type.Int64 == 2 {

		if len(tokens) == 0 {

			stmt, err := db.Prepare("UPDATE utility SET description = ?, status = -1 WHERE id = ?  ")
			if err != nil {

				logger.Error("error retrieving customer details %s ", err.Error())
				RespondRaw(db, started, w, r, http.StatusInternalServerError, "Failed")
				return
			}
			defer stmt.Close()

			_, err = stmt.Exec(fmt.Sprintf("FAILED with %s", string(jsn)), id.Int64)
			if err != nil {

				logger.Error("error retrieving customer details %s ", err.Error())
			}

			RespondRaw(db, started, w, r, http.StatusOK, map[string]string{
				"status": "success",
			})
			return
		}

		sms := fmt.Sprintf("KPLC Token\nMtrNo: %s\nToken: %s\nUnits: %s\nToken Amount: %d", reference.String, tokens, units, amount.Int64)

		stmt, err := db.Prepare("UPDATE utility SET description = ? WHERE id = ? ")
		if err != nil {

			logger.Error("error retrieving customer details %s ", err.Error())
			RespondRaw(db, started, w, r, http.StatusInternalServerError, "Failed")
			return
		}
		defer stmt.Close()

		_, err = stmt.Exec(sms, id.Int64)
		if err != nil {

			logger.Error("error retrieving customer details %s ", err.Error())
		}

		library.SendSMS(db, cfg, "TOKENS", initiator.Int64, sms)
	}

	RespondRaw(db,started,w, r,http.StatusOK,map[string]string{
		"status": "success",
	})
	return

}

/**
{
    "short_code": 491919,
    "client_id": 5,
    "msisdn": 254726120256,
    "amount": 200,
    "reference": "QWERTY",
    "method": "LIPA_NA_MPESA",
    "reference_id": 1,
    "transaction_id": "123456789",
    "first_name": "First",
    "middle_name": "Middle",
    "last_name": "Last",
    "account": "123456789",
    "status_code": 3112,
    "retries": 0,
    "status_description": "OK",
    "result_description": "OK",
    "paybill_balance": "0",
    "payment_id": 1
}
 */
func Payment(db *sql.DB,cfg *ini.File,conn *amqp.Connection,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()

	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	c2bCallback := models.C2BPaymentCallback{}

	err = json.Unmarshal(body, &c2bCallback)
	if err != nil {

		logger.Error(" got error decoding payload %s ",err.Error())
		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	};

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};

	if len(c2bCallback.TransactionID) == 0 {

		c2bCallback.TransactionID = c2bCallback.Account
	}

	//j, _ := json.Marshal(c2bCallback)

	//logger.Info("Got payload %s ",string(j))
	// params

	if c2bCallback.StatusCode != 3112 {

		logger.Error("Failed transaction ")
		RespondJSON(db,started,  w, r, http.StatusOK, "Failed transaction")
		return
	}

	// get load transaction

	var profile_id, transaction_type sql.NullInt64
	var reference sql.NullString

	tx, err := db.Begin()
	if err != nil {

		tx.Rollback()
		logger.Error("got error starting db transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// create profile if it does not exist
	profile, err := library.GetProfileFromMSISDN(db,redisConn,cfg,c2bCallback.Msisdn,c2bCallback.FirstName,fmt.Sprintf("%s %s",c2bCallback.MiddleName,c2bCallback.LastName),c2bCallback.Method,0,true)
	if err != nil {

		tx.Rollback()
		logger.Log(" could not retrieve profile %s ",err.Error())
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	profile_id.Int64 = profile.ID

	// reference
	ms := library.FormatMSISDN(c2bCallback.TransactionID)

	if len(ms) == 0 {

		transaction_type.Int64 = library.GetTransactionType(c2bCallback.TransactionID)

		if transaction_type.Int64 == 2 {

			reference.String = c2bCallback.TransactionID
		}

	} else {

		transaction_type.Int64 = 1
		reference.String = ms
	}

	// create transaction
	stmt, err := tx.Prepare("INSERT INTO utility (profile_id, transaction_type, reference, amount, initiator, status, created) VALUE (?,?,?,?,?,0,now()) ")
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}
	defer stmt.Close()

	res, err := stmt.Exec(profile.ID,transaction_type.Int64,reference.String,c2bCallback.Amount,c2bCallback.Msisdn)
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	transactionID, err := res.LastInsertId()
	if err != nil {

		tx.Rollback()
		logger.Error("got error retrieving last insert ID %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	logger.Error("wants to insert into payment ")

	// create payment
	paymentSTMT, err := tx.Prepare("INSERT INTO payment (transaction_id,profile_id,first_name,middle_name,last_name,short_code, amount, reference, method, created) VALUE (?,?,?,?,?,?, ?, ?, ?,now()) ")
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	defer paymentSTMT.Close()

	res, err = paymentSTMT.Exec(transactionID,profile_id.Int64,c2bCallback.FirstName,c2bCallback.MiddleName,c2bCallback.LastName,c2bCallback.ShortCode, c2bCallback.Amount, c2bCallback.Reference, c2bCallback.Method)
	if err != nil {

		tx.Rollback()
		//logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	paymentID, err := res.LastInsertId()
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	network := "KPLC"

	if transaction_type.Int64 == 1 {
		// get network
		network = library.GetKENetwork(cfg, reference.String)

		network = strings.ToUpper(network)

		if network == "TELCOM" {

			network = "TELKOM"

		}

		if network == "ORANGE" {

			network = "network"

		}
	}


	logger.Error("wants to update utility ")

	// update transaction
	transactionSTMT, err := tx.Prepare("UPDATE  utility SET status = 1, payment_id = ?, network = ? WHERE id = ?  ")
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	defer transactionSTMT.Close()

	_, err = transactionSTMT.Exec(paymentID,network,transactionID)
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	_, err = library.CreateTransaction(tx,profile_id.Int64,library.TRANSACTION_TYPE_CREDIT,library.REFERENCE_ID_DEPOSIT,paymentID,c2bCallback.Amount,0,fmt.Sprintf("Deposit#%s",reference.String))
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}


	_, err = library.CreateTransaction(tx,profile_id.Int64,library.TRANSACTION_TYPE_DEBIT,transaction_type.Int64,paymentID,c2bCallback.Amount,0,fmt.Sprintf("Utility Purchase#%d",transactionID))
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	tx.Commit()

	transactionType := "Airtime"

	if transaction_type.Int64 == 2 {

		transactionType = "KPLC Token"
	}

	// send SMS

	if transaction_type.Int64 == 1 {

		err = library.Publish(conn, cfg, "AIRTIME", transactionID, 0)

	} else {

		err = library.Publish(conn, cfg, "KPLC_TOKEN", transactionID, 0)

	}

	if err != nil {

		logger.Error(" got error publishing to queue %s", aurora.Red(err.Error()))
		m := models.ErrorMessage{}
		m.Error = "Could not publish to queue"
		RespondJSON(db,started, w, r, http.StatusInternalServerError, m)
		return
	}

	sms := library.GetSMSTemplate(db,redisConn,"PAYMENT_ACKNOWLEDGEMENT")

	if len(sms) > 0 {

		points := c2bCallback.Amount / 10

		replacements := map[string] string{
			"reference" : c2bCallback.Reference,
			"amount" : fmt.Sprintf("%d",c2bCallback.Amount),
			"points" : fmt.Sprintf("%d",points),
			"balance" : fmt.Sprintf("%d",profile.Points + points),
			"type" : transactionType,
		}

		sms = library.BuildSMS(sms,replacements)

		library.SendSMS(db,cfg,"PAYMENT_ACKNOWLEDGEMENT",c2bCallback.Msisdn,sms)
	}

	RespondJSON(db,started, w, r, http.StatusOK, "transaction queued")
	return
}

/**
{
	"status":"",
	"message":""
}
 */
func ReversalCallback(db *sql.DB,cfg *ini.File,conn *amqp.Connection,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()

	vars := mux.Vars(r)
	transID := vars["reversalID"]
	transactionID, _ := strconv.ParseInt(transID,10,64)

	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	c2bCallback := models.ReversalCallback{}

	err = json.Unmarshal(body, &c2bCallback)
	if err != nil {

		logger.Error(" got error decoding payload %s ",err.Error())
		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	};

	j, _ := json.Marshal(c2bCallback)

	logger.Info(" GOT PAYBILL_ANALYTICS_QUEUE payload %s ",string(j))

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};

	logger.Error(" GOT DEPOSIT PAYLOAD AS %s ",payload)

	// params
	stUpdate, err := db.Prepare("UPDATE reversal SET status = ?, description = ? WHERE id = ? ")
	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	defer stUpdate.Close()

	if c2bCallback.Status == 1 {

		_, err = stUpdate.Exec(-1,c2bCallback.Message,transactionID)

	} else {

		_, err = stUpdate.Exec(1,c2bCallback.Message,transactionID)

	}

	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	RespondJSON(db,started, w, r, http.StatusOK, "OK")
	return
}

/**
{
    "short_code": 491919,
    "client_id": 5,
    "msisdn": 254726120256,
    "amount": 200,
    "reference": "QWERTY",
    "method": "LIPA_NA_MPESA",
    "reference_id": 1,
    "transaction_id": "123456789",
    "first_name": "First",
    "middle_name": "Middle",
    "last_name": "Last",
    "account": "123456789",
    "status_code": 3112,
    "retries": 0,
    "status_description": "OK",
    "result_description": "OK",
    "paybill_balance": "0",
    "payment_id": 1
}
*/
func PostPaid(db *sql.DB,cfg *ini.File,conn *amqp.Connection,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()

	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	c2bCallback := models.C2BPaymentCallback{}

	err = json.Unmarshal(body, &c2bCallback)
	if err != nil {

		logger.Error(" got error decoding payload %s ",err.Error())
		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	};

	j, _ := json.Marshal(c2bCallback)

	logger.Info(" GOT PAYBILL_ANALYTICS_QUEUE payload %s ",string(j))

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};

	logger.Error(" GOT DEPOSIT PAYLOAD AS %s ",payload)

	// params

	if c2bCallback.StatusCode != 3112 {

		logger.Error("Failed transaction ")
		RespondJSON(db,started,  w, r, http.StatusOK, "Failed transaction")
		return
	}

	// get load transaction

	var profile_id, transaction_type sql.NullInt64
	var reference sql.NullString

	tx, err := db.Begin()
	if err != nil {

		tx.Rollback()
		logger.Error("got error starting db transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// create profile if it does not exist
	profile, err := library.GetProfileFromMSISDN(db,redisConn,cfg,c2bCallback.Msisdn,c2bCallback.FirstName,fmt.Sprintf("%s %s",c2bCallback.MiddleName,c2bCallback.LastName),c2bCallback.Method,0,true)
	if err != nil {

		tx.Rollback()
		logger.Log(" could not retrieve profile %s ",err.Error())
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	profile_id.Int64 = profile.ID

	// reference
	ms := library.FormatMSISDN(c2bCallback.TransactionID)

	if len(ms) == 0 {

		transaction_type.Int64 = library.GetTransactionType(c2bCallback.TransactionID)

		if transaction_type.Int64 == 2 {

			reference.String = c2bCallback.TransactionID
		}

	} else {

		transaction_type.Int64 = 1
		reference.String = ms
	}

	// create transaction
	stmt, err := tx.Prepare("INSERT INTO utility (profile_id, transaction_type, reference, amount, status, created) VALUE (?,?,?,?,0,now()) ")
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}
	defer stmt.Close()

	res, err := stmt.Exec(profile.ID,transaction_type.Int64,reference.String,c2bCallback.Amount)
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	transactionID, err := res.LastInsertId()
	if err != nil {

		tx.Rollback()
		logger.Error("got error retrieving last insert ID %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	// create payment
	paymentSTMT, err := tx.Prepare("INSERT INTO payment (transaction_id,profile_id,first_name,middle_name,last_name,short_code, amount, reference, method, created) VALUE (?,?,?,?,?,?, ?, ?, ?,now()) ")
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	defer paymentSTMT.Close()

	res, err = paymentSTMT.Exec(transactionID,profile_id.Int64,c2bCallback.FirstName,c2bCallback.MiddleName,c2bCallback.LastName,c2bCallback.ShortCode, c2bCallback.Amount, c2bCallback.Reference, c2bCallback.Method)
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	paymentID, err := res.LastInsertId()
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	network := "KPLC"

	if transaction_type.Int64 == 1 {
		// get network
		network = library.GetKENetwork(cfg, reference.String)

		network = strings.ToUpper(network)

		if network == "TELCOM" {

			network = "TELKOM"

		}

		if network == "ORANGE" {

			network = "network"

		}
	}

	// update transaction
	transactionSTMT, err := tx.Prepare("UPDATE  utility SET status = 1, payment_id = ?, network = ? WHERE id = ?  ")
	if err != nil {

		tx.Rollback()
		logger.Error("got error preparing query %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	defer transactionSTMT.Close()

	_, err = transactionSTMT.Exec(paymentID,network,transactionID)
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	_, err = library.CreateTransaction(tx,profile_id.Int64,library.TRANSACTION_TYPE_CREDIT,library.REFERENCE_ID_DEPOSIT,paymentID,c2bCallback.Amount,0,fmt.Sprintf("Deposit#%s",reference.String))
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	_, err = library.CreateTransaction(tx,profile_id.Int64,library.TRANSACTION_TYPE_DEBIT,transaction_type.Int64,paymentID,c2bCallback.Amount,0,fmt.Sprintf("Utility Purchase#%d",transactionID))
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	tx.Commit()

	transactionType := "Airtime"

	if transaction_type.Int64 == 2 {

		transactionType = "KPLC Token"
	}

	// send SMS
	go func() {

		// send SMS
		sms := library.GetSMSTemplate(db,redisConn,"PAYMENT_ACKNOWLEDGEMENT")

		if len(sms) > 0 {

			replacements := map[string] string{
				"reference" : c2bCallback.Reference,
				"amount" : fmt.Sprintf("%d",c2bCallback.Amount),
				"type" : transactionType,
			}

			sms = library.BuildSMS(sms,replacements)

			library.SendSMS(db,cfg,"PAYMENT_ACKNOWLEDGEMENT",c2bCallback.Msisdn,sms)
		}

	}()

	if transaction_type.Int64 == 1 {

		err = library.Publish(conn, cfg, "AIRTIME", transactionID, 0)

	} else {

		err = library.Publish(conn, cfg, "KPLC_TOKEN", transactionID, 0)

	}

	if err != nil {

		logger.Error(" got error publishing to queue %s", aurora.Red(err.Error()))
		m := models.ErrorMessage{}
		m.Error = "Could not publish to queue"
		RespondJSON(db,started, w, r, http.StatusInternalServerError, m)
		return
	}

	RespondJSON(db,started, w, r, http.StatusOK, "transaction queued")
	return
}
