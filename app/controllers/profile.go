package controllers

import (
	"credo/app/library"
	"credo/app/logger"
	"credo/app/models"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"gopkg.in/ini.v1"
	"io"
	"io/ioutil"
	"net/http"
	"redis"
	"strconv"
)

// Implement length-based sort with ByLen type.

func ProfileBalance(db *sql.DB,redisConn *redis.Client, w http.ResponseWriter, r *http.Request)  {

	started := library.GetTime()
	vars := mux.Vars(r)
	profile_id := vars["profile_id"]

	profileID, err := strconv.Atoi(profile_id)
	if err != nil {

		RespondJSON(db,started, w, r, http.StatusInternalServerError,"Profile data could not be loaded now")
		return
	}

	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db, started, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db, started, w, r, http.StatusUnprocessableEntity, m.Error)
		return
	}


	token, _ := library.GetString(payload, "token", "")
	if len(token) == 0 {

		logger.Log(" could not retrieve token ")
		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	tokenKey := fmt.Sprintf("token:id:%d",profileID)
	getToken, err := library.GetRedisKey(redisConn,tokenKey)
	if err != nil {

		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if token != getToken {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	profile, err := library.GetProfile(db,redisConn,int64(profileID))
	if err != nil {

		RespondJSON(db,started, w, r, http.StatusInternalServerError,"Profile data could not be loaded now")
		return
	}

	p := *profile

	RespondJSON(db,started, w, r, http.StatusOK, p)
	return
}
/**
@params
{
    "src":"web",
    "msisdn":"0726120256",
    "password":"1234"
}

 */
func SignUp(db *sql.DB,cfg *ini.File,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};

	// params

	src,_ := library.GetString(payload,"src","web")

	mobile,_ := library.GetString(payload,"msisdn","")

	mobile = library.FormatMSISDN(mobile)
	if len(mobile) == 0 {

		logger.Log(" could not retrieve mobile number ")
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}

	msisdn,_:= strconv.ParseInt(mobile,10,64)

	password,_ := library.GetString(payload,"password","")
	if len(password) == 0 {

		logger.Log(" could not retrieve password ")
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Invalid password supplied")
		return
	}

	// generate code
	code := library.VerificationCode()

	// do registration
	_, err = library.CreateProfile(db,cfg,redisConn,msisdn,password,code,"","",src,0)
	if err != nil {

		logger.Error("Error starting db transaction %s ",err.Error())
		RespondJSON(db,started,  w, r, http.StatusInternalServerError, err.Error())
		return
	}

	RespondJSON(db,started, w, r, http.StatusOK, "Account created, please verify your password")
	return
}

/**
@params
{
    "msisdn":"0726120256",
    "code":"1234"
}

 */

func Verify(db *sql.DB,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};

	// params

	mobile,_ := library.GetString(payload,"msisdn","")
	mobile = library.FormatMSISDN(mobile)
	if len(mobile) == 0 {

		logger.Log(" could not retrieve mobile number ")
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}
	msisdn,_:= strconv.ParseInt(mobile,10,64)

	code,_ := library.GetString(payload,"code","")
	if len(code) == 0 {

		logger.Log(" could not retrieve password ")
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Invalid password supplied")
		return
	}

	// check code



	// check code
	var profileID sql.NullInt64

	err = db.QueryRow("SELECT id FROM profile WHERE msisdn = ? AND code = ? AND status = 1 ",msisdn,code).Scan(&profileID)

	if err == sql.ErrNoRows || !profileID.Valid{

		RespondJSON(db,started, w, r, http.StatusNotFound, "Invalid account verification code")
		return
	}

	if err != nil {

		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	profile, err := library.GetProfile(db,redisConn,profileID.Int64)
	if err != nil {

		logger.Error("got error retrieving profile details %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	p := *profile

	RespondJSON(db,started, w, r, http.StatusOK, p)
	return
}

/**
@params
{
    "msisdn":"0726120256",
    "password":"1234"
}

 */
func Login(db *sql.DB,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};

	// params

	mobile,_ := library.GetString(payload,"msisdn","")
	mobile = library.FormatMSISDN(mobile)
	if len(mobile) == 0 {

		logger.Log(" could not retrieve mobile number ")
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}
	msisdn,_:= strconv.ParseInt(mobile,10,64)

	code,_ := library.GetString(payload,"password","")
	if len(code) == 0 {

		logger.Log(" could not retrieve password ")
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Invalid password supplied")
		return
	}

	// check code
	var profileID sql.NullInt64
	err = db.QueryRow("SELECT id FROM profile WHERE msisdn = ? AND password = MD5(?) AND status = 1 ",msisdn,code).Scan(&profileID)

	if err == sql.ErrNoRows || !profileID.Valid {

		RespondJSON(db,started, w, r, http.StatusNotFound, "Invalid mobile number or password ")
		return
	}

	if err != nil {

		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}


	profile, err := library.GetProfile(db,redisConn,profileID.Int64)
	if err != nil {

		logger.Error("got error retrieving profile details %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	p := *profile

	tokenKey := fmt.Sprintf("token:id:%d",p.ID)
	token := library.ProfileToken(msisdn)
	err = library.SetRedisKey(redisConn,tokenKey,token)
	if err != nil {

		logger.Error("got error saving profile token %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	tokenKey = fmt.Sprintf("token:msisdn:%d",p.Msisdn)
	err = library.SetRedisKey(redisConn,tokenKey,token)
	if err != nil {

		logger.Error("got error saving profile token %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	uProfile := models.ProfileUI{
		FirstName: p.FirstName,
		OtherName: p.OtherName,
		Token:     token,
		Msisdn:    p.Msisdn,
		ID:        p.ID,
		Balance:   p.Balance,
		Points:    p.Points,
		Status:    p.Status,
		Created:   p.Created,
	}

	RespondJSON(db,started, w, r, http.StatusOK, uProfile)
	return
}

/**
@params
{
    "msisdn":"0726120256",
    "password":"1234",
	"code":"1234"
}

*/
func ChangePassword(db *sql.DB,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {
		logger.Error("got error reading body %s ",err.Error())
		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		logger.Error("got error closing body %s ",err.Error())
		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};

	// params

	mobile,_ := library.GetString(payload,"msisdn","")
	mobile = library.FormatMSISDN(mobile)
	if len(mobile) == 0 {

		logger.Log(" could not retrieve mobile number ")
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}
	msisdn,_:= strconv.ParseInt(mobile,10,64)

	cd,_ := library.GetString(payload,"code","")
	if len(cd) == 0 {

		logger.Log(" could not retrieve code ")
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Invalid password supplied")
		return
	}
	code,_:= strconv.ParseInt(cd,10,64)

	password,_ := library.GetString(payload,"password","")
	if len(password) == 0 {

		logger.Log(" could not retrieve password ")
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Invalid password supplied")
		return
	}

	logger.Error("got reset code %d and msisdn %d ",code,msisdn)

	// check if password matches

	var check sql.NullInt64

	err = db.QueryRow("SELECT id FROM profile WHERE msisdn = ? AND code = ? ",msisdn,code).Scan(&check)
	if err != nil  {

		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "Invalid reset code ")
		return
	}

	if !check.Valid || check.Int64 < 1 {

		RespondJSON(db,started, w, r, http.StatusNotFound, "Invalid reset code ")
		return
	}

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}
	// update password
	stmt, err := tx.Prepare("UPDATE profile SET password = MD5(?),code = ? WHERE msisdn = ? AND code = ? ")
	if err != nil {

		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	res, err := stmt.Exec(password,code,msisdn,code)
	if err != nil {

		stmt.Close()
		tx.Rollback()
		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	_, err = res.RowsAffected()
	if err != nil {

		tx.Rollback()
		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	stmt.Close()

	tx.Commit()

	RespondJSON(db,started, w, r, http.StatusOK, "Password changed successfully please login again")
	return
}

/**
@params
{
    "msisdn":"0726120256"
}

*/
func ResetPassword(db *sql.DB,cfg *ini.File,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		logger.Error("got error reading body %s ",err.Error())
		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		logger.Error("got error closing body %s ",err.Error())
		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};

	// params

	mobile,_ := library.GetString(payload,"msisdn","")
	mobile = library.FormatMSISDN(mobile)
	if len(mobile) == 0 {

		logger.Log(" could not retrieve mobile number ")
		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Missing mobile number")
		return
	}
	msisdn,_:= strconv.ParseInt(mobile,10,64)

	code := library.VerificationCode()

	// update password
	stmt, err := db.Prepare("UPDATE profile SET code = ? WHERE msisdn = ? ")
	if err != nil {

		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	res, err := stmt.Exec(code,msisdn)
	if err != nil {

		stmt.Close()
		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	stmt.Close()

	affectedRows, err := res.RowsAffected()
	if err != nil {

		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	if affectedRows > 0 {

		var name sql.NullString

		db.QueryRow("SELECT first_name FROM profile WHERE msisdn = ? ",msisdn).Scan(&name)
		if err != nil {

			logger.Error("got error retrieving profile first_name %s ",err.Error())
			name.String = "Rafiki"
		}

		// retrieve SMS template
		templateName := "PASSWORD-RESET"
		sms := library.GetSMSTemplate(db,redisConn,templateName)

		if !name.Valid || len(name.String) == 0 {

			name.String = "Rafiki"
		}

		replacements := map[string] string{
			"name" : name.String,
			"code" : fmt.Sprintf("%d",code),
		}

		sms = library.BuildSMS(sms,replacements)

		library.SendSMS(db,cfg,"PASSWORD-RESET",msisdn,sms)
	}

	RespondJSON(db,started, w, r, http.StatusOK, "If your account exists a password reset code has been send")
	return


	// check code
	var profileID sql.NullInt64
	err = db.QueryRow("SELECT id FROM profile WHERE msisdn = ? AND code = ? AND status = 1 ",msisdn).Scan(&profileID)

	if err == sql.ErrNoRows || !profileID.Valid{

		RespondJSON(db,started, w, r, http.StatusNotFound, "Invalid account verification code")
		return
	}

	if err != nil {

		logger.Error("got error verifying account %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	profile, err := library.GetProfile(db,redisConn,profileID.Int64)
	if err != nil {

		logger.Error("got error retrieving profile details %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusNotFound, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	p := *profile

	RespondJSON(db,started, w, r, http.StatusOK, p)
	return
}

func Transactions(db *sql.DB,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()
	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};

	// params

	profile_id,err := library.GetFloat(payload,"profile_id",0)
	if profile_id == 0 {

		RespondJSON(db,started,  w, r, http.StatusUnprocessableEntity, "Missing profileID")
		return
	}

	token, _ := library.GetString(payload, "token", "")
	if len(token) == 0 {

		logger.Log(" could not retrieve token ")
		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}

	tokenKey := fmt.Sprintf("token:id:%d",int64(profile_id))
	getToken, err := library.GetRedisKey(redisConn,tokenKey)
	if err != nil {

		logger.Log(" could not retrieve token %s ", err.Error())
		RespondJSON(db, started, w, r, http.StatusUnprocessableEntity, "Sorry we cannot complete your requestat the moment please try again later")
		return
	}

	if token != getToken {

		RespondJSON(db, started, w, r, http.StatusUnauthorized, "Unauthorized request")
		return
	}



	redisKey := fmt.Sprintf("PROFILE:UTILITIES:P-%0.f",profile_id)
	var profileCache map[string]interface{}


	dt, err := library.GetRedisKey(redisConn,redisKey)

	if err == nil && len(dt) > 0 {

		if err := json.Unmarshal([]byte(dt), &profileCache); err == nil {

			RespondJSON(db,started, w, r, http.StatusOK, profileCache)
			return
		};

	}

	rows, err := db.Query("SELECT  id,reference, IF(transaction_type = 1,'AIRTIME','KPLC-TOKEN') as transaction_type, amount, status, created FROM utility WHERE profile_id = ? ORDER BY id DESC ",int64(profile_id))
	if err != nil {

		logger.Error("got preparing DB query %s ",err.Error())
		RespondJSON(db,started, w, r, http.StatusInternalServerError, "We sorry we cannot complete your request at the moment, try again later")
		return
	}

	var response []models.ProfileTransaction

	for rows.Next() {

		var id, amount, status sql.NullInt64
		var reference,transaction_type sql.NullString

		var created mysql.NullTime


		err = rows.Scan(&id,&reference,&transaction_type,&amount,&status,&created)
		if err != nil {

			logger.Error("got fetching results DB query %s ",err.Error())

		} else {

			pick := models.ProfileTransaction{

				ID: id.Int64,
				Reference : reference.String,
				TransactionType : transaction_type.String,
				Amount : amount.Int64,
				Status : status.Int64,
				Created : library.ToMysql(created.Time),
			}

			response = append(response,pick)
		}
	}
	rows.Close()

	s, err := json.Marshal(response)
	if err != nil {

		logger.Error("got error converting data to string %s ",err.Error())

	} else {

		library.SetRedisKeyWithExpiry(redisConn,redisKey,string(s),30)
	}

	RespondJSON(db,started, w, r, http.StatusOK, response)
	return
}

func Template(db *sql.DB,redisConn *redis.Client, w http.ResponseWriter, r *http.Request) {

	started := library.GetTime()

	var payload map[string]interface{}

	// gets payload
	body, err := ioutil.ReadAll(io.Reader(r.Body))

	if err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := r.Body.Close(); err != nil {

		RespondError(db,started,w, r,http.StatusInternalServerError, err.Error())
		return
	}

	if err := json.Unmarshal(body, &payload); err != nil {

		m := models.ErrorMessage{}
		m.Error = "invalid values passed"
		//msg, _ := json.Marshal(m)

		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, m.Error)
		return
	};


	b, err := json.Marshal(payload)
	if err != nil {

		logger.Error("got errors %s ",err.Error())

	} else {

		logger.Error("# Update template PAYLOAD AS %s ", string(b))
	}

	message, err := library.GetString(payload,"message","")
	if err != nil {

		logger.Error(" got error retrieving message %s ",err.Error())
		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, "invalid link_id")
		return
	}

	name, err := library.GetString(payload,"name","")
	if err != nil {

		logger.Error(" got error retrieving name %s ",err.Error())
		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, "invalid name")
		return
	}

	err = library.SetRedisKey(redisConn,name,message)

	if err != nil {

		logger.Error(" got error retrieving name %s ",err.Error())
		RespondRaw(db,started,w, r,http.StatusUnprocessableEntity, err.Error())
		return
	}

	RespondJSON(db,started, w, r, http.StatusOK, "Template saved")
	return
}
