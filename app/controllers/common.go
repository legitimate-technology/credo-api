package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/logrusorgru/aurora"
	"net/http"
	"credo/app/library"
	"credo/app/logger"
	"credo/app/models"
	"time"
)

type RequestResonse struct {
	Path       string
	Age        int64
	StatusCode int
	Created    *time.Time
}

// RespondJSON makes the response with payload as json format
func response(db *sql.DB,started int64, w http.ResponseWriter, r *http.Request, status int, payload []byte) {

	//requestResonse := RequestResonse{r.URL.Path, library.GetTime() - started, status, nil}

	tt := (library.GetTime() - started ) / 1
	s := fmt.Sprintf("path %s status code %d response time %d ms ", r.URL.Path, aurora.Blue(status), aurora.Green(tt))

	logger.Info("%s", s)
/*
	sqlQuery := "INSERT INTO api_request (path,status,time_taken,created) VALUE (?,?,?,now()) "

	st, err := db.Prepare(sqlQuery)
	defer st.Close()

	if err != nil {

		logger.Error("got error saving api_request %s ",err.Error())

	} else {

		_, err = st.Exec(r.URL.Path,status,tt)
		if err != nil {

			logger.Error("got error saving api_request %s ",err.Error())

		}
	}
*/
	//now := library.ToNairobiTime(time.Now())
	//logger.Info("got nairobi now time is %s ",now)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(status)
	w.Write(payload)
}

// respondError makes the error response with payload as json format
func RespondError(db *sql.DB,started int64, w http.ResponseWriter, r *http.Request, code int, message string) {

	m := models.ErrorMessage{}
	m.Error = message

	res := models.ResponseMessage{}
	res.Status = code
	res.Message = m
	msg, _ := json.Marshal(res)
	response(db,started, w, r, code, msg)
}

// respondError makes the error response with payload as json format
/*
func RespondSuccess(started int64, w http.ResponseWriter, r *http.Request, code int, message string) {

	res := models.ResponseMessage{}
	res.Status = code
	res.Message = message
	msg, _ := json.Marshal(res)
	response(started, w, r, code, msg)
}
*/
// respondError makes the error response with payload as json format
func Response(db *sql.DB,started int64, w http.ResponseWriter, r *http.Request, code int, message models.ResponseMessage) {

	msg, _ := json.Marshal(message)
	response(db,started, w, r, code, msg)
}

// respondError makes the error response with payload as json format
func RespondJSON(db *sql.DB,started int64, w http.ResponseWriter, r *http.Request, code int, message interface{}) {

	res := models.ResponseMessage{}
	res.Status = code
	res.Message = message
	msg, _ := json.Marshal(res)
	response(db,started, w, r, code, msg)
}

// respondError makes the error response with payload as json format
func RespondRaw(db *sql.DB,started int64, w http.ResponseWriter, r *http.Request, code int, message interface{}) {

	msg, _ := json.Marshal(message)
	response(db,started, w, r, code, msg)
}
