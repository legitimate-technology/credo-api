package models

type Profile struct {
	FirstName string `json:"first_name"`
	OtherName string `json:"other_name"`
	Msisdn    int64  `json:"msisdn"`
	ID        int64  `json:"id"`
	Balance   int64  `json:"balance"`
	Points    int64  `json:"points"`
	Status    int64  `json:"status"`
	Created   string `json:"created"`
}

type ProfileUI struct {
	FirstName string `json:"first_name"`
	OtherName string `json:"other_name"`
	Token string `json:"token"`
	Msisdn    int64  `json:"msisdn"`
	ID        int64  `json:"id"`
	Balance   int64  `json:"balance"`
	Points    int64  `json:"points"`
	Status    int64  `json:"status"`
	Created   string `json:"created"`
}

type ProfileTransaction struct {
	Amount          int64  `json:"amount"`
	ID              int64  `json:"id"`
	TransactionType string `json:"transaction_type"`
	Reference       string `json:"reference"`
	Status          int64  `json:"status"`
	Created         string `json:"created"`
}

type C2BPaymentCallback struct {
	ShortCode         int64  `json:"short_code"`
	ClientID          int64  `json:"client_id"`
	Msisdn            int64  `json:"msisdn"`
	Amount            int64  `json:"amount"`
	Reference         string `json:"reference"`
	Method            string `json:"method"`
	ReferenceID       int64  `json:"reference_id"`
	TransactionID     string `json:"transaction_id"`
	FirstName         string `json:"first_name"`
	MiddleName        string `json:"middle_name"`
	LastName          string `json:"last_name"`
	Account           string `json:"account"`
	StatusCode        int    `json:"status_code"`
	Retries           int    `json:"retries"`
	Description       string `json:"status_description"`
	ResultDescription string `json:"result_description"`
	PaybillBalance    string `json:"paybill_balance"`
	PaymentID         int64  `json:"payment_id"`
}

type ReversalCallback struct {
	ShortCode int64  `json:"short_code"`
	Reference string `json:"reference"`
	Status    int64 `json:"status"`
	Message   string `json:"message"`
}
