package models

type Inbox struct {
	LinkID    string `json:"link_id"`
	Message   string `json:"message"`
	Msisdn    int64  `json:"msisdn"`
	ShortCode string `json:"shortCode"`
	InboxID   int64  `json:"inbox_id"`
}

type HTTPResponse struct {

	Message   string `json:"message"`
	Status    int64  `json:"status"`
}