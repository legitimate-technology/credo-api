package models

type ErrorMessage struct {
	Error string `json:"error"`
}

type ResponseMessage struct {
	Status  int         `json:"status"`
	Message interface{} `json:"message"`
}

type SMSResponse struct {
	Errors       []string `json:"errors"`
	ResponseCode int      `json:"response_code"`
	Status       string   `json:"status"`
}

type SMSPayload struct {
	Sender    string `json:"sender"`
	Message   string `json:"message"`
	Recipient string `json:"recipient"`
	Bulk      int    `json:"bulk"`
	CallBack  string `json:"call_back"`
}

type STKPayload struct {
	Paybill string `json:"paybill"`
	Amount  int64 `json:"amount"`
	Msisdn  int64 `json:"msisdn"`
	Account string `json:"account"`
}

type KyandaCallback struct {
	MerchantID  string `json:"MerchantID"`
	Amount      int    `json:"amount"`
	Category    string `json:"category"`
	Destination string `json:"destination"`
	Details     struct {
		Tokens string `json:"tokens"`
		Units  string `json:"units"`
	} `json:"details"`
	Direction       string `json:"direction"`
	Message         string `json:"message"`
	RequestMetadata struct {
	} `json:"requestMetadata"`
	Source          string `json:"source"`
	Status          string `json:"status"`
	StatusCode      string `json:"status_code"`
	TransactionDate string `json:"transactionDate"`
	TransactionRef  string `json:"transactionRef"`
}