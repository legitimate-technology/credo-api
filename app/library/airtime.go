package library

import (
	"credo/app/logger"
	"credo/config"
	"crypto/hmac"
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/ini.v1"
	"net/url"
	"os/exec"
	"strings"
)

type KiandaAirtimeResponse struct {
	Status         string `json:"status"`
	StatusCode     string `json:"status_code"`
	TransactionID  string `json:"transactionId"`
	Transactiontxt string `json:"transactiontxt"`
	MerchantReference string `json:"merchant_reference"`
}

func getPayKonnectMSISDN(msisdn int64) string  {

	ms := fmt.Sprintf("%d",msisdn)
	return ms
	return 	ms[3:]

}

func getKiandaMSISDN(msisdn int64) string  {

	ms := fmt.Sprintf("%d",msisdn)
	return fmt.Sprintf("0%s",ms[3:])

}

func paykonnectAirtime(db *sql.DB, cfg *ini.File,transactionID int64,amount int64,msisdn int64) error {

	provider := "paykonnect"

	agentid, err := config.GetKey(cfg,provider,"agentid")
	if err != nil {
		agentid= "101"
	}

	retailerid, err := config.GetKey(cfg,provider,"retailerid")
	if err != nil {
		retailerid= agentid
	}

	network := GetKENetwork(cfg,fmt.Sprintf("%d",msisdn))
	operatorcode := "4"

	network =  strings.ToUpper(network)

	if network == "SAFARICOM" {

		operatorcode = "4"

	}

	if network == "AIRTEL" {

		operatorcode = "1"

	}

	if network == "TELKOM" {

		operatorcode = "2"

	}

	if network == "TELCOM" {

		operatorcode = "2"

	}

	if network == "ORANGE" {

		operatorcode = "2"

	}

	logger.Error("Got TELCO %s Got operator code %s ",network,operatorcode)

	denomination := "0"

	agentpwd, err := config.GetKey(cfg,provider,"agentpwd")
	if err != nil {
		agentpwd= "demo123"
	}

	endpoint, err := config.GetKey(cfg,provider,"url")
	if err != nil {

		retailerid= "http://193.104.202.165/kenya/mainlinkpos/purchase"
	}

	endpoint = fmt.Sprintf("%s/pw_etrans.php3",endpoint)

	params := map[string] string{
		"agentid":agentid,
		"transid": fmt.Sprintf("%d%d",transactionID,GetTime()),
		"retailerid":retailerid,
		"operatorcode":operatorcode,
		"circode":"1",
		"product":"",
		"denomination":denomination,
		"recharge": fmt.Sprintf("%d",amount),
		"mobileno": getPayKonnectMSISDN(msisdn),
		"bulkqty": "1",
		"narration": fmt.Sprintf("Airtime purchase for %d ",msisdn),
		"agentpwd": agentpwd,
		"loginstatus": "LIVE",
		"appver": "1.0",
		"custid":fmt.Sprintf("%d",transactionID),
	}

	var fields[] string

	for key,value := range params {

		val := fmt.Sprintf("%s=%v",key,url.QueryEscape(value))

		fields = append(fields,val)
	}

	parameters := strings.Join(fields,"&")

	endpoint = fmt.Sprintf("%s?%s",endpoint,parameters)

	logger.Info(" Wants to POST data to URL %s ",endpoint)

	headers := map[string] string {
		"accept":"plain/text",
		"content-type":"plain/text",
	}

	ta := GetTime()
	httpStatus,responsePayload := PayKonnectHTTPPostWithHeaders(endpoint,params,headers)
	tt := GetTime() - ta

	// remove end of data
	body := strings.Replace(responsePayload,"$$$","",-1)

	// split into fields
	parts := strings.Split(body,"%$")

	st := 1

	if len(parts) < 6 {

		return errors.New(body)
	}

	// check if response was a success
	//merchanttransid := parts[0]
	pktransid := parts[1]
	//transdatetime := parts[2]
	responsecode := parts[3]
	//responsemessage := parts[4]
	status := parts[5]

	if status == "SUCCESS" {

		st = 2
	}

	if status == "PENDING" {

		st = 1
	}

	if status == "FAILED" {

		st = 0
	}

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ",err.Error())
		return err
	}

	requestSTMT, err := tx.Prepare("INSERT INTO api_request (transaction_id, provider, url, request_type, request_body, response_body, external_reference,http_status, response_status, response_description, status, time_taken,operator_code, created) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,now())   ")
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}

	_, err = requestSTMT.Exec(transactionID, provider, endpoint, "POST", endpoint, responsePayload, pktransid,httpStatus, status, responsecode, st, tt,operatorcode)

	tx.Commit()

	return nil
}

func baiAirtime(db *sql.DB, cfg *ini.File,transactionID int64,amount int64,msisdn int64) error {

	t1 := GetTime()

	provider := "bai"

	remoteURL, err := config.GetKey(cfg,provider,"airtime_url")
	if err != nil {

		remoteURL= "https://api.vaspro.co.ke/v3/rewards/airtime"
	}

	apiKey, err := config.GetKey(cfg,provider,"apiKey")
	if err != nil {
		apiKey= "65e6262e01f1704ee548239c84e96f01"
	}

	callbackURL, err := config.GetKey(cfg,provider,"airtime_callback")
	if err != nil {

		callbackURL= "https://credo.smestech.com/airtime/callback/{transactionID}"
	}

	callbackURL = strings.Replace(callbackURL,"{transactionID}",fmt.Sprintf("%d",transactionID),-1)

	params := map[string] string{
		"apiKey":apiKey,
		"unique_id": fmt.Sprintf("CREDO_%d",transactionID),
		"msisdn":fmt.Sprintf("%d",msisdn),
		"amount": fmt.Sprintf("%d",amount),
		"callback":callbackURL,
	}

	requestPayload, _ := json.Marshal(params)

	status, responseBody := HTTPPost(remoteURL,params,nil)

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ",err.Error())
		return err
	}

	requestSTMT, err := tx.Prepare("INSERT INTO api_request (transaction_id, provider, url, request_type, request_body, response_body, external_reference,http_status, response_status, response_description, status, time_taken,operator_code, created) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,now())   ")
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}

	_, err = requestSTMT.Exec(transactionID, provider, remoteURL, "POST", requestPayload, responseBody, transactionID,status, status, responseBody, status,GetTime() - t1,"0")

	tx.Commit()

	if status > 299 || status < 200 {

		return errors.New(responseBody)

	}

	return nil
}

func SendAirtime(db *sql.DB, cfg *ini.File,transactionID int64,amount int64,msisdn,initiator int64) error {

	provider, _ := config.GetKey(cfg,"airtime_provider","provider")

	network := GetKENetwork(cfg,fmt.Sprintf("%d",msisdn))

	network =  strings.ToUpper(network)

	if network == "TELCOM" {

		network = "TELKOM"
	}

	if network == "ORANGE" {

		network = "TELKOM"
	}

	if network == "JTL" {

		network = "FAIBA"

	}


	provider1, _ := config.GetKey(cfg,network,"provider")

	if len(provider1) > 2 {

		provider = provider1
	}

	if strings.ToUpper(network) != "SAFARICOM" {

		provider = "kyanda"
	}

	if 	msisdn == 254726120256 || initiator == 254726120256 {

		provider = "kyanda"
	}

	provider = "kyanda"


	if provider == "paykonnect" {

		return paykonnectAirtime(db,cfg,transactionID,amount,msisdn)

	} else if provider == "bai" {

		return baiAirtime(db,cfg,transactionID,amount,msisdn)

	} else if provider == "kyanda" {

		return kyandaAirtime(db,cfg,transactionID,amount,msisdn,initiator)
	}

	return nil
}

func kyandaAirtime(db *sql.DB, cfg *ini.File,transactionID int64,amount int64,msisdn,initiatorPhone int64) error {

	//initiatorPhone = 254782444470

	t1 := GetTime()

	provider := "kyanda"

	remoteURL, err := config.GetKey(cfg,provider,"airtime_url")
	if err != nil {

		remoteURL= "https://api.kyanda.app/billing/v1/airtime/create"
	}

	apiKey, err := config.GetKey(cfg,provider,"apiKey")
	if err != nil {
		apiKey= "86b0f017dbed4c4783e9c93c95a4086a"
	}

	MerchantID, err := config.GetKey(cfg,provider,"MerchantID")
	if err != nil {
		MerchantID= "credo"
	}

	network := GetKENetwork(cfg,fmt.Sprintf("%d",msisdn))

	network =  strings.ToUpper(network)

	if network == "TELCOM" {

		network = "TELKOM"
	}

	if network == "ORANGE" {

		network = "TELKOM"
	}


	if network == "JTL" {

		network = "FAIBA"

	}

	if len(network) == 0 {

		network = "AIRTEL"
	}

	initiatorPhoneS := getKiandaMSISDN(initiatorPhone)
	phone := getKiandaMSISDN(msisdn)

	data := fmt.Sprintf("%d%s%s%s%s",amount,phone,network,initiatorPhoneS,MerchantID)
	secret := apiKey

	// Create a new HMAC by defining the hash type and the key (as byte array)
	h := hmac.New(sha256.New, []byte(secret))
	// Write Data to it
	h.Write([]byte(data))

	// Get result and encode as hexadecimal string
	signature := hex.EncodeToString(h.Sum(nil))

	signature1,_ := getHash(amount,phone,initiatorPhoneS,apiKey, MerchantID, network)

	logger.Error("GOT signature %s signature1 %s ",signature,signature1)

	params := map[string] string{
		"telco": network,
		"initiatorPhone": initiatorPhoneS,
		"phone": phone,
		"amount": fmt.Sprintf("%d",amount),
		"signature":signature1,
		"MerchantID": MerchantID,
	}

	headers := map[string]string {
		"apiKey": apiKey,
		"Accept": "application/json",
		"Content-Type": "application/json",
	}

	requestPayload, _ := json.Marshal(params)
	status, responseBody := HTTPPost(remoteURL,params,headers)

	resp := new(KiandaAirtimeResponse)

	if status >= 200 && status <= 299 {

		err = json.Unmarshal([]byte(responseBody),resp)
		if err != nil {

			logger.Error("got error decoding kyanda response %s ",err.Error())
			resp.Status = "Failed"
		}
	}

	requestSTMT, err := db.Prepare("INSERT INTO api_request (transaction_id, provider, url, request_type, request_body, response_body, external_reference,http_status, response_status, response_description, status, time_taken,operator_code, created) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,now())   ")
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}
	defer requestSTMT.Close()

	_, err = requestSTMT.Exec(transactionID, provider, remoteURL, "POST", requestPayload, responseBody, resp.MerchantReference,status, resp.Status, resp.Transactiontxt, resp.Status,GetTime() - t1,0)
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}
	if status > 299 || status < 200 {

		return errors.New(responseBody)

	}

	return nil
}

func getHash(amount int64,phone,initiatorPhone,apiKey, MerchantID, network string) (string,error)  {

	cd := exec.Command("/usr/bin/php", "/credo/scripts/hmac.php",fmt.Sprintf("phone='%s'",phone),fmt.Sprintf("amount='%d'",amount),fmt.Sprintf("network='%s'",network),fmt.Sprintf("initiatorPhone='%s'",initiatorPhone),fmt.Sprintf("MerchantID='%s'",MerchantID),fmt.Sprintf("apiKey='%s'",apiKey))
	results, err := cd.Output()
	if err != nil {

		logger.Error("got error calling php script %s  ",err.Error())
		return "", err
	}

	return string(results), err
}

func getHash1(apiKey,MerchantID,remoteURL string) (string,error)  {

	cd := exec.Command("/usr/bin/php", "/credo/scripts/hmac.php",fmt.Sprintf("phone="),fmt.Sprintf("amount="),fmt.Sprintf("network="),fmt.Sprintf("initiatorPhone="),fmt.Sprintf("MerchantID='%s'",MerchantID),fmt.Sprintf("apiKey='%s'",apiKey))
	results, err := cd.Output()
	if err != nil {

		logger.Error("got error calling php script %s  ",err.Error())
		return "", err
	}

	return string(results), err
}