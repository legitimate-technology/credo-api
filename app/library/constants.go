package library

const TRANSACTION_TYPE_CREDIT  = 1
const TRANSACTION_TYPE_DEBIT  = 2

const REFERENCE_ID_AIRTIME_PURCHASE  = 1
const REFERENCE_ID_KPLC_TOKEN_PURCHASE  = 2
const REFERENCE_ID_BET  = 3
const REFERENCE_ID_FREEBET_BET  = 4
const REFERENCE_ID_DEPOSIT  = 5
const REFERENCE_ID_FREE_DEPOSIT  = 6
const REFERENCE_ID_WITHDRAW  = 7
const REFERENCE_ID_WITHDRAW_CHARGES  = 8
const REFERENCE_ID_WITHDRAW_REVERSAL  = 10
const REFERENCE_ID_VIRTUALS_DEBIT  = 11
const REFERENCE_ID_VIRTUALS_CREDIT  = 12
const REFERENCE_ID_WINNING_ROLLBACK  = 13
const REFERENCE_ID_FREEBET_ACTIVATION  = 14
const REFERENCE_ID_SAMBAZA  = 15
const REFERENCE_ID_SAMBAZA_CHARGES  = 16
const REFERENCE_ID_JACKPOT_BET  = 17
const REFERENCE_ID_REEDEM_POINTS  = 18
