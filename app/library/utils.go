package library

import (
	"credo/app/logger"
	"credo/config"
	"bufio"
	"errors"
	"fmt"
	"gopkg.in/ini.v1"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

func GetString(payload map[string]interface{}, name string, defaults string) (string, error) {

	if payload[name] == nil {

		return defaults, errors.New(fmt.Sprintf("%s is not set", name))
	}

	v := reflect.ValueOf(payload[name])

	switch v.Kind() {

	case reflect.Invalid:
		return defaults, nil

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return fmt.Sprintf("%d",v.Int()), nil
		//return strconv.FormatInt(v.Int(), 10), nil

	case reflect.Float64, reflect.Float32:
		return fmt.Sprintf("%0.f",v.Float()), nil

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return strconv.FormatUint(v.Uint(), 10), nil

	// ...floating-point and complex cases omitted for brevity...
	case reflect.Bool:
		return strconv.FormatBool(v.Bool()), nil

	case reflect.String:
		return v.String(), nil

	case reflect.Chan, reflect.Func, reflect.Ptr, reflect.Slice, reflect.Map:
		return v.Type().String() + " 0x" + strconv.FormatUint(uint64(v.Pointer()), 16), nil

	default: // reflect.Array, reflect.Struct, reflect.Interface
		return v.Type().String() + " value", nil
	}
}

func GetFloat(payload map[string]interface{}, name string, defaults float64) (float64, error) {

	if payload[name] == nil {

		return defaults, errors.New(fmt.Sprintf("%s is not set", name))
	}

	v := reflect.ValueOf(payload[name])

	switch v.Kind() {

	case reflect.Invalid:
		return defaults, errors.New(fmt.Sprintf("%s is not set", name))

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return float64(v.Int()), nil

	case reflect.Float64, reflect.Float32:
		return v.Float(), nil

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return float64(v.Uint()), nil
	// ...floating-point and complex cases omitted for brevity...
	case reflect.Bool:
		//return strconv.FormatBool(v.Bool())
		return defaults, errors.New(fmt.Sprintf("%s is not set", name))

	case reflect.String:
		val, err := strconv.ParseFloat(v.String(), 64)

		if err != nil {

			return defaults, err
		}

		return val, nil

	case reflect.Chan, reflect.Func, reflect.Ptr, reflect.Slice, reflect.Map:
		//return v.Type().String() + " 0x" + strconv.FormatUint(uint64(v.Pointer()), 16)
		return defaults, errors.New(fmt.Sprintf("%s is not set", name))

	default: // reflect.Array, reflect.Struct, reflect.Interface
		//return v.Type().String() + " value"
		return defaults, errors.New(fmt.Sprintf("%s is not set", name))
	}

	value := payload[name].(float64)

	if value == 0 {

		return defaults, errors.New(fmt.Sprintf("%s is not set", name))
	}

	return value, nil
}

func GetInt(payload map[string]interface{}, name string, defaults int64) (int64, error) {

	if payload[name] == nil {

		return defaults, errors.New(fmt.Sprintf("%s is not set", name))
	}

	v := reflect.ValueOf(payload[name])

	switch v.Kind() {

	case reflect.Invalid:
		return defaults, errors.New(fmt.Sprintf("%s is not set", name))

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return int64(v.Int()), nil

	case reflect.Float64, reflect.Float32:
		return int64(v.Float()), nil

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return int64(v.Uint()), nil
	// ...floating-point and complex cases omitted for brevity...
	case reflect.Bool:
		//return strconv.FormatBool(v.Bool())
		return defaults, errors.New(fmt.Sprintf("%s is not set", name))

	case reflect.String:
		val, err := strconv.ParseInt(v.String(),10, 64)

		if err != nil {

			return defaults, err
		}

		return val, nil

	case reflect.Chan, reflect.Func, reflect.Ptr, reflect.Slice, reflect.Map:
		//return v.Type().String() + " 0x" + strconv.FormatUint(uint64(v.Pointer()), 16)
		return defaults, errors.New(fmt.Sprintf("%s is not set", name))

	default: // reflect.Array, reflect.Struct, reflect.Interface
		//return v.Type().String() + " value"
		return defaults, errors.New(fmt.Sprintf("%s is not set", name))
	}

	value := payload[name].(int64)

	if value == 0 {

		return defaults, errors.New(fmt.Sprintf("%s is not set", name))
	}

	return value, nil
}

func GetBool(payload map[string]interface{}, name string, defaults bool) (bool, error) {

	if payload[name] == nil {

		return defaults, errors.New(fmt.Sprintf("%s is not set", name))
	}

	value := payload[name].(bool)

	return value, nil
}

func GetParam(payload map[string]interface{}, name string, defaults interface{}) (interface{}, error) {

	if payload[name] == nil {

		return defaults, errors.New(fmt.Sprintf("%s is not set", name))
	}

	value := payload[name]

	if value == nil {

		return defaults, errors.New(fmt.Sprintf("%s is not set", name))
	}

	return value, nil
}

func GetInt64Value(payload interface{}, defaults int64) (int64, error) {

	if payload == nil {

		return defaults, errors.New(fmt.Sprintf(" is not set"))
	}

	v := reflect.ValueOf(payload)

	switch v.Kind() {

	case reflect.Invalid:
		return defaults, errors.New(fmt.Sprintf(" is not set"))

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int(), nil

	case reflect.Float64, reflect.Float32:
		return int64(v.Float()), nil

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return int64(v.Uint()), nil
	// ...floating-point and complex cases omitted for brevity...
	case reflect.Bool:
		//return strconv.FormatBool(v.Bool())
		return defaults, errors.New(fmt.Sprintf("is not set"))

	case reflect.String:
		val, err := strconv.ParseInt(v.String(), 10,64)

		if err != nil {

			return defaults, err
		}

		return val, nil

	case reflect.Chan, reflect.Func, reflect.Ptr, reflect.Slice, reflect.Map:
		//return v.Type().String() + " 0x" + strconv.FormatUint(uint64(v.Pointer()), 16)
		return defaults, errors.New(fmt.Sprintf("is not set"))

	default: // reflect.Array, reflect.Struct, reflect.Interface
		//return v.Type().String() + " value"
		return defaults, errors.New(fmt.Sprintf("is not set"))
	}

	return defaults, nil
}

func GetStringValue(payload interface{}, defaults string) (string, error) {

	if payload == nil {

		return defaults, errors.New(fmt.Sprintf("is not set"))
	}

	v := reflect.ValueOf(payload)

	switch v.Kind() {

	case reflect.Invalid:
		return defaults, nil

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return fmt.Sprintf("%d",v.Int()), nil
		//return strconv.FormatInt(v.Int(), 10), nil

	case reflect.Float64, reflect.Float32:
		return fmt.Sprintf("%0.f",v.Float()), nil

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return strconv.FormatUint(v.Uint(), 10), nil

	// ...floating-point and complex cases omitted for brevity...
	case reflect.Bool:
		return strconv.FormatBool(v.Bool()), nil

	case reflect.String:
		return v.String(), nil

	case reflect.Chan, reflect.Func, reflect.Ptr, reflect.Slice, reflect.Map:
		return v.Type().String() + " 0x" + strconv.FormatUint(uint64(v.Pointer()), 16), nil

	default: // reflect.Array, reflect.Struct, reflect.Interface
		return v.Type().String() + " value", nil
	}
}

func GetFloatValue(payload interface{}, defaults float64) (float64, error) {

	if payload == nil {

		return defaults, errors.New(fmt.Sprintf("%s is not set"))
	}

	v := reflect.ValueOf(payload)

	switch v.Kind() {

	case reflect.Invalid:
		return defaults, errors.New(fmt.Sprintf("%s is not set"))

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return float64(v.Int()), nil

	case reflect.Float64, reflect.Float32:
		return v.Float(), nil

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return float64(v.Uint()), nil
	// ...floating-point and complex cases omitted for brevity...
	case reflect.Bool:
		//return strconv.FormatBool(v.Bool())
		return defaults, errors.New(fmt.Sprintf("%s is not set"))

	case reflect.String:
		val, err := strconv.ParseFloat(v.String(), 64)

		if err != nil {

			return defaults, err
		}

		return val, nil

	case reflect.Chan, reflect.Func, reflect.Ptr, reflect.Slice, reflect.Map:
		//return v.Type().String() + " 0x" + strconv.FormatUint(uint64(v.Pointer()), 16)
		return defaults, errors.New(fmt.Sprintf("%s is not set"))

	default: // reflect.Array, reflect.Struct, reflect.Interface
		//return v.Type().String() + " value"
		return defaults, errors.New(fmt.Sprintf("%s is not set"))
	}

	return defaults, nil
}


func FormatMSISDN(msisdn string) string {

	formatedNumber := ""

	if len(msisdn) <  9 {

		return formatedNumber
	}

	var re = regexp.MustCompile(`[\n\r\s\D]+`)
	msisdn = re.ReplaceAllString(msisdn, "")

	input1 := msisdn[0:1]
	input2 := msisdn[0:2]
	input4 := msisdn[0:4]
	input5 := msisdn[0:5]

	length := len(msisdn)

	if input1 == "7" && length == 9 {

		formatedNumber = SubstrReplace(msisdn, "2547", 0, 1)

	} else if input1 == "1" && length == 9 {

		formatedNumber = SubstrReplace(msisdn, "2541", 0, 1)

	} else if input2 == "07" && length == 10 {

		formatedNumber = SubstrReplace(msisdn, "254", 0, 1)

	} else if input2 == "01" && length == 10 {

		formatedNumber = SubstrReplace(msisdn, "254", 0, 1)

	} else if input4 == "2547" && length == 12 {

		formatedNumber = msisdn;

	} else if input4 == "2541" && length == 12 {

		formatedNumber = msisdn;

	} else if input5 == "+2547" && length == 13 {

		formatedNumber = SubstrReplace(msisdn, "", 0, 1)

	} else if input5 == "+2541" && length == 13 {

		formatedNumber = SubstrReplace(msisdn, "", 0, 1)

	}

	//logger.Error("received %s formatted to %s ",msisdn,formatedNumber)

	return formatedNumber
}

func SubstrReplace(haystack string, needle string, start int, end int) string {

	if start == 0 {

		parts := haystack[end:]
		return needle + parts
	}

	part1 := haystack[0:start]
	part2 := haystack[end:]

	return part1 + needle + part2
}


func GetTransactionType(number string) int64 {

	number = strings.TrimSpace(number)
	// check if its a valid phone number
	if strings.HasPrefix(number,"2547") && len(number) == 12 {

		return 1
	}

	if strings.HasPrefix(number,"2541") && len(number) == 12 {

		return 1
	}

	if strings.HasPrefix(number,"07") && len(number) == 10 {

		return 1
	}

	if strings.HasPrefix(number,"011") && len(number) == 10 {

		return 1
	}

	if strings.HasPrefix(number,"010") && len(number) == 10 {

		return 1
	}

	if len(number) == 11 {

		return 2
	}

	return 1
}

func GetKENetwork(conf *ini.File,msisdn string) string  {

	if len(msisdn) < 6 {

		logger.Error("GOT msisdn %s ==> SAFARICOM",msisdn)
		return "SAFARICOM"
	}

	telecoCode1 := msisdn[0:5]
	telecoCode2 := msisdn[0:6]

	five_digits, _ := strconv.ParseInt(telecoCode1, 10, 64)
	six_digits, _ := strconv.ParseInt(telecoCode2, 10, 64)

	logger.Error("MSISDN %s got five_digits %d ",msisdn,five_digits)
	logger.Error("MSISDN %s got six_digits %d ",msisdn,six_digits)

	fileLocation, err := config.GetKey(conf,"network","ke_network_extension")

	if err != nil {

		logger.Error("got error reading file %s ",err.Error())
		return ""
	}

	file, err := os.Open(fileLocation)
	if err != nil {

		logger.Critical("GOT error openning file %s error %s ", fileLocation, err.Error())
		logger.Error("GOT msisdn %s ==> %s",msisdn,"ERROR")
		return ""

	}
	defer file.Close()

	IdentifiedNetwork := ""

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)

	// read line by line, lets first check extensions with 6 digits
	for fileScanner.Scan() {

		text := fileScanner.Text()
		record := strings.Split(text, "=")

		if record == nil {

			logger.Error("Got error while reading CSV %s", err.Error())
			//return totalProcessed,err
			//continue
		} else {

			extension := record[0]
			network := record[1]

			// only check where extension is 6 digits

			if len(extension) == 6 {

				six_digits_extension, _ := strconv.ParseInt(extension, 10, 64)

				if six_digits_extension == six_digits {

					IdentifiedNetwork = network
				}
			}
		}
	}

	file.Close()

	if len(IdentifiedNetwork) == 0 || IdentifiedNetwork == "" {

		f, err := os.Open(fileLocation)
		if err != nil {

			logger.Critical("GOT error openning file %s error %s ", fileLocation, err.Error())
			logger.Error("GOT msisdn %s ==> %s",msisdn,"ERROR")
			return ""

		}
		defer f.Close()

		fc := bufio.NewScanner(f)
		fc.Split(bufio.ScanLines)

		// read line by line, lets then check extensions with 5 digits
		for fc.Scan() {

			text := fc.Text()
			record := strings.Split(text, "=")

			if record == nil {

				logger.Error("Got error while reading CSV %s", err.Error())
				//return totalProcessed,err
				//continue
			} else {

				extension := record[0]
				network := record[1]

				// only check where extension is 5 digits

				if len(extension) == 5 {

					five_digits_extension, _ := strconv.ParseInt(extension, 10, 64)

					if five_digits_extension == five_digits {

						IdentifiedNetwork = network
					}
				}
			}
		}

	}

	logger.Error("GOT 1 msisdn %s ==> %s",msisdn,IdentifiedNetwork)

	return IdentifiedNetwork

}
