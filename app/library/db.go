package library

import (
	"database/sql"
	"regexp"
	"credo/app/logger"
	"unicode/utf8"
)

// router and DB instance
type Db struct {
	DB     *sql.DB
	TX *sql.Tx
	Query string
	Params []interface{}
	Result []interface{}
}

func (a *Db) InsertQuery() (lastInsertID *int64, err error) {

	tx, err := a.DB.Begin()
	if err != nil {

		logger.Error("Got error starting transaction %s ",err.Error())
		return nil,err
	}

	stmt, err := tx.Prepare(a.Query)
	if err != nil {

		logger.Error("Got error preparing a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	defer stmt.Close()

	res, err := stmt.Exec(a.Params...)
	if err != nil {

		tx.Rollback()
		logger.Error("Got error executing a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	lastInsertId, err := res.LastInsertId()
	if err != nil {
		tx.Rollback()
		logger.Error("Got error retrieving last insert ID a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	tx.Commit();

	return &lastInsertId,nil
}

func (a *Db) UpdateQuery() (rowsAffected *int64, err error) {

	tx, err := a.DB.Begin()
	if err != nil {

		logger.Error("Got error starting transaction %s ",err.Error())
		return nil,err
	}

	stmt, err := tx.Prepare(a.Query)
	if err != nil {

		logger.Error("Got error preparing a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	defer stmt.Close()

	res, err := stmt.Exec(a.Params...)
	if err != nil {

		tx.Rollback()
		logger.Error("Got error executing a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	rowsaffected, err := res.RowsAffected()
	if err != nil {
		tx.Rollback()
		logger.Error("Got error retrieving last insert ID a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	tx.Commit();

	return &rowsaffected,nil
}

func (a *Db) InsertInTransaction() (lastInsertID *int64, err error) {

	stmt, err := a.TX.Prepare(a.Query)
	if err != nil {

		logger.Error("Got error preparing a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	defer stmt.Close()

	res, err := stmt.Exec(a.Params...)
	if err != nil {

		logger.Error("Got error executing a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	lastInsertId, err := res.LastInsertId()
	if err != nil {
		logger.Error("Got error retrieving last insert ID a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	return &lastInsertId,nil
}

func (a *Db) UpdateInTransaction() (rowsAffected *int64, err error) {

	stmt, err := a.TX.Prepare(a.Query)
	if err != nil {

		logger.Error("Got error preparing a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	defer stmt.Close()

	res, err := stmt.Exec(a.Params...)
	if err != nil {

		logger.Error("Got error executing a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	rowsaffected, err := res.RowsAffected()
	if err != nil {

		logger.Error("Got error retrieving last insert ID a.Query %s a.Params %v error %s ",a.Query,a.Params,err.Error())
		return nil,err
	}

	return &rowsaffected,nil
}

func (a *Db) FetchOne() *sql.Row {

	return a.DB.QueryRow(a.Query,a.Params...)
}

func (a *Db) Fetch() (*sql.Rows, error) {

	return a.DB.Query(a.Query,a.Params...)
}

func (a *Db) SetParams(params ...interface{})  {

	a.Params = params
}

func (a *Db) SetQuery(query string)  {

	a.Query = query
}

func (a *Db) setResults(result ...interface{})  {

	a.Result = result
}

func Escape(sql string) string {

	re := regexp.MustCompile("[[:^ascii:]]")
	t := re.ReplaceAllLiteralString(sql, "")
	return t

	if !utf8.ValidString(sql) {

		v := make([]rune, 0, len(sql))
		for i, r := range sql {
			if r == utf8.RuneError {
				_, size := utf8.DecodeRuneInString(sql[i:])
				if size == 1 {
					continue
				}
			}
			v = append(v, r)
		}
		sql = string(v)
	}

	return sql

	dest := make([]byte, 0, 2*len(sql))
	var escape byte
	for i := 0; i < len(sql); i++ {
		c := sql[i]

		escape = 0

		switch c {
		case 0: /* Must be escaped for 'mysql' */
			escape = '0'
			break
		case '\n': /* Must be escaped for logs */
			escape = 'n'
			break
		case '\r':
			escape = 'r'
			break
		case '\\':
			escape = '\\'
			break
		case '\'':
			escape = '\''
			break
		case '"': /* Better safe than sorry */
			escape = '"'
			break
		case '\032': /* This gives problems on Win32 */
			escape = 'Z'
		}

		if escape != 0 {
			dest = append(dest, '\\', escape)
		} else {
			dest = append(dest, c)
		}
	}

	return string(dest)
}

func NewNullString(s string) sql.NullString {

	if len(s) == 0 {

		return sql.NullString{}
	}

	return sql.NullString{
		String: s,
		Valid: true,
	}
}