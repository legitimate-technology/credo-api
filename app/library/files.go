package library

import (
	"credo/app/logger"
	"bytes"
	"fmt"
	"github.com/Pallinder/go-randomdata"
	"github.com/go-cmd/cmd"
	"io"
	"os"
	"path"
	"strconv"
	"strings"
)

func MoveFile(sourcePath, destPath string) error {

	//return storage.UploadFromPath(sourcePath,destPath)

	inputFile, err := os.Open(sourcePath)
	if err != nil {
		return fmt.Errorf("Couldn't open source file: %s", err)
	}
	outputFile, err := os.Create(destPath)
	if err != nil {
		inputFile.Close()
		return fmt.Errorf("Couldn't open dest file: %s", err)
	}
	defer outputFile.Close()
	_, err = io.Copy(outputFile, inputFile)
	inputFile.Close()
	if err != nil {
		return fmt.Errorf("Writing to output file failed: %s", err)
	}
	// The copy was successful, so now delete the original file
	err = os.Remove(sourcePath)
	if err != nil {
		return fmt.Errorf("Failed removing original file: %s", err)
	}
	return nil
}

func CopyFile(sourcePath, destPath string) error {

	//return storage.Copy(sourcePath,destPath)

	inputFile, err := os.Open(sourcePath)
	if err != nil {
		return fmt.Errorf("Couldn't open source file: %s", err)
	}
	outputFile, err := os.Create(destPath)
	if err != nil {
		inputFile.Close()
		return fmt.Errorf("Couldn't open dest file: %s", err)
	}
	defer outputFile.Close()
	_, err = io.Copy(outputFile, inputFile)
	inputFile.Close()
	if err != nil {
		return fmt.Errorf("Writing to output file failed: %s", err)
	}
	return nil
}

func NumberOfLines(file string) (int) {


	envCmd := cmd.NewCmd("wc","-l",file)

	// Run and wait for Cmd to return Status
	status := <-envCmd.Start()

	var response1 string

	// Print each line of STDOUT from Cmd
	for _, line := range status.Stdout {
		response1 = line
	}

	words := strings.Fields(response1)

	fmt.Println(words[0])

	// Create Cmd, buffered output
	//envCmd := cmd.NewCmd("awk","'END{print NR}' ",file)
	envCmd = cmd.NewCmd("wc","-l",file)

	// Run and wait for Cmd to return Status
	status = <-envCmd.Start()

	var response string

	// gets each line of STDOUT from Cmd
	for i, line := range status.Stdout {
		logger.Info("got index %d is %s",i,line)
		response = line
	}

	words = strings.Fields(response)

	count,_ := strconv.Atoi(words[0])

	logger.Info("NumberOfLines checking number of files for file %s, got %d lines from %s",file,count,words[0])

	return count
}

func ReadFile(filename string) string {


	filerc, err := os.Open(filename)

	if err != nil{

		logger.Error("cant read file %s got error %s",filename,err.Error())
		return ""
	}

	defer filerc.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(filerc)
	contents := buf.String()

	return contents

}

func GetFileNameWithoutExtension(fn string) string {

	f, err := os.Stat(fn)

	if err != nil {

		logger.Error(" could not get file name from %s, got error  %s",fn,err.Error())
		return "0"
	}

	fn = f.Name();

	return strings.TrimSuffix(fn, path.Ext(fn))
}

func GetFileExtension(fn string) string {

	return  path.Ext(fn)

}

func RandomFileName(length int) (string,error) {

	return randomdata.Letters(length),nil
}

func DeleteFile(path string) {

	//storage.Delete(path)

	//return
	// delete file
	os.Remove(path)
	logger.Info("==> done deleting file ",path)
}