package library

import (
	"fmt"
	"os/exec"
	"redis"
	"credo/app/logger"
	"time"
)

func GetRedisKey(conn *redis.Client, key string) (string, error) {

	key = fmt.Sprintf("credo-%s",key)

	var data string
	data, err := conn.Get(key).Result()
	if err != nil {

		return data, fmt.Errorf("error getting key %s: %v", key, err)
	}

	return data, err
}

func SetRedisKey(conn *redis.Client,key string, value string ) error {

	key = fmt.Sprintf("credo-%s",key)

	_, err := conn.Set(key, value,time.Second * time.Duration(0) ).Result()
	if err != nil {

		v := string(value)

		if len(v) > 15 {

			v = v[0:12] + "..."
		}

		return fmt.Errorf("error setting key %s to %s: %v", key, v, err)
	}
	return err
}

func SetRedisKeyWithExpiry(conn *redis.Client,key string, value string,seconds int) error {

	key = fmt.Sprintf("credo-%s",key)

	_, err := conn.Set(key, value,time.Second * time.Duration(seconds) ).Result()
	if err != nil {

		v := string(value)

		if len(v) > 15 {

			v = v[0:12] + "..."
		}

		return fmt.Errorf("error setting key %s to %s: %v", key, v, err)
	}
	return err
}

func SetRedisKeyWithExpiryInMiliSeconds(conn *redis.Client,key string, value string,milliseconds int) error {

	key = fmt.Sprintf("credo-%s",key)

	_, err := conn.Set(key, value,time.Millisecond * time.Duration(milliseconds) ).Result()
	if err != nil {

		v := string(value)

		if len(v) > 15 {

			v = v[0:12] + "..."
		}

		return fmt.Errorf("error setting key %s to %s: %v", key, v, err)
	}
	return err
}

func DeleteRedisKey(conn *redis.Client,key string) error {

	key = fmt.Sprintf("credo-%s",key)

	_, err := conn.Del(key).Result()

	if err != nil {

		return fmt.Errorf("error getting key %s: %v", key, err)
	}

	return err
}

func GetRedisKeyWithConnection(conn *redis.Client, key string) (string, error) {

	key = fmt.Sprintf("credo-%s",key)

	var data string
	data, err := conn.Get(key).Result()
	if err != nil {

		return data, fmt.Errorf("error getting key %s: %v", key, err)
	}

	return data, err
}

func SetRedisKeyWithConnection(conn *redis.Client,key string, value string ) error {

	key = fmt.Sprintf("credo-%s",key)

	_, err := conn.Set(key, value, time.Second * 0 ).Result()
	if err != nil {

		v := string(value)

		if len(v) > 15 {

			v = v[0:12] + "..."
		}

		return fmt.Errorf("error setting key %s to %s: %v", key, v, err)
	}
	return err
}

func DeleteRedisKeyWithConnection(conn *redis.Client,key string) error {

	key = fmt.Sprintf("credo-%s",key)

	_, err := conn.Del(key).Result()

	if err != nil {

		return fmt.Errorf("error getting key %s: %v", key, err)
	}

	return err
}

func execute(command string,arg ...string) error {

	// here we perform the pwd command.
	// we can store the output of this in our out variable
	// and catch any errors in err
	_, err := exec.Command(command,arg...).Output()

	// if there is an error with our execution
	// handle it here
	if err != nil {

		logger.Error("got error could execute command %s got error %s", command,err)
		return err

	}

	// as the out variable defined above is of type []byte we need to convert
	// this to a string or else we will see garbage printed out in our console
	// this is how we convert it to a string
	//fmt.Println(command,"\n COMMAND Successfully Executed")
	//output := string(out[:])
	//fmt.Println(output)

	// let's try the pwd command herer

	//fmt.Println(output)
	return nil
}
