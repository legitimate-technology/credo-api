package library

import (
	"credo/app/logger"
	"credo/app/models"
	"crypto/md5"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	r "github.com/Pallinder/go-randomdata"
	"github.com/go-sql-driver/mysql"
	"github.com/logrusorgru/aurora"
	"gopkg.in/ini.v1"
	"redis"
)

func GetProfile(db *sql.DB, redisConn *redis.Client, profileID int64) (*models.Profile, error) {

	var profile models.Profile

	redisKey := fmt.Sprintf("PROFILE-%d", profileID)
	// check profile balance
	userData, err := GetRedisKey(redisConn, redisKey)

	if err == nil {

		err = json.Unmarshal([]byte(userData), &profile);
		if err != nil {

			logger.Error(" got error %s ",err.Error())
		}
	}

	if err != nil {

		var msisdn,balance, status,points sql.NullInt64
		var first_name, other_name sql.NullString
		var created mysql.NullTime

		err = db.QueryRow(`SELECT p.first_name, p.other_name, p.msisdn, p.status, p.created,pb.balance,
       		pb.points FROM profile p INNER JOIN profile_balance pb ON p.id=pb.profile_id WHERE p.id = ? `,
			profileID).Scan(&first_name, &other_name, &msisdn, &status, &created, &balance, &points)

		if err != nil {

			logger.Error(" got error quering profile %s ",err.Error())
			return nil, err
		}

		profile.ID = profileID
		profile.FirstName = first_name.String
		profile.OtherName = other_name.String
		profile.Msisdn = msisdn.Int64
		profile.Balance = balance.Int64
		profile.Points = points.Int64
		profile.Created = ToMysql(created.Time)
		profile.Status = status.Int64

		message, err := json.Marshal(profile)

		if err != nil {

			logger.Error(" got error decoding profile to string %s ", aurora.Red(err.Error()))
			return nil, err
		}

		SetRedisKeyWithExpiry(redisConn, redisKey, string(message), 60 * 5)

	}

	return &profile, nil
}

func ReloadProfile(db *sql.DB, redisConn *redis.Client, profileID int64) (*models.Profile, error) {

	var profile models.Profile

	redisKey := fmt.Sprintf("PROFILE-%d", profileID)

	var msisdn,balance,points, status sql.NullInt64
	var first_name, other_name sql.NullString
	var created mysql.NullTime

	err := db.QueryRow(`SELECT p.first_name, p.other_name, p.msisdn, p.status, p.created,pb.balance,
       		pb.points FROM profile p INNER JOIN profile_balance pb ON p.id=pb.profile_id WHERE p.id = ? `,
		profileID).Scan(&first_name, &other_name, &msisdn, &status, &created, &balance, &points)


	if err != nil {

		return nil, err
	}

	profile.ID = profileID
	profile.FirstName = first_name.String
	profile.OtherName = other_name.String
	profile.Msisdn = msisdn.Int64
	profile.Balance = balance.Int64
	profile.Points = points.Int64
	profile.Created = ToMysql(created.Time)
	profile.Status = status.Int64

	message, err := json.Marshal(profile)

	if err != nil {

		logger.Error(" got error decoding profile to string %s ", aurora.Red(err.Error()))
		return nil, err
	}

	SetRedisKeyWithExpiry(redisConn, redisKey, string(message), 60 *5)

	return &profile, nil
}

func CreateProfile(db *sql.DB,cfg *ini.File, redisConn *redis.Client, msisdn int64, password string, code int, firstName string, otherName string, src string, balance int64) (*models.Profile, error) {

	profile, message := RegisterProfile(db,redisConn, msisdn, password, code, firstName, otherName, src, balance)

	if profile == nil {

		return profile, errors.New(message)
	}

	SendSMS(db,cfg,"REGISTRATION", msisdn, message)

	return profile, nil
}

func RegisterProfile(db *sql.DB, redisConn *redis.Client, msisdn int64, password string, code int, firstName string, otherName string, src string, balance int64) (*models.Profile, string) {

	//logger.Error("AM AT RegisterProfile 1 ");

	tx, err := db.Begin();
	if err != nil {

		logger.Error("Error starting db transaction %s ", err.Error())
		return nil, "We sorry we cannot complete your request at the moment, try again later"
	}

	//logger.Error("AM AT RegisterProfile 2 ");

	// check if profile exists
	var check sql.NullInt64
	err = db.QueryRow("SELECT id FROM profile WHERE msisdn = ? ",msisdn).Scan(&check)
	if err != sql.ErrNoRows {

		return nil, "User with the same phone number already exists, please login to continue"
	}


	stmtProfile, err := tx.Prepare("INSERT INTO profile (first_name, other_name, msisdn,password,code,status, created) VALUE (?,?,?,MD5(?),?,1,now()) ")
	if err != nil {

		logger.Error("got error preparing query to create profile %s ", err.Error())
		return nil, "We sorry we cannot complete your request at the moment, try again later"
	}

	res, err := stmtProfile.Exec(firstName, otherName, msisdn, password, code)
	if err != nil {

		stmtProfile.Close()
		tx.Rollback()
		logger.Error("got error executing query to create profile %s ",err.Error())
		return nil, "We sorry we cannot complete your request at the moment, try again later"
	}

	stmtProfile.Close()

	profileID, err := res.LastInsertId()
	if err != nil {

		tx.Rollback()
		logger.Error("got error retrieving profileID %s ",err.Error())
		return nil, "user already exist. please login or rest your password"
	}

	stmtBalance, err := tx.Prepare("INSERT INTO profile_balance (profile_id,balance,points,created) VALUE (?,?,?,now()) ON DUPLICATE KEY UPDATE balance=balance + VALUES(balance),points=points + VALUES(points) ")
	if err != nil {

		tx.Rollback()
		logger.Error(" got error preparing query to create profile balance %s", err.Error())
		return nil, "We sorry we cannot complete your request at the moment, try again later"
	}

	res, err = stmtBalance.Exec(profileID, balance,0)
	if err != nil {

		stmtBalance.Close()
		tx.Rollback()
		logger.Error("got error executing query to create profile %s ",err.Error())
		return nil, "We sorry we cannot complete your request at the moment, try again later"
	}

	stmtBalance.Close()

	if len(src) > 0 {

		// create attribution
		stmtSrc, err := tx.Prepare("INSERT INTO profile_attribution (profile_id, src, created) VALUE (?,?,now())")
		if err != nil {

			tx.Rollback()
			logger.Error("got error preparing query to create profile_attribution")
			return nil, "We sorry we cannot complete your request at the moment, try again later"
		}

		_, err = stmtSrc.Exec(profileID, src)
		if err != nil {

			stmtSrc.Close()
			tx.Rollback()
			logger.Error("got error executing query to create profile_attribution %s ",err.Error())
			return nil, "We sorry we cannot complete your request at the moment, try again later"
		}

		stmtSrc.Close()
	}

	tx.Commit()

	// retrieve SMS template
	templateName := "REGISTRATION"
	sms := GetSMSTemplate(db,redisConn,templateName)

	if len(firstName) == 0 {

		firstName = "Rafiki"
	}

	replacements := map[string]string{
		"name": firstName,
		"code": fmt.Sprintf("%d", code),
	}

	sms = BuildSMS(sms, replacements)


	_,err = GetProfile(db, redisConn, profileID)
	if err != nil {

		logger.Error("got error retrieving profile %s ",err.Error())
	}

	profile := models.Profile{

		ID: profileID,
		FirstName: firstName,
		OtherName: otherName,
		Msisdn: msisdn,
		Balance: balance,
		Points: 0,
		Created: MysqlNow(),
		Status: 1,
	}
	//SendSMS(db,msisdn,sms,"REGISTRATION")

	return &profile, sms
}

func VerificationCode() int {

	min := 1000
	max := 9999

	return r.Number(min, max)
}

func GetReference(db *sql.DB, profileID int64) (int, error) {

	var reference sql.NullInt64

	err := db.QueryRow("SELECT (reference + 1) AS reference FROM bet_reference WHERE profile_id = ? ", profileID).Scan(&reference)

	if err == sql.ErrNoRows || !reference.Valid {

		stmt, err := db.Prepare("INSERT INTO bet_reference (profile_id, reference, created) VALUES (?,1,now()) ")

		if err != nil {

			logger.Error("got error creating bet reference %s ", err.Error())
			return 0, err
		}

		_, err = stmt.Exec(profileID)

		if err != nil {

			stmt.Close()
			logger.Error("got error creating bet reference %s ", err.Error())
			return 0, err
		}

		stmt.Close()
		return 1, nil

	} else if err != nil {

		logger.Error("got error creating bet reference %s ", err.Error())
		return 0, err
	}

	stmt, err := db.Prepare("UPDATE bet_reference SET reference = reference + 1 WHERE profile_id = ? ")

	if err != nil {

		logger.Error("got error creating bet reference %s ", err.Error())
		return 0, err
	}

	defer stmt.Close()
	_, err = stmt.Exec(profileID)

	if err != nil {

		logger.Error("got error creating bet reference %s ", err.Error())
		return 0, err
	}

	stmt.Close()
	return int(reference.Int64), nil

}

func GetProfileFromMSISDN(db *sql.DB, redisConn *redis.Client, cfg *ini.File,msisdn int64,firstName string, otherName string,src string,bal int64,createIfNil bool) (*models.Profile, error) {

	var profile models.Profile

	var profileID, status sql.NullInt64
	var balance, points sql.NullInt64
	var first_name, other_name sql.NullString
	var created mysql.NullTime

	err := db.QueryRow(`SELECT p.id,p.first_name, p.other_name, p.status, p.created,pb.balance,
       		pb.points FROM profile p INNER JOIN profile_balance pb ON p.id=pb.profile_id WHERE p.msisdn = ? `,
		msisdn).Scan(&profileID, &first_name, &other_name, &status, &created, &balance, &points)

	if err == sql.ErrNoRows {

		if createIfNil {

			password := "822822"
			code := 822822

			prof, message := RegisterProfile(db,redisConn, msisdn, password, code, firstName, otherName,  src, bal)

			if prof == nil {

				return nil, errors.New(message)
			}

			SendSMS(db,cfg,"REGISTRATION", msisdn, message)

			profile = *prof
		}

	} else if err != nil {

		return nil, err

	} else {

		profile.ID = profileID.Int64
		profile.FirstName = first_name.String
		profile.OtherName = other_name.String
		profile.Msisdn = msisdn
		profile.Balance = balance.Int64
		profile.Points = points.Int64
		profile.Created = ToMysql(created.Time)
		profile.Status = status.Int64

	}

	message, err := json.Marshal(profile)

	if err != nil {

		logger.Error(" got error decoding profile to string %s ", aurora.Red(err.Error()))
		return nil, err
	}

	redisKey := fmt.Sprintf("PROFILE-%d", profileID.Int64)

	SetRedisKeyWithExpiry(redisConn, redisKey, string(message), 120)

	return &profile, nil
}

func GetProfileIDFromMSISDN(db *sql.DB, msisdn int64) (int64, error) {

	var profileID sql.NullInt64
	err := db.QueryRow(`SELECT p.id FROM profile p WHERE p.msisdn = ? `, msisdn).Scan(&profileID)

	if err == sql.ErrNoRows {

		return 0, err
	}

	if err != nil {

		return 0, err
	}

	return profileID.Int64,nil
}

func ProfileToken(msisdn int64) string {
	salt := r.Letters(20)
	return MD5S(fmt.Sprintf("%d%s%d",msisdn,salt,msisdn))
}

func MD5S (s string) string {

	h := md5.New()
	h.Write([]byte(s))
	return fmt.Sprintf("%x", h.Sum(nil))
}