package library

import (
	"credo/app/logger"
	"credo/config"
	"encoding/json"
	"github.com/logrusorgru/aurora"
	"github.com/streadway/amqp"
	"gopkg.in/ini.v1"
	"time"
)

type Consumer struct {
	conn    *amqp.Connection
	channel *amqp.Channel
	tag     string
	done    chan error
}

func getTime() int64 {

	return int64(time.Now().UnixNano() / 1000000)
}

func Publish(conn *amqp.Connection,cfg *ini.File, name string, payload interface{}, priority uint8) error {

	queue, err := config.GetKey(cfg,name, "queue")
	if err != nil {
		logger.Error(err.Error())
	}

	exchange, err := config.GetKey(cfg,name, "exchange")
	if err != nil {
		logger.Error(err.Error())
	}

	key, err := config.GetKey(cfg,name, "routing_key")
	if err != nil {
		logger.Error(err.Error())
	}

	exchangeType, err := config.GetKey(cfg,name, "exchange_type")
	if err != nil {
		logger.Error(err.Error())
	}

	//a1 := getTime();
	//logger.Log(" %s INITIALIZING Consumer with parameters queue %s exchange %s exchangeType %s routing_key %s ",name,queue,exchange,exchangeType,key)

	ch, err := conn.Channel()

	if err != nil {

		logger.Critical(" got error oppening rabbitMQ channel %s ", aurora.Red(err.Error()))
		return err
	}

	//logger.Log("%s Got Queue %s and exchange type %s ",name,queue,exchangeType)

	defer ch.Close()

	err = ch.ExchangeDeclare(
		queue,        // name
		exchangeType, // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // no-wait
		nil,          // arguments
	)

	if err != nil {

		logger.Critical(" got error Failed to declare a queue %s error %s ",name, aurora.Red(err.Error()))
		return err
	}

	message, err := json.Marshal(payload)

	if err != nil {

		logger.Critical(" got error decoding payload to string %s ", aurora.Red(err.Error()))
		return err
	}

	err = ch.Publish(
		exchange, // exchange
		key,      // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        message,
			Priority:    priority,
		})

	if err != nil {

		logger.Critical(" got error publishing message %s error %s ", aurora.Green(message), aurora.Red(err.Error()))
		return err
	}

	//a2 := getTime();
	//tt := a2 - a1

	//logger.Log(" PUBLISH TO %s TT %dms ",queue, tt)

	return nil
}