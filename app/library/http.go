package library

import (
	"credo/app/models"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/logrusorgru/aurora"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"credo/app/logger"
	"strings"
	"sync"
	"time"
)

var (
	once              sync.Once
	netClient         *http.Client
)

func HTTPPost(url string, payload interface{},headers map[string] string) (int,string) {

	jsonData, err := json.Marshal(payload)

	if err != nil {

		logger.Error("Got error decoding payload error %v ", aurora.Red(err.Error()))
		return 0,""
	}

	var jsonStr = jsonData

	logger.Info("GOT payload %s ",string(jsonData))

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {

		logger.Error("got error executing post request to %s error %s ",url,err.Error())
		return 0,""
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	if headers != nil {

		for k,v := range headers {

			logger.Error("HEADERS %s ==> %s",k,v)
			req.Header.Set(k,v)
		}

	}

	resp, err := newNetClient().Do(req)
	if err != nil {

		logger.Error("Request timeouts error executing post request to %s error %s ",url,err.Error())
		return 0,""
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {

		logger.Error("Got error retrieving response body %s ",err.Error())
	}

	status := resp.StatusCode

	logger.Info(" Got status %d posting data from URL %s response %s ",status,url,string(body))

	return status, string(body)

}

func HTTPPostWithHeaders(url string, payload interface{},headers map[string] string) (int,models.HTTPResponse ) {

	jsonData, err := json.Marshal(payload)

	if err != nil {

		logger.Error("Got error decoding payload error %v ", aurora.Red(err.Error()))
		return 0,models.HTTPResponse{
			Status: 0,
			Message: err.Error(),
		}
	}

	logger.Info("GOT payload %s ",string(jsonData))

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {

		logger.Error("got error executing post request to %s error %s ",url,err.Error())
		return 0,models.HTTPResponse{
			Status: 0,
			Message: err.Error(),
		}
	}

	for k,v := range headers {

		req.Header.Set(k,v)
	}

	resp, err := newNetClient().Do(req)

	if err != nil {

		logger.Error("got error executing post request to %s error %s ",url,err.Error())
		return 0,models.HTTPResponse{
			Status: 0,
			Message: err.Error(),
		}
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {

		logger.Error("Got error retrieving response body %s ",err.Error())
	}

	status := resp.StatusCode

	logger.Info(" Got status %d posting data from URL %s response %s ",status,url,string(body))

	c2bCallback := models.HTTPResponse{}

	err = json.Unmarshal(body, &c2bCallback)
	if err != nil {

		return status,models.HTTPResponse{
			Status: 200,
			Message: err.Error(),
		}
	};

	return status,c2bCallback

}

func PayKonnectHTTPPostWithHeaders(url string, payload interface{},headers map[string] string) (int,string ) {

	jsonData, err := json.Marshal(payload)

	if err != nil {

		logger.Error("Got error decoding payload error %v ", aurora.Red(err.Error()))
		return 0,err.Error()
	}

	logger.Info("GOT payload %s ",string(jsonData))

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))


	for k,v := range headers {

		req.Header.Set(k,v)
	}

	resp, err := newNetClient().Do(req)

	if err != nil {

		logger.Error("got error executing post request to %s error %s ",url,err.Error())
		return 0,err.Error()
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {

		logger.Error("Got error retrieving response body %s ",err.Error())
	}

	status := resp.StatusCode

	logger.Info(" Got status %d posting data from URL %s response %s ",status,url,string(body))

	return status,string(body)

}

func HTTPGet(remoteURL string,payload  map[string]string) (int,string) {

	var fields[] string

	for key,value := range payload {

		val := fmt.Sprintf("%s=%v",key,url.QueryEscape(value))

		fields = append(fields,val)
	}

	params := strings.Join(fields,"&")

	endpoint := fmt.Sprintf("%s?%s",remoteURL,params)

	logger.Info(" Wants to GET data to URL %s ",endpoint)

	resp, err := http.Get(endpoint)

	if err != nil {

		logger.Error(" Got error posting data to URL %s error %s ",endpoint,err.Error())

		return 500,err.Error()
	}

	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	status := resp.StatusCode

	logger.Info(" Got status %d GETing data from URL %s response %s ",status,endpoint,string(body))

	return status,string(body)
}

func newNetClient() *http.Client {

	once.Do(func() {
		var netTransport = &http.Transport{
			Dial: (&net.Dialer{
				Timeout: 120 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 120 * time.Second,
		}
		netClient = &http.Client{
			Timeout:   time.Second * 120,
			Transport: netTransport,
		}
	})

	return netClient
}

func HTTPPostV1(url string, payload interface{}) int {

	jsonData, err := json.Marshal(payload)

	if err != nil {

		logger.Error("Got error decoding payload error %v ", aurora.Red(err.Error()))
		return 0
	}

	var jsonStr = []byte(jsonData)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	//client := &http.Client{}

	resp, err := newNetClient().Do(req)

	if err != nil {

		logger.Error("got error executing post request to %s error %s ",url,err.Error())
		return 0
	}

	// read unti all is complete
	_, err = ioutil.ReadAll(resp.Body)

	if err != nil {

		logger.Error("got error reading response nody %s ",err.Error())

	}

	//defer resp.Body.Close()

	status := resp.StatusCode
	return status

}

func HTTPPostForm(endpoint string, payload map[string]string) (int,string) {

	formData := url.Values{}

	for key,value := range payload {

		formData.Add(key,value)
	}

	resp, err := http.PostForm( endpoint,formData)

	if err != nil {

		logger.Error("got error executing post request to %s error %s ",endpoint,err.Error())
		return 0,""
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {

		logger.Error("Got error retrieving response body %s ",err.Error())
	}

	status := resp.StatusCode

	logger.Info(" Got status %d posting data from URL %s response %s ",status,endpoint,string(body))

	return status, string(body)

}
