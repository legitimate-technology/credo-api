package library

import (
	"credo/app/logger"
	"credo/config"
	"crypto/hmac"
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/ini.v1"
	"net/url"
	"strings"
)

func TokenFormatter(tokens string) string {

	parts := strings.Split(tokens,"")
	counts := 4

	var groups []string
	group := ""

	for z,y := range parts {

		x := z +1

		group = strings.TrimSpace(fmt.Sprintf("%s%s",group,y))

		if x > 0 && x % counts == 0 {

			groups = append(groups,group)
			group = ""
		}

	}


	if len(group) > 0 {

		groups = append(groups,group)
		group = ""
	}

	return strings.Join(groups,"-")

}

func baiKPLCToken(db *sql.DB, cfg *ini.File,transactionID int64,amount int64,deviceno string,msisdn int64) error {

	t1 := GetTime()

	provider := "bai"

	remoteURL, err := config.GetKey(cfg,provider,"tokens_url")
	if err != nil {

		remoteURL= "https://api.vaspro.co.ke/v3/rewards/airtime"
	}

	apiKey, err := config.GetKey(cfg,provider,"apiKey")
	if err != nil {
		apiKey= "65e6262e01f1704ee548239c84e96f01"
	}

	callbackURL, err := config.GetKey(cfg,provider,"tokens_callback")
	if err != nil {

		callbackURL= "https://credo.smestech.com/tokens/callback/{transactionID}"
	}

	callbackURL = strings.Replace(callbackURL,"{transactionID}",fmt.Sprintf("%d",transactionID),-1)

	params := map[string] string{
		"apiKey":apiKey,
		"bill_reference": deviceno,
		"account_type":"1",// 1- prepaid 2- postpaid
		"unique_id": fmt.Sprintf("CREDO_%d",transactionID),
		"msisdn":fmt.Sprintf("%d",msisdn),
		"amount": fmt.Sprintf("%d",amount),
		"callback":callbackURL,
	}

	requestPayload, _ := json.Marshal(params)

	status, responseBody := HTTPPost(remoteURL,params,nil)

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ",err.Error())
		return err
	}

	requestSTMT, err := tx.Prepare("INSERT INTO api_request (transaction_id, provider, url, request_type, request_body, response_body, external_reference,http_status, response_status, response_description, status, time_taken,operator_code, created) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,now())   ")
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}

	_, err = requestSTMT.Exec(transactionID, provider, remoteURL, "POST", requestPayload, responseBody, transactionID,status, status, responseBody, status,GetTime() - t1,"0")

	tx.Commit()

	return nil
}

func payconnectKPLCToken(db *sql.DB, cfg *ini.File,transactionID int64,amount int64,deviceno string,msisdn int64) error {

	provider := "paykonnect"

	agentid, err := config.GetKey(cfg,provider,"agentid")
	if err != nil {

		agentid= "20"
	}

	retailerid, err := config.GetKey(cfg,provider,"retailerid")
	if err != nil {

		retailerid= agentid
	}

	operatorcode := "5"
	denomination := "0"

	agentpwd, err := config.GetKey(cfg,provider,"agentpwd")
	if err != nil {

		agentpwd= "infinity20"
	}

	endpoint, err := config.GetKey(cfg,provider,"url")
	if err != nil {

		retailerid= "http://193.104.202.165/kenya/mainlinkpos/purchase"
	}

	endpoint = fmt.Sprintf("%s/pw_trans.php3",endpoint)

	params := map[string] string{
		"agentid":agentid,
		"transid": fmt.Sprintf("%d%d",transactionID,GetTime()),
		"retailerid":retailerid,
		"operatorcode":operatorcode,
		"circode":"1",
		"product":"",
		"denomination":denomination,
		"recharge": fmt.Sprintf("%d",amount),
		"mobileno": getPayKonnectMSISDN(msisdn),
		"deviceno": deviceno,
		"bulkqty": "1",
		"narration": fmt.Sprintf("Airtime purchase for %d ",msisdn),
		"agentpwd": agentpwd,
		"loginstatus": "LIVE",
		"appver": "1.0",
		"custid":fmt.Sprintf("%d",transactionID),
	}

	var fields[] string

	for key,value := range params {

		val := fmt.Sprintf("%s=%v",key,url.QueryEscape(value))

		fields = append(fields,val)
	}

	parameters := strings.Join(fields,"&")

	endpoint = fmt.Sprintf("%s?%s",endpoint,parameters)

	logger.Info(" Wants to POST data to URL %s ",endpoint)

	headers := map[string] string {
		"accept":"plain/text",
		"content-type":"plain/text",
	}

	ta := GetTime()
	httpStatus,responsePayload := PayKonnectHTTPPostWithHeaders(endpoint,params,headers)
	tt := GetTime() - ta

	// remove end of data
	body := strings.Replace(responsePayload,"$$$","",-1)

	// split into fields
	parts := strings.Split(body,"%$")
	// split into fields

	logger.Info("Got response here as parts %d => %s ", len(parts),responsePayload)

	if len(parts) < 11 {

		return errors.New(body)
	}

	logger.Info("At 1 here ")
	//%$ %$ %$transdatetime%$token%$tokenunits%$bssttoken%$bssttokenunits%$responsecode%$responsemessage%$status$$$

	// check if response was a success
	merchanttransid := parts[0]
	pktransid := parts[1]
	operatortransid := parts[2]
	//transdatetime := parts[3]
	token := parts[4]
	tokenunits := parts[5]
	bssttoken := parts[6]

	next := 8
	bssttokenunits := parts[7]

	if bssttoken == "0" {

		bssttokenunits = "0"
		next = 7
	}

	responsecode := parts[next]
	responsemessage := parts[next+1]
	status := parts[next+2]

	st := 1

	if status == "SUCCESS" {

		st = 2
	}

	if status == "PENDING" {

		st = 1
	}

	if status == "FAILED" {

		st = 0
	}

	logger.Info("At 2 here ")

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ",err.Error())
		return err
	}

	requestSTMT, err := tx.Prepare("INSERT INTO api_request (transaction_id, provider, url, request_type, request_body, response_body, external_reference,http_status, response_status, response_description, status,operator_code, time_taken, created) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, now())   ")
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}
	defer requestSTMT.Close()

	res, err := requestSTMT.Exec(transactionID, provider, endpoint, "POST", endpoint, responsePayload, pktransid,httpStatus, status, responsecode, st,operatorcode, tt)
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}
	apiRequestID, _ := res.LastInsertId()

	kplcSTMT, err := tx.Prepare("INSERT INTO kplc_token_log (api_request_id, merchanttransid, pktransid, operatortransid, token, tokenunits, bssttoken, bssttokenunits, responsecode, responsemessage, status, created) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now()) ")
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}

	defer kplcSTMT.Close()

	_, err = kplcSTMT.Exec(apiRequestID, merchanttransid, pktransid, operatortransid, token, tokenunits, bssttoken, bssttokenunits, responsecode, responsemessage, status)
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}

	tx.Commit()

	if st == 2 {

		logger.Info("At 3 here ")
		sms := fmt.Sprintf("KPLC Token\nMtrNo: %s\nToken: %s\nUnits: %s\nToken Amount: %d",deviceno,TokenFormatter(token),tokenunits,amount)
		logger.Info("At 4 here ")

		SendSMS(db,cfg,"TOKENS",msisdn,sms)
	}

	return nil
}

func PurchaseKPLCToken(db *sql.DB, cfg *ini.File,transactionID int64,amount int64,meterNumber string,msisdn int64) error {

	provider, _ := config.GetKey(cfg,"tokens_provider","provider")

	if provider == "paykonnect" {

		return payconnectKPLCToken(db,cfg,transactionID,amount,meterNumber,msisdn)

	} else if provider == "kyanda" {

		return kiandaKPLCToken(db,cfg,transactionID,amount,meterNumber,msisdn)

	} else if provider == "bai" {

		return baiKPLCToken(db,cfg,transactionID,amount,meterNumber,msisdn)
	}

	return nil
}

func kiandaKPLCToken1(db *sql.DB, cfg *ini.File,transactionID int64,amount int64,deviceno string,msisdn int64) error {

	t1 := GetTime()

	provider := "bai"

	remoteURL, err := config.GetKey(cfg,provider,"tokens_url")
	if err != nil {

		remoteURL= "https://api.vaspro.co.ke/v3/rewards/airtime"
	}

	apiKey, err := config.GetKey(cfg,provider,"apiKey")
	if err != nil {
		apiKey= "65e6262e01f1704ee548239c84e96f01"
	}

	callbackURL, err := config.GetKey(cfg,provider,"tokens_callback")
	if err != nil {

		callbackURL= "https://credo.smestech.com/tokens/callback/{transactionID}"
	}

	callbackURL = strings.Replace(callbackURL,"{transactionID}",fmt.Sprintf("%d",transactionID),-1)

	params := map[string] string{
		"apiKey":apiKey,
		"bill_reference": deviceno,
		"account_type":"1",// 1- prepaid 2- postpaid
		"unique_id": fmt.Sprintf("CREDO_%d",transactionID),
		"msisdn":fmt.Sprintf("%d",msisdn),
		"amount": fmt.Sprintf("%d",amount),
		"callback":callbackURL,
	}

	requestPayload, _ := json.Marshal(params)

	status, responseBody := HTTPPost(remoteURL,params,nil)

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error starting db transaction %s ",err.Error())
		return err
	}

	requestSTMT, err := tx.Prepare("INSERT INTO api_request (transaction_id, provider, url, request_type, request_body, response_body, external_reference,http_status, response_status, response_description, status, time_taken,operator_code, created) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,now())   ")
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}

	_, err = requestSTMT.Exec(transactionID, provider, remoteURL, "POST", requestPayload, responseBody, transactionID,status, status, responseBody, status,GetTime() - t1,"0")

	tx.Commit()

	return nil
}

func kiandaKPLCToken(db *sql.DB, cfg *ini.File,transactionID int64,amount int64,account string,initiatorPhone int64) error {

	t1 := GetTime()

	provider := "kyanda"

	remoteURL, err := config.GetKey(cfg,provider,"kplc_url")
	if err != nil {

		remoteURL= "https://api.kyanda.app/billing/v1/bill/create"
	}

	remoteURL= "https://api.kyanda.app/billing/v1/bill/create"

	apiKey, err := config.GetKey(cfg,provider,"apiKey")
	if err != nil {
		apiKey= "86b0f017dbed4c4783e9c93c95a4086a"
	}

	MerchantID, err := config.GetKey(cfg,provider,"MerchantID")
	if err != nil {
		MerchantID= "credo"
	}

	telco := "KPLC_PREPAID"

	//amount+account+telco+initiatorPhone+MerchantID
	data := fmt.Sprintf("%d%s%s%d%s",amount,account,telco,initiatorPhone,MerchantID)
	secret := apiKey

	// Create a new HMAC by defining the hash type and the key (as byte array)
	h := hmac.New(sha256.New, []byte(secret))
	// Write Data to it
	h.Write([]byte(data))

	// Get result and encode as hexadecimal string
	//signature := hex.EncodeToString(h.Sum(nil))

	signature1,_ := getHash(amount,account,fmt.Sprintf("%d",initiatorPhone),apiKey, MerchantID, telco)

	params := map[string] string{
		"telco": telco,
		"initiatorPhone": fmt.Sprintf("%d",initiatorPhone),
		"account": account,
		"amount": fmt.Sprintf("%d",amount),
		"signature":signature1,
		"MerchantID": MerchantID,
	}

	headers := map[string]string {
		"apiKey": apiKey,
		"Accept": "application/json",
		"Content-Type": "application/json",
	}

	requestPayload, _ := json.Marshal(params)
	status, responseBody := HTTPPost(remoteURL,params,headers)

	resp := new(KiandaAirtimeResponse)

	if status >= 200 && status <= 299 {

		err = json.Unmarshal([]byte(responseBody),resp)
		if err != nil {

			logger.Error("got error decoding kyanda response %s ",err.Error())
			resp.Status = "Failed"
		}
	}

	logger.Error("kiandaKPLCToken log api-request")
	requestSTMT, err := db.Prepare("INSERT INTO api_request (transaction_id, provider, url, request_type, request_body, response_body, external_reference,http_status, response_status, response_description, status, time_taken,operator_code, created) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,now())   ")
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}
	defer requestSTMT.Close()

	_, err = requestSTMT.Exec(transactionID, provider, remoteURL, "POST", requestPayload, responseBody, resp.MerchantReference,status, resp.Status, resp.Transactiontxt, resp.Status,GetTime() - t1,0)
	if err != nil {

		logger.Error("got error preparing db transaction %s ",err.Error())
		return err
	}
	if status > 299 || status < 200 {

		return errors.New(responseBody)

	}

	return nil
}

func KiandaRegisterURL(cfg *ini.File) error {

	//initiatorPhone = 254782444470

	provider := "kyanda"

	remoteURL := "https://api.kyanda.app/billing/v1/callback-url/create"

	apiKey, err := config.GetKey(cfg,provider,"apiKey")
	if err != nil {
		apiKey= "86b0f017dbed4c4783e9c93c95a4086a"
	}

	MerchantID, err := config.GetKey(cfg,provider,"MerchantID")
	if err != nil {
		MerchantID= "credo"
	}

	callbackURL := "https://credo.smestech.com/tokens/callback"

	//MerchantID
	data := fmt.Sprintf("%s",MerchantID)
	secret := apiKey

	// Create a new HMAC by defining the hash type and the key (as byte array)
	h := hmac.New(sha256.New, []byte(secret))
	// Write Data to it
	h.Write([]byte(data))

	// Get result and encode as hexadecimal string
	signature := hex.EncodeToString(h.Sum(nil))

	signature1,_ := getHash1(apiKey, MerchantID,remoteURL)

	logger.Error("GOT signature %s signature1 %s ",signature,signature1)

	params := map[string] string{
		"signature":signature1,
		"MerchantID": MerchantID,
		"callbackURL": callbackURL,
	}

	headers := map[string]string {
		"apiKey": apiKey,
		"Accept": "application/json",
		"Content-Type": "application/json",
	}

	status, responseBody := HTTPPost(remoteURL,params,headers)

	resp := new(KiandaAirtimeResponse)

	if status >= 200 && status <= 299 {

		err = json.Unmarshal([]byte(responseBody),resp)
		if err != nil {

			logger.Error("got error decoding kyanda response %s ",err.Error())
			resp.Status = "Failed"
		}
	}

	if status > 299 || status < 200 {

		return errors.New(responseBody)

	}

	return nil
}
