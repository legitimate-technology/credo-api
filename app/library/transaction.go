package library

import (
	"credo/app/logger"
	"database/sql"
)

func CreateTransaction(tx *sql.Tx,profileID int64, transactionType int64,referenceID int64,transactionID int64,amount int64,balance int64,description string) (id int64, err error)  {

	logger.Error("wants to insert into transaction ")

	stmt, err := tx.Prepare("INSERT INTO transaction (profile_id, transaction_type, transaction_id, reference_id, amount, balance, description, created) VALUE (?, ?, ?, ?, ?, ?, ?, now())")
	if err != nil {

		logger.Error("Got error preparing transaction query %s ",err.Error())
		return 0, err
	}
	defer stmt.Close()

	res, err := stmt.Exec(profileID, transactionType, transactionID, referenceID, amount, balance, description)
	if err != nil {

		logger.Error("Got error executing transaction query %s ",err.Error())
		return 0, err
	}

	id, err = res.LastInsertId()
	if err != nil {

		logger.Error("Got error retrieing last insert ID %s ",err.Error())
		return 0, err
	}


	logger.Error("DOne inserting into transaction ")

	return id, nil
}

func CreateTransaction1(db *sql.DB,profileID int64, transactionType int64,referenceID int64,transactionID int64,amount int64,balance int64,description string) (id int64, err error)  {

	stmt, err := db.Prepare("INSERT INTO transaction (profile_id, transaction_type, transaction_id, reference_id, amount, balance, description, created) VALUE (?, ?, ?, ?, ?, ?, ?, now())")
	if err != nil {

		logger.Error("Got error preparing transaction query %s ",err.Error())
		return 0, err
	}

	defer stmt.Close()
	res, err := stmt.Exec(profileID, transactionType, transactionID, referenceID, amount, balance, description)
	if err != nil {

		logger.Error("Got error executing transaction query %s ",err.Error())
		return 0, err
	}

	id, err = res.LastInsertId()
	if err != nil {

		logger.Error("Got error retrieing last insert ID %s ",err.Error())
		return 0, err
	}

	return id, nil

}
