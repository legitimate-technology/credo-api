package library

import (
	"credo/app/logger"
	"credo/config"
	"database/sql"
	"encoding/base64"
	"fmt"
	"github.com/logrusorgru/aurora"
	"gopkg.in/ini.v1"
	"redis"
	"strings"
)

type SiliconSMS struct {
	Sender    string `json:"sender"`
	Message   string `json:"message"`
	Recipient string `json:"recipient"`
	Bulk      int    `json:"bulk"`
	CallBack  string `json:"call_back"`
}
func BuildSMS(message string,placeholder map[string]string ) string {

	for key,value := range placeholder {

		message = strings.Replace(message, fmt.Sprintf("[%s]",key), value, -1)

	}

	message = strings.Replace(message, "{line}","\n", -1)

	return message
}

func SendSMS1(cfg *ini.File,msisdn int64,message string) {

	logger.Info(" Wants to send SMS to phone %d message %s ",msisdn,message)

	urlString, err := config.GetKey(cfg,"sms","url")
	if err != nil {

		urlString ="https://vas.ujumbetech.co.ke/api/message/send/sms"
	}

	user_id, err := config.GetKey(cfg,"sms","user_id")
	if err != nil {

		user_id =""
	}

	org_id, err := config.GetKey(cfg,"sms","org_id")
	if err != nil {

		org_id =""
	}

	src_address, err := config.GetKey(cfg,"sms","src_address")
	if err != nil {

		src_address ="SMSAfrica"
	}

	auth_key, err := config.GetKey(cfg,"sms","auth_key")
	if err != nil {

		src_address ="auth_key"
	}

	app_id, err := config.GetKey(cfg,"sms","app_id")
	if err != nil {

		src_address ="SMSAfrica"
	}

	timestamp, err := config.GetKey(cfg,"sms","timestamp")
	if err != nil {

		timestamp ="20200514152813"
	}

	payload := map[string]interface{}{
		"reference": getTime(),
		"user_id": user_id,
		"org_id": org_id,
		"subject": "Send SMS",
		"src_address": src_address,
		"dst_address":    msisdn,
		"message_type": "1",
		"auth_key": auth_key,
		"app_id": app_id,
		"operation": "send",
		"timestamp": timestamp,
		"service_id": "",
		"keyword": "",
		"message": message,
	}

	header := map[string] string {
		"Content-Type": "application/json",
		"Accept": "application/json",
	}

	HTTPPostWithHeaders(urlString,payload,header)

}

func SendSMS(db *sql.DB,cfg *ini.File,sms_type string,msisdn int64,message string) {

	stmt, err := db.Prepare("INSERT INTO outbox (msisdn, sms_type, message,message_length,status, created) VALUE (?,?,?,?,?,now())")
	if err == nil {

		_, err = stmt.Exec(msisdn,sms_type,message,len(message),1)
		if err != nil {

			logger.Error("got error saving sms %s ",err.Error())
		}

	} else {

		logger.Error("got error saving sms %s ",err.Error())
	}

	go func() {

		var username, password string

		sender_id, err := config.GetKey(cfg, "sms", "sender_id")
		if err != nil {

			logger.Error(" cant retriving sms sender_id got error %s ", aurora.Red(err.Error()))
			return
		}

		username, err = config.GetKey(cfg, "sms", "username")
		if err != nil {

			logger.Error(" cant retriving sms username got error %s ", aurora.Red(err.Error()))
			return
		}

		password, err = config.GetKey(cfg, "sms", "password")
		if err != nil {

			logger.Error(" cant retriving sms password got error %s ", aurora.Red(err.Error()))
			return
		}

		url, err := config.GetKey(cfg, "sms", "url")
		if err != nil {

			logger.Error(" cant retriving sms url got error %s ", aurora.Red(err.Error()))
			return
		}

		payload := SiliconSMS{
			Sender:    sender_id,
			Recipient: fmt.Sprintf("%d",msisdn),
			Message:   message,
			Bulk:      1,
		}

		auth := fmt.Sprintf("%s:%s", username, password)
		basicAuth := base64.StdEncoding.EncodeToString([]byte(auth))

		headers := make(map[string] string)
		headers["Authorization"] = fmt.Sprintf("Basic %s",basicAuth)
		headers["Content-Type"] = "application/json"
		headers["Accept"] = "application/json"

		status, body := HTTPPostWithHeaders(url,payload,headers)
		if status == 0 {

			logger.Error("Failed to make HTTP Request got error %s ",body)
			return
		}

	}()

	return
}

func GetSMSTemplate(db *sql.DB,redisConn *redis.Client,templateName string) string {

	// retrieve SMS template
	templateName = fmt.Sprintf("SMS:TEMPLATE:%s",templateName)

	sms, err := GetRedisKey(redisConn,templateName)

	if err != nil || len(sms) == 0 {

		//logger.Error(" got error retrieving SMS template %s error %s",templateName,err.Error())

		var tName sql.NullString
		err = db.QueryRow("SELECT message FROM sms_template WHERE name = ? LIMIT 1 ",templateName).Scan(&tName)
		if err != nil {

			logger.Error("%s",aurora.Red(fmt.Sprintf(" got error retriving sms template for %s error %s",templateName, err.Error())))
			sms = ""

		} else if !tName.Valid {

			logger.Error("%s",aurora.Red(fmt.Sprintf(" got error retriving sms template for %s template does not exist",templateName)))
			sms = ""

		} else  {

			sms = tName.String

		}

		err = SetRedisKey(redisConn,templateName,sms)
		if err != nil {

			logger.Error("got error saving SMS Templates")
		}
	}

	return sms

}