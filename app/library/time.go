package library

import (
	"credo/app/logger"
	"math/rand"
	"time"
)

func GetTime() int64 {

	return int64(time.Now().UnixNano() / 1000000)
}

func GameID() int {

	min := 1001
	max := 9999

	return min + rand.Intn(max-min)

}

func MysqlNow() string {

	return time.Now().Format("2006-01-02 03:04:05")
}


func ToMysql(t time.Time) string {

	//loc, _ := time.LoadLocation("Africa/Nairobi")
	//return t.In(loc).Format("2006-01-02 15:04:05")

	tt := t.Format("2006-01-02 15:04:05")

	//logger.Info("got mysqlNow as %s ",tt)

	return tt
}

func ToNairobiTime(t time.Time) string {

	loc := time.FixedZone("spotika", +3 *60*60 )
	return t.In(loc).Format("2006-01-02 15:04:05")

	loc, err := time.LoadLocation("Africa/Nairobi")

	if err != nil {

		logger.Error("got error loading timezone %s ",err.Error())
		return t.Format("2006-01-02 15:04:05")
	}

	return t.In(loc).Format("2006-01-02 15:04:05")

	//return t.Format("2006-01-02 15:04:05")
}
