package library

import (
	"credo/app/logger"
	"database/sql"
	"github.com/streadway/amqp"
	"gopkg.in/ini.v1"
	"strconv"
	"strings"
)

type Job struct {

	TransactionType int64
	TransactionID int64
}

func ResendFailedWithWrongStatus(db *sql.DB,cfg *ini.File,conn *amqp.Connection) {

	rows, err := db.Query("SELECT id,transaction_type,amount,reference FROM utility WHERE status IN (3) AND id NOT IN (select transaction_id FROM api_request WHERE status BETWEEN 200 AND 299 ) AND retry_count < 10 AND (TIMESTAMPDIFF(MINUTE,last_retry,now()) > 4 OR last_retry IS NULL) ")
	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		return
	}

	defer rows.Close()

	x := 0

	st, err := db.Prepare("UPDATE utility SET retry_count = retry_count + 1 WHERE id = ? ")
	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		return
	}

	for rows.Next() {

		x++
		var id,transaction_type,amount sql.NullInt64
		var reference sql.NullString

		err = rows.Scan(&id,&transaction_type,&amount,&reference)

		if err == nil {

			_, err := st.Exec(id.Int64)

			if err != nil {

				logger.Error("Got error updating transaction retry_count %s ",err.Error())
			}

			logger.Info("Got at 1 with %d amount %d reference %s ",x,amount.Int64,reference.String)

			if transaction_type.Int64 == 1 {

				err = Publish(conn, cfg, "AIRTIME", id.Int64, 0)

			} else {

				err = Publish(conn, cfg, "KPLC_TOKEN", id.Int64, 0)

			}

		} else  {

			logger.Error("Could not scan data got error %s ",err.Error())

		}
	}

	st.Close()

	return
}

func ResendFailed(db *sql.DB,cfg *ini.File,conn *amqp.Connection) {

	rows, err := db.Query("SELECT id,transaction_type,amount,reference FROM utility WHERE status IN (1,2,3) AND id NOT IN (select transaction_id FROM api_request ) AND retry_count < 10 AND (TIMESTAMPDIFF(MINUTE,last_retry,now()) > 4 OR last_retry IS NULL) ")
	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		return
	}


	defer rows.Close()

	x := 0

	st, err := db.Prepare("UPDATE utility SET retry_count = retry_count + 1 WHERE id = ? ")
	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		return
	}

	for rows.Next() {

		x++
		var id,transaction_type,amount sql.NullInt64
		var reference sql.NullString

		err = rows.Scan(&id,&transaction_type,&amount,&reference)

		if err == nil {

			_, err := st.Exec(id.Int64)

			if err != nil {

				logger.Error("Got error updating transaction retry_count %s ",err.Error())
			}

			logger.Info("Got at 1 with %d amount %d reference %s ",x,amount.Int64,reference.String)

			if transaction_type.Int64 == 1 {

				err = Publish(conn, cfg, "AIRTIME", id.Int64, 0)

			} else {

				err = Publish(conn, cfg, "KPLC_TOKEN", id.Int64, 0)

			}

		} else  {

			logger.Error("Could not scan data got error %s ",err.Error())

		}
	}

	st.Close()

	return
}

func RevertPermanentlyFailed(db *sql.DB,cfg *ini.File) {

	rows, err := db.Query("SELECT t.id,p.reference,p.short_code,p.amount,p.id as payment_id,p.profile_id FROM utility t INNER JOIN payment p ON t.id=p.transaction_id WHERE t.status IN (1,2,3) AND t.id NOT IN (select transaction_id FROM api_request ) AND t.retry_count = 10 AND (TIMESTAMPDIFF(MINUTE,t.last_retry,now()) > 5 OR t.last_retry IS NULL) ")
	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		return
	}

	defer rows.Close()

	x := 0

	tx, err := db.Begin()
	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		return
	}

	stUpdate, err := tx.Prepare("UPDATE utility SET retry_count = retry_count + 1, status = -1, description = 'Permanently Failed', last_retry = now() WHERE id = ? ")
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		return
	}

	defer stUpdate.Close()


	/*
	stReversalSTMT, err := tx.Prepare("INSERT INTO reversal (payment_id, profile_id, amount, reference, short_code, status, created) VALUE (?, ?, ?, ?, ?, 0, now()) ")
	if err != nil {

		tx.Rollback()
		logger.Error("got error executing transaction %s ",err.Error())
		return
	}

	defer stReversalSTMT.Close()
	*/

	for rows.Next() {

		x++
		var id,short_code,amount,payment_id,profile_id sql.NullInt64
		var reference sql.NullString

		err = rows.Scan(&id,&reference,&short_code,&amount,&payment_id,&profile_id)

		if err == nil {

			/*
			res, err := stReversalSTMT.Exec(payment_id.Int64, profile_id.Int64, amount.Int64, reference.String, short_code.Int64)

			if err != nil {

				logger.Error("gor error creating reversals %s ",err.Error())
				continue
			}

			reversalID, err := res.LastInsertId()

			if err != nil {

				logger.Error("gor error retrieving reversalID %s ",err.Error())
				continue
			}

			// initiate STK Push
			paymentURL, err := config.GetKey(cfg,"payment","reversalURL")
			if err != nil {

				logger.Error("got error retrieving payment->reversalURL from configs %s ",err.Error())
			}

			reverseCallbackURL, err := config.GetKey(cfg,"payment","reverseCallbackURL")
			if err != nil {

				logger.Error("got error retrieving payment->reverseCallbackURL from configs %s ",err.Error())
			}

			token, err := config.GetKey(cfg,"payment","token")
			if err != nil {

				logger.Error("got error retrieving payment->token from configs %s ",err.Error())
				token = ""
			}

			reverseCallbackURL = strings.Replace(reverseCallbackURL,"{reversalID}",fmt.Sprintf("%d",reversalID),-1)

			var requestPayload map[string]interface{}

			requestPayload = map[string]interface{}{
				"short_code":   short_code.Int64,
				"transaction_id": reference.String,
				"callback_url": reverseCallbackURL,
			}

			headers := map[string] string {

				"Authorization": token,
				"Content-Type": "application/json",
				"Accept": "application/json",
			}

			status, msg := HTTPPostWithHeaders(paymentURL,requestPayload,headers)

			if status == 200 { */

				_, err = stUpdate.Exec(id.Int64)
				if err != nil {

					logger.Error("got error updating transaction to reversed %s ",err.Error())
					continue
				}

				//logger.Error("got error initiating STK Push %s ",msg)
				continue
			//}

		} else  {

			logger.Error("Could not scan data got error %s ",err.Error())

		}
	}

	tx.Commit()

	return
}

func ResendFailed1(db *sql.DB,cfg *ini.File,conn *amqp.Connection) {

	rows, err := db.Query("SELECT id,transaction_type,amount,reference FROM utility WHERE status IN (1,2,3) AND id NOT IN (select transaction_id FROM api_request ) AND retry_count < 10 AND (TIMESTAMPDIFF(MINUTE,last_retry,now()) > 4 OR last_retry IS NULL) ")
	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		return
	}

	defer rows.Close()

	x := 0

	st, err := db.Prepare("UPDATE utility SET retry_count = retry_count + 1 WHERE id = ? ")
	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		return
	}

	for rows.Next() {

		x++
		var id,transaction_type,amount sql.NullInt64
		var reference sql.NullString

		err = rows.Scan(&id,&transaction_type,&amount,&reference)

		if err == nil {

			_, err := st.Exec(id.Int64)

			if err != nil {

				logger.Error("Got error updating transaction retry_count %s ",err.Error())
			}

			logger.Info("Got at 1 with %d amount %d reference %s ",x,amount.Int64,reference.String)

			if transaction_type.Int64 == 1 {

				err = Publish(conn, cfg, "AIRTIME", id.Int64, 0)

			} else {

				err = Publish(conn, cfg, "KPLC_TOKEN", id.Int64, 0)

			}

		} else  {

			logger.Error("Could not scan data got error %s ",err.Error())

		}
	}

	st.Close()

	return
}

func UpdateOperatorCode(db *sql.DB) {

	rows, err := db.Query("SELECT id,url FROM api_request ")
	if err != nil {

		logger.Error("got error executing transaction %s ",err.Error())
		return
	}

	defer rows.Close()

	for rows.Next() {

		var id sql.NullInt64
		var reference sql.NullString

		err = rows.Scan(&id,&reference)

		if err == nil {

			//http://193.104.202.165/kenya/mainlinkpos/purchase/pw_etrans.php3?loginstatus=LIVE&transid=1741605509579074&operatorcode=4&recharge=200&mobileno=254723113953&agentpwd=infinitytech20&circode=1&narration=Airtime+purchase+for+254723113953+&custid=174&appver=1.0&retailerid=20&denomination=0&product=&bulkqty=1&agentid=20
			parts := strings.Split(reference.String,"operatorcode=")
			if len(parts) > 1 {

				operatorcode := int64(0)

				s := parts[1]
				pp := s[0:1]

				logger.Info("GOT STring %s ",pp)

				operatorcode, err = strconv.ParseInt(pp,10,64)

				if err == nil {

					st, err := db.Prepare("UPDATE api_request SET operator_code = ? WHERE id = ? ")
					if err != nil {

						logger.Error("gor error creating reversals %s ",err.Error())
						continue
					}

					st.Exec(operatorcode,id.Int64)

				} else {

					logger.Info("Get errors here converting %s error %s",pp,err.Error())
				}

			}

		} else  {

			logger.Error("Could not scan data got error %s ",err.Error())

		}
	}

	return
}
