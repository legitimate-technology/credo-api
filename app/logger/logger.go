package logger

import (
	"cloud.google.com/go/logging"
	"fmt"
	"github.com/getsentry/sentry-go"
	"log"
	"os"
	"time"
)

var Directory string
var criticalLog, errorLog, emergencyLog, noticeLog, warningLog, alertLog, logLog, infoLog, PanicLog *log.Logger
var Level int

func init() {

	//jsonConfig := config.GetConfig();

	//log.Println(jsonConfig.Logs.Error)

	critical :="/credo/logs/api/info.log"
	emergency :="/credo/logs/api/info.log"
	errorL :="/credo/logs/api/info.log"
	info :="/credo/logs/api/info.log"
	notice :="/credo/logs/api/info.log"
	warning :="/credo/logs/api/info.log"
	alert :="/credo/logs/api/info.log"
	logs :="/credo/logs/api/info.log"
	levels :=9

	var criticalFile, err0 = os.OpenFile(critical, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err0 != nil {
		panic(err0)
	}

	var emergencyFile, err1 = os.OpenFile(emergency, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err1 != nil {
		panic(err1)
	}

	var errorFile, err2 = os.OpenFile(errorL, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err2 != nil {
		panic(err2)
	}

	var infoFile, err4 = os.OpenFile(info, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err4 != nil {
		panic(err4)
	}

	var noticeFile, err3 = os.OpenFile(notice, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err3 != nil {

		panic(err3)
	}

	var warningFile, err5 = os.OpenFile(warning, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err5 != nil {
		panic(err5)
	}

	var alertFile, err6 = os.OpenFile(alert, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err6 != nil {

		panic(err6)
	}

	var logFile, err7 = os.OpenFile(logs, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err7 != nil {

		panic(err7)
	}

	Level = levels;

	criticalLog = log.New(criticalFile, "", log.LstdFlags|log.Lshortfile)
	emergencyLog = log.New(emergencyFile, "", log.LstdFlags|log.Lshortfile)
	errorLog = log.New(errorFile, "",log.LstdFlags|log.Lshortfile)
	infoLog = log.New(infoFile, "", log.LstdFlags|log.Lshortfile)
	noticeLog = log.New(noticeFile, "", log.LstdFlags|log.Lshortfile)
	warningLog = log.New(warningFile, "", log.LstdFlags|log.Lshortfile)
	alertLog = log.New(alertFile, "", log.LstdFlags|log.Lshortfile)
	logLog = log.New(logFile, "", log.LstdFlags|log.Lshortfile)
	PanicLog = log.New(errorFile, "", log.LstdFlags|log.Lshortfile)

}

func Critical(format string, v ...interface{}) {

	criticalLog.SetPrefix("[Critical] ")

	if Level > 0 {

		if len(v) == 0 {

			criticalLog.Output(2, fmt.Sprint(format))

		} else {

			criticalLog.Output(2, fmt.Sprintf(format, v...))
		}

	}
}

func remoteLog(name string,severity logging.Severity,format string, v ...interface{})  {

	return
	//ctx := context.Background()
	//
	//// Sets your Google Cloud Platform project ID.
	//projectID, err :=  config.GetKey(cfg,"GCLOUD","project_id"); //"YOUR_PROJECT_ID"
	//if err != nil {
	//
	//	projectID = "spotika-2"
	//}
	//
	//key_path, err :=  config.GetKey(cfg,"GCLOUD","key_path"); //"YOUR_PROJECT_ID"
	//if err != nil {
	//
	//	key_path = "/credo/config/spotika-2-ea9e444ffa9d.json"
	//}
	//
	//// Creates a client. /credo/config/spotika-2-ea9e444ffa9d.json
	//client, err := logging.NewClient(ctx, projectID,option.WithCredentialsFile(key_path))
	//if err != nil {
	//
	//	Error1("Failed to create client: %s", err.Error())
	//	return
	//}
	//
	//defer client.Close()
	//
	//// Sets the name of the log to write to.
	//logger := client.Logger(name).StandardLogger(severity)
	//
	//// Logs "hello world", log entry is visible at
	//// Stackdriver Logs.
	//logger.Printf(format,v...)
	//
	//err = sentry.Init(sentry.ClientOptions {
	//
	//	Dsn: "https://4c6a362a82004ede857e24c10485db44@sentry.io/4478177",
	//
	//})
	//
	//if err != nil {
	//
	//	Error1("Failed push to sentry: %s", err.Error())
	//	return
	//}
	//
	//sentry.CaptureMessage(fmt.Sprintf(format, v...))

}

func Emergency(format string, v ...interface{}) {

	emergencyLog.SetPrefix("[Emergency] ")

	if Level > 1 {

		if len(v) == 0 {

			emergencyLog.Output(2, fmt.Sprint(format))

		} else {

			emergencyLog.Output(2, fmt.Sprintf(format, v...))
		}

		go func() {

			err := sentry.Init(sentry.ClientOptions{
				Dsn: "https://4c6a362a82004ede857e24c10485db44@sentry.io/4478177",
			})
			if err != nil {

				return
			}

			sentry.CaptureMessage(fmt.Sprintf(format, v...))
		}()

	}
}

func Error(format string, v ...interface{}) {

	errorLog.SetPrefix("[Error] ")

	if Level > 2 {

		if len(v) == 0 {

			errorLog.Output(2, fmt.Sprint(format))

		} else {

			errorLog.Output(2, fmt.Sprintf(format, v...))
		}

		go func() {

			err := sentry.Init(sentry.ClientOptions{
				Dsn: "https://4c6a362a82004ede857e24c10485db44@sentry.io/4478177",
			})
			if err != nil {

				return
			}

			sentry.CaptureMessage(fmt.Sprintf(format, v...))
		}()

	}

}

func Warning(format string, v ...interface{}) {

	warningLog.SetPrefix("[warning] ")

	if Level > 3 {

		if len(v) == 0 {

			warningLog.Output(2, fmt.Sprint(format))

		} else {

			warningLog.Output(2, fmt.Sprintf(format, v...))
		}

		go func() {

			err := sentry.Init(sentry.ClientOptions{
				Dsn: "https://4c6a362a82004ede857e24c10485db44@sentry.io/4478177",
			})
			if err != nil {

				return
			}

			sentry.CaptureMessage(fmt.Sprintf(format, v...))
		}()

	}
}

func Alert(format string, v ...interface{}) {

	alertLog.SetPrefix(fmt.Sprintf("[Alert] "))

	if Level > 4 {

		if len(v) == 0 {

			alertLog.Output(2, fmt.Sprint(format))

		} else {

			alertLog.Output(2, fmt.Sprintf(format, v...))
		}

	}
}

func Notice(format string, v ...interface{}) {

	noticeLog.SetPrefix(fmt.Sprintf("[Notice] "))

	if Level > 5 {

		if len(v) == 0 {

			noticeLog.Output(2, fmt.Sprint(format))

		} else {

			noticeLog.Output(2, fmt.Sprintf(format, v...))
		}
	}
}

func Info(format string, v ...interface{}) {

	infoLog.SetPrefix(fmt.Sprintf("[Info] "))

	if Level > 6 {

		if len(v) == 0 {

			infoLog.Output(2, fmt.Sprint(format))

		} else {

			infoLog.Output(2, fmt.Sprintf(format, v...))

		}

	}
}

func Log(format string, v ...interface{}) {

	if Level > 9 {

		logLog.SetPrefix(fmt.Sprintf("[Log] "))

		if len(v) == 0 {

			logLog.Output(2, fmt.Sprint(format))

		} else {

			logLog.Output(2, fmt.Sprintf(format, v...))

		}

	}
}

func Error1(format string, v ...interface{}) {

	if Level > 2 {

		if len(v) == 0 {

			errorLog.Output(2, fmt.Sprint(format))

		} else {

			errorLog.Output(2, fmt.Sprintf(format, v...))
		}

		go func() {

			err := sentry.Init(sentry.ClientOptions{
				Dsn: "https://4c6a362a82004ede857e24c10485db44@sentry.io/4478177",
			})
			if err != nil {

				return
			}

			sentry.CaptureMessage(fmt.Sprintf(format, v...))
		}()

	}

}

func LogTime(t time.Time) string {

	//loc, _ := time.LoadLocation("Africa/Nairobi")
	//return t.In(loc).Format("2006-01-02 15:04:05")

	return t.Format(time.RFC3339) //("2006-01-02T15:04:05")
}
