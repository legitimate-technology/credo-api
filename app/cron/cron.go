package cron

import (
	"credo/app/database"
	"credo/app/library"
	"credo/app/logger"
	"gopkg.in/ini.v1"
	"log"
	"time"
)

func init() {

	var defaultConfigPath = "/credo/config/config.ini"

	conf, err := ini.Load(defaultConfigPath)

	if err != nil {

		log.Panic(err.Error())
	}

	go func() {

		ticker := time.NewTicker(20 * time.Second)
		conn := database.GetRabbitMQConnection(conf)
		db := database.DbInstance(conf)

		//go func() {

			for t := range ticker.C {

				logger.Info(" cronjob ResendFailed running here at %s ",t.Format("2006-01-02 15:04:05"))

				if conn == nil || conn.IsClosed() {

					conn = database.GetRabbitMQConnection(conf)
				}

				if db == nil || db.Ping() != nil {

					db = database.DbInstance(conf)
				}

				library.ResendFailedWithWrongStatus(db,conf,conn)
				time.Sleep(10 * time.Second)
				library.ResendFailed(db,conf,conn)
			}

		//}()

		select {}
	}()

	go func() {

		ticker := time.NewTicker(1 * time.Minute)
		conn := database.GetRabbitMQConnection(conf)
		db := database.DbInstance(conf)

		//go func() {

			for t := range ticker.C {

				logger.Info(" cronjob ResendPermanentlyFailed running here at %s ",t.Format("2006-01-02 15:04:05"))

				if conn == nil || conn.IsClosed() {

					conn = database.GetRabbitMQConnection(conf)
				}

				if db == nil || db.Ping() != nil {

					db = database.DbInstance(conf)
				}

				library.RevertPermanentlyFailed(db,conf)
			}

		//}()

		select {}
	}()

}