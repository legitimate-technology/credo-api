package database

import (
	"credo/app/logger"
	"credo/config"
	"fmt"
	reds "github.com/garyburd/redigo/redis"
	"github.com/logrusorgru/aurora"
	"gopkg.in/ini.v1"
	"redis"
	"strconv"
	"time"
)

func newPool(server, password string, databaseNumber int) *reds.Pool {

	return &reds.Pool{
		MaxIdle: 10,
		IdleTimeout: 60 * time.Second,
		MaxActive: 1000,
		Dial: func () (reds.Conn, error) {

			c, err := reds.Dial("tcp", server)
			if err != nil {
				return nil, err
			}

			if _, err := c.Do("AUTH", password); err != nil {
				c.Close()
				return nil, err
			}

			if _, err := c.Do("SELECT", databaseNumber); err != nil {

				c.Close()
				return nil, err
			}

			return c, err
		},
		TestOnBorrow: func(c reds.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func GetRedis(cfg *ini.File) *reds.Pool {

	host, err := config.GetKey(cfg,"redis", "host")

	if err != nil {

		logger.Error(fmt.Sprintf("got error reading redis host configs %s ",aurora.Red(err.Error())))
		return nil
	}

	port, err := config.GetKey(cfg,"redis", "port")

	if err != nil {

		logger.Error(fmt.Sprintf("got error reading redis port configs %s ",aurora.Red(err.Error())))
		return nil
	}

	auth, err := config.GetKey(cfg,"redis", "password")

	if err != nil {

		logger.Error(fmt.Sprintf("got error reading redis configs %s ",aurora.Red(err.Error())))
		return nil
	}

	db, err := config.GetKey(cfg,"redis", "database_number")

	if err != nil {

		db = "1"
	}

	dbNumber, err := strconv.Atoi(db)

	if err != nil {

		dbNumber = 1
	}

	uri := fmt.Sprintf("redis://%s:%s",host,port)
	uri = fmt.Sprintf("%s:%s",host,port)

	conn := newPool(uri,auth,dbNumber)

	//logger.Log("%s",aurora.Cyan(fmt.Sprintf("CURRENT ACTIVE CONNECTIONS %d AND IDLE CONNECTIONS %d ",conn.Stats().ActiveCount,conn.Stats().IdleCount)))

	if conn == nil {

		logger.Error(fmt.Sprintf("got nil while trying to connect to reddis "))
		return nil
	}

	return conn
}

func GetRedisConn(cfg *ini.File) reds.Conn {

	host, err := config.GetKey(cfg,"redis", "host")

	if err != nil {

		logger.Error(fmt.Sprintf("got error reading redis host configs %s ",aurora.Red(err.Error())))
		return nil
	}

	port, err := config.GetKey(cfg,"redis", "port")

	if err != nil {

		logger.Error(fmt.Sprintf("got error reading redis port configs %s ",aurora.Red(err.Error())))
		return nil
	}

	auth, err := config.GetKey(cfg,"redis", "password")

	if err != nil {

		logger.Error(fmt.Sprintf("got error reading redis configs %s ",aurora.Red(err.Error())))
		return nil
	}

	db, err := config.GetKey(cfg,"redis", "database_number")

	if err != nil {

		db = "1"
	}

	dbNumber, err := strconv.Atoi(db)

	if err != nil {

		dbNumber = 1
	}

	uri := fmt.Sprintf("redis://%s:%s",host,port)
	//uri = fmt.Sprintf("%s:%s",host,port)

	//conn := newPool(uri,auth,dbNumber)

	//logger.Log("%s",aurora.Cyan(fmt.Sprintf("CURRENT ACTIVE CONNECTIONS %d AND IDLE CONNECTIONS %d ",conn.Stats().ActiveCount,conn.Stats().IdleCount)))

	// Initialize the redis connection to a redis instance running on your local machine
	conn, err := reds.DialURL(uri)

	if err != nil {

		logger.Error(fmt.Sprintf("got error trying to connect to reddis %s ",aurora.Red(err.Error())))
		return nil
	}

	if conn == nil {

		logger.Error(fmt.Sprintf("got nil while trying to connect to reddis "))
		return nil
	}


	if _, err := conn.Do("AUTH", auth); err != nil {
		conn.Close()

		logger.Error(fmt.Sprintf("got error trying to authenticated to reddis %s ",aurora.Red(err.Error())))
		return nil
	}


	if _, err := conn.Do("SELECT", dbNumber); err != nil {

		conn.Close()
		logger.Error("got error trying to select db to reddis %s ",aurora.Red(err.Error()))
		return nil
	}

	return conn
}

func RedisClient(cfg *ini.File) *redis.Client {

	host, err := config.GetKey(cfg,"redis", "host")

	if err != nil {

		logger.Error(fmt.Sprintf("got error reading redis host configs %s ",aurora.Red(err.Error())))
		return nil
	}

	port, err := config.GetKey(cfg,"redis", "port")

	if err != nil {

		logger.Error(fmt.Sprintf("got error reading redis port configs %s ",aurora.Red(err.Error())))
		return nil
	}

	db, err := config.GetKey(cfg,"redis", "database_number")

	if err != nil {

		db = "1"
	}

	dbNumber, err := strconv.Atoi(db)

	if err != nil {

		dbNumber = 1
	}

	auth, err := config.GetKey(cfg,"redis", "password")

	if err != nil {

		logger.Error(fmt.Sprintf("got error reading redis configs %s ",aurora.Red(err.Error())))
		return nil
	}

	uri := fmt.Sprintf("redis://%s:%s",host,port)
	uri = fmt.Sprintf("%s:%s",host,port)

	client := redis.NewClient(&redis.Options{
		MinIdleConns: 10,
		IdleTimeout: 60 * time.Second,
		PoolSize: 1000,
		Addr:     uri,
		Password: auth, // no password set
		DB:       dbNumber,  // use default DB
	})

	return client
}