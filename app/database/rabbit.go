package database

import (
	"fmt"
	"github.com/streadway/amqp"
	"gopkg.in/ini.v1"
	"credo/app/logger"
	"credo/config"
)

func GetRabbitMQConnection(cfg *ini.File) *amqp.Connection {

	host, err := config.GetKey(cfg,"rabbit", "host")

	if err != nil {

		logger.Error(err.Error())
	}

	user, err := config.GetKey(cfg,"rabbit", "user")

	if err != nil {
		logger.Error(err.Error())
	}

	pass, err := config.GetKey(cfg,"rabbit", "pass")

	if err != nil {
		logger.Error(err.Error())
	}

	port, err := config.GetKey(cfg,"rabbit", "port")

	if err != nil {
		logger.Error(err.Error())
	}

	vhost, err := config.GetKey(cfg,"rabbit", "vhost")
	if err != nil {

		logger.Error(err.Error())
	}

	uri := fmt.Sprintf("amqp://%s:%s@%s:%s/%s", user, pass, host, port,vhost)

	conn, err := amqp.Dial(uri)

	if err != nil {

		logger.Critical("got error connecting to rabbitMQ %s with %s", err.Error(), uri)
		panic(err)
	}

	return conn
}

func GetAMPQURL(cfg *ini.File) string {

	host, err := config.GetKey(cfg,"rabbit", "host")

	if err != nil {
		logger.Error(err.Error())
	}

	user, err := config.GetKey(cfg,"rabbit", "user")

	if err != nil {
		logger.Error(err.Error())
	}

	pass, err := config.GetKey(cfg,"rabbit", "pass")

	if err != nil {
		logger.Error(err.Error())
	}

	port, err := config.GetKey(cfg,"rabbit", "port")

	if err != nil {
		logger.Error(err.Error())
	}

	amqpURI := fmt.Sprintf("amqp://%s:%s@%s:%s/", user, pass, host, port)

	return amqpURI
}