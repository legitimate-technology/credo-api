package database

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/logrusorgru/aurora"
	"gopkg.in/ini.v1"
	//"github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/mysql"
	"credo/app/logger"
	"credo/config"
	"strconv"
)

func DbInstance(cfg *ini.File) *sql.DB {

	adapter, err := config.GetKey(cfg,"database", "adapter")

	if err != nil {

		logger.Error("got error reading db configs %s ", aurora.Red(err.Error()))
		return nil

	} else if adapter == "mysql" {

		username, err := config.GetKey(cfg,"database", "username")

		if err != nil {
			logger.Error(err.Error())
		}
		password, err := config.GetKey(cfg,"database", "password")

		if err != nil {
			logger.Error(err.Error())
		}

		dbname, err := config.GetKey(cfg,"database", "dbname")

		if err != nil {
			logger.Error(err.Error())
		}

		host, err := config.GetKey(cfg,"database", "host")

		if err != nil {
			logger.Error(err.Error())
		}

		port, err := config.GetKey(cfg,"database", "port")

		if err != nil {
			logger.Error(err.Error())
		}

		charset, err := config.GetKey(cfg,"database", "charset")

		if err != nil {
			logger.Error(err.Error())
		}

		dbURI := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True", username, password, host, port, dbname, charset)

		//Db, err = sqlx.Connect(DbConfig.RemoteDb.Dialect, dbURI)

		Db, err := sql.Open("mysql", dbURI)

		checkErr(err)

		idle_connection, err := config.GetKey(cfg,"database", "idle_connection")

		if err != nil {
			logger.Error(err.Error())
		}

		ic, err := strconv.Atoi(idle_connection)

		if err != nil {

			ic = 5
		}

		logger.Error("maxDN connections set to %d ",ic)

		//Db.SetMaxIdleConns(100)
		//Db.SetConnMaxLifetime(time.Hour * 5)
		//Db.SetMaxOpenConns(200)


		err = Db.Ping()
		checkErr(err)


		//cfg := mysql.Cfg(INSTANCE_CONNECTION_NAME, DATABASE_USER, PASSWORD)
		//cfg.DBName = DATABASE_NAME
		//db, err := mysql.DialCfg(cfg)

		return Db

	} else if adapter == "postgres" {

		username, err := config.GetKey(cfg,"database", "username")

		if err != nil {
			logger.Error(err.Error())
		}
		password, err := config.GetKey(cfg,"database", "password")

		if err != nil {
			logger.Error(err.Error())
		}
		host, err := config.GetKey(cfg,"database", "host")

		if err != nil {
			logger.Error(err.Error())
		}

		dbname, err := config.GetKey(cfg,"database", "dbname")

		if err != nil {
			logger.Error(err.Error())
		}

		port, err := config.GetKey(cfg,"database", "port")

		if err != nil {
			logger.Error(err.Error())
		}

		dbURI := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s", host, port, username, dbname, password)

		Db, err := sql.Open("postgres", dbURI)
		defer Db.Close()
		checkErr(err)

		return Db

	}
	return nil
}

func GormDbInstance(cfg *ini.File) *gorm.DB {

	username, err := config.GetKey(cfg,"database", "username")

	if err != nil {
		logger.Error(err.Error())
	}
	password, err := config.GetKey(cfg,"database", "password")

	if err != nil {
		logger.Error(err.Error())
	}

	dbname, err := config.GetKey(cfg,"database", "dbname")

	if err != nil {
		logger.Error(err.Error())
	}

	host, err := config.GetKey(cfg,"database", "host")

	if err != nil {
		logger.Error(err.Error())
	}

	port, err := config.GetKey(cfg,"database", "port")

	if err != nil {
		logger.Error(err.Error())
	}

	charset, err := config.GetKey(cfg,"database", "charset")

	if err != nil {
		logger.Error(err.Error())
	}

	dbURI := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True", username, password, host, port, dbname, charset)

	//Db, err = sqlx.Connect(DbConfig.RemoteDb.Dialect, dbURI)

	Db, err :=  gorm.Open("mysql", dbURI)

	// Db, err := sql.Open("mysql", dbURI)

	checkErr(err)

	return Db
}

func checkErr(err error) {
	if err != nil {

		logger.Emergency("DB ERROR %s ", err.Error())
	}
}
