package database

import (
	"context"
	"fmt"
	"github.com/logrusorgru/aurora"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/options"
	"gopkg.in/ini.v1"
	"credo/app/logger"
	"credo/config"
)

func MongoInstance(cfg *ini.File,) *mongo.Client {

	host, err := config.GetKey(cfg,"mongo", "host")

	if err != nil {

		logger.Error(err.Error())
		return nil
	}

	port, err := config.GetKey(cfg,"mongo", "port")

	if err != nil {
		logger.Error(err.Error())
		return nil
	}

	dbname, err := config.GetKey(cfg,"user", "database")

	if err != nil {
		logger.Error(err.Error())
		return nil
	}

	user, err := config.GetKey(cfg,"mongo", "user")

	if err != nil {
		logger.Error(err.Error())
		return nil
	}

	password, err := config.GetKey(cfg,"mongo", "password")

	if err != nil {
		logger.Error(err.Error())
		return nil
	}

	//mongodb://user:password@localhost:27017/?authSource=admin

	con_string := fmt.Sprintf("mongodb://%s:%s@%s:%s",user,password,host,port)

	if err != nil {
		logger.Error(err.Error())
	}

	opts := options.Client().SetAppName(dbname).SetAuth(options.Credential{
		Password:                password,
		Username:                user,
	})

	client, err := mongo.Connect(context.TODO(), con_string,opts)

	if err != nil {

		logger.Error("got error connecting to mongo %s ",aurora.Red(err.Error()))
	}


	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {

		logger.Error("got error connecting to mongo %s ",aurora.Red(err.Error()))

	}

	return client
}