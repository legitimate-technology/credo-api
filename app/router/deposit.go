package router

import (
	"net/http"
	"credo/app/controllers"
)

func (a *App) STKPaymentCallback(w http.ResponseWriter, r *http.Request) {

	controllers.STKPaymentCallback(a.DB,a.Config,a.Conn,a.RedisConnection,w, r)
}

func (a *App) TokensCallback(w http.ResponseWriter, r *http.Request) {

	controllers.TokensCallback(a.DB,a.Config,a.Conn,a.RedisConnection,w, r)
}


func (a *App) Payment(w http.ResponseWriter, r *http.Request) {

	controllers.Payment(a.DB,a.Config,a.Conn,a.RedisConnection,w, r)
}

func (a *App) ReversalCallback(w http.ResponseWriter, r *http.Request) {

	controllers.ReversalCallback(a.DB,a.Config,a.Conn,a.RedisConnection,w, r)
}