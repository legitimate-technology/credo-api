package router

import (
	"credo/app/database"
	"credo/app/logger"
	"credo/config"
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/logrusorgru/aurora"
	"github.com/streadway/amqp"
	"gopkg.in/ini.v1"
	"log"
	"net/http"
	"redis"
	"time"
)

// router and DB instance
type App struct {

	Router *mux.Router
	Conn     *amqp.Connection
	DB     *sql.DB
	ArriveTime int64
	RedisConnection *redis.Client
	Config *ini.File
}

var defaultConfigPath = "/credo/config/config.ini"

// Initialize initializes the app with predefined configuration
func (a *App) Initialize() {


	cfg, err := ini.Load(defaultConfigPath)
	if err != nil {

		log.Panic(err.Error())
	}

	a.Conn = database.GetRabbitMQConnection(cfg)
	a.DB = database.DbInstance(cfg)
	a.RedisConnection = database.RedisClient(cfg)

	a.Config = cfg

	a.Router = mux.NewRouter()
	a.setRouters()
	//go library.UpdateOperatorCode(a.DB)
}

// setRouters sets the all required router
func (a *App) setRouters() {

	// profile
	a.Post("/profile/balance/{profile_id}", a.ProfileBalance)
	a.Post("/profile/signup", a.SignUp)
	a.Post("/profile/verify", a.Verify)
	a.Post("/profile/login", a.Login)
	a.Post("/profile/password/reset", a.ResetPassword)
	a.Post("/profile/password/change", a.ChangePassword)
	a.Post("/profile/utility", a.Transactions)
	a.Post("/sms/template", a.Template)

	// airtime purchase API
	a.Post("/purchase/airtime", a.AirtimePurchase)
	a.Post("/purchase/multiple/airtime", a.AirtimeMultiPurchase)
	a.Post("/purchase/tokens", a.TokenPurchase)
	a.Post("/purchase/multiple/tokens", a.TokenMultiPurchase)
	a.Post("/profile/redeem", a.RedeemPoints)

	a.Post("/admin/redeem", a.AdminRedeemPoints)
	a.Post("/admin/purchase", a.AdminPurchaseAirtime)
	a.Post("/admin/requeue", a.AdminRequeueTransaction)

	//payment callbacks
	a.Post("/payment/mpesa/stk/{transactionID}", a.STKPaymentCallback)
	a.Post("/payment/mpesa/reverse/{reversalID}", a.ReversalCallback)
	a.Post("/payment/mpesa", a.Payment)
	a.Post("/tokens/callback", a.TokensCallback)

	a.Post("/status", a.Status)
	a.Get("/status", a.Status)

	//go library.KiandaRegisterURL(a.Config)

}

// Run the app on it's router
func (a *App) Run() {

	host, err := config.GetKey(a.Config,"server", "host")
	if err != nil {

		logger.PanicLog.Fatalf("Cannot get hostname %s ", aurora.Red(err.Error()))
	}

	port, err := config.GetKey(a.Config,"server", "port")

	if err != nil {

		logger.Error("Cannot read server port %s ",aurora.Red(err.Error()))
	}

	server := fmt.Sprintf("%s:%s",host,port)
	logger.Critical(" listening on %s ",aurora.BgGreen(server))

	srv := &http.Server {
		Handler: a.Router,
		Addr:    server,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		ErrorLog: logger.PanicLog,
	}

	//logger.Critical(http.ListenAndServe(server, a.Router).Error())
	logger.Critical(srv.ListenAndServe().Error())

	//logger.Critical(http.ListenAndServe(server, a.Router).Error())

}