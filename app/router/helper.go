package router

import (
	"net/http"
)

// Get wraps the router for GET method
func (a *App) Get(path string, f func(w http.ResponseWriter, r *http.Request))  {
	a.Router.HandleFunc(path, f).Methods("GET")


}

func (a *App) GetWithparams(path string,params map[string] string, f func(w http.ResponseWriter, r *http.Request))  {


	for k, v := range params {

		a.Router.HandleFunc(path, f).Methods("GET").Queries(k,v)

	}

}

// Post wraps the router for POST method
func (a *App) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {

	a.Router.HandleFunc(path, f).Methods("POST")

}

// Put wraps the router for PUT method
func (a *App) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT")

}

// Delete wraps the router for DELETE method
func (a *App) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE")

}