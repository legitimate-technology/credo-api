package router

import (
	"net/http"
	"credo/app/controllers"
)

func (a *App) AirtimePurchase(w http.ResponseWriter, r *http.Request) {

	controllers.AirtimePurchase(a.DB,a.Config,a.RedisConnection,w, r)
}

func (a *App) AirtimeMultiPurchase(w http.ResponseWriter, r *http.Request) {

	controllers.AirtimeMultiPurchase(a.DB,a.Config,a.RedisConnection,w, r)
}

func (a *App) TokenMultiPurchase(w http.ResponseWriter, r *http.Request) {

	controllers.TokenMultiPurchase(a.DB,a.Config,a.RedisConnection,w, r)
}

func (a *App) TokenPurchase(w http.ResponseWriter, r *http.Request) {

	controllers.TokenPurchase(a.DB,a.Config,a.RedisConnection,w, r)
}

func (a *App) RedeemPoints(w http.ResponseWriter, r *http.Request) {

	controllers.RedeemPoints(a.DB,a.Conn,a.Config,a.RedisConnection,w, r)
}

func (a *App) AdminRedeemPoints(w http.ResponseWriter, r *http.Request) {

	controllers.AdminRedeemPoints(a.DB,a.Conn,a.Config,a.RedisConnection,w, r)
}

func (a *App) AdminPurchaseAirtime(w http.ResponseWriter, r *http.Request) {

	controllers.AdminPurchaseAirtime(a.DB,a.Conn,a.Config,a.RedisConnection,w, r)
}

func (a *App) AdminRequeueTransaction(w http.ResponseWriter, r *http.Request) {

	controllers.AdminRequeueTransaction(a.DB,a.Conn,a.Config,a.RedisConnection,w, r)
}
