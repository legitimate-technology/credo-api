package router

import (
	"net/http"
	"credo/app/controllers"
)

func (a *App) SignUp(w http.ResponseWriter, r *http.Request) {

	controllers.SignUp(a.DB,a.Config,a.RedisConnection,w, r)
}

func (a *App) ProfileBalance(w http.ResponseWriter, r *http.Request) {

	controllers.ProfileBalance(a.DB,a.RedisConnection,w, r)
}

func (a *App) Verify(w http.ResponseWriter, r *http.Request) {

	controllers.Verify(a.DB,a.RedisConnection,w, r)
}

func (a *App) Login(w http.ResponseWriter, r *http.Request) {

	controllers.Login(a.DB,a.RedisConnection,w, r)
}

func (a *App) ResetPassword(w http.ResponseWriter, r *http.Request) {

	controllers.ResetPassword(a.DB,a.Config,a.RedisConnection,w, r)
}

func (a *App) ChangePassword(w http.ResponseWriter, r *http.Request) {

	controllers.ChangePassword(a.DB,a.RedisConnection,w, r)
}

func (a *App) Transactions(w http.ResponseWriter, r *http.Request) {

	controllers.Transactions(a.DB,a.RedisConnection,w, r)
}

func (a *App) Template(w http.ResponseWriter, r *http.Request) {

	controllers.Template(a.DB,a.RedisConnection,w, r)
}

func (a *App) Status(w http.ResponseWriter, r *http.Request) {


	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(200)
	w.Write([]byte("OK"))
}