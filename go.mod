module credo

go 1.14

require (
	cloud.google.com/go/logging v1.0.0
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/dongri/phonenumber v0.0.0-20191114083529-37aca6809ea4 // indirect
	github.com/garyburd/redigo v1.6.0
	github.com/getsentry/sentry-go v0.7.0
	github.com/go-cmd/cmd v1.2.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/snappy v0.0.2-0.20190904063534-ff6b7dc882cf // indirect
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/logrusorgru/aurora v0.0.0-20190417123914-21d75270181e
	github.com/mongodb/mongo-go-driver v0.3.0
	github.com/streadway/amqp v1.0.0
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.1-0.20180714160509-73f8eece6fdc // indirect
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208 // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/ini.v1 v1.58.0
)
