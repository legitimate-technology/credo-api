package config

import (
	"gopkg.in/ini.v1"
)

const (
	// Local path to config content
	defaultMarketgPath = "/credo/config/markets.ini"
)
/**

 */

 func GetKey(config *ini.File,section string, key string) (string,error)  {

	return  config.Section(section).Key(key).String(),nil

}

